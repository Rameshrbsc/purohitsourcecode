package panditapp.codegen.com.panditapp.Activity;

import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.BookingDetailListPojo;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import panditapp.codegen.com.panditapp.Utils.NotificationUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class FlashActivity extends AppCompatActivity {

    SeekBar seekBar;
    View thumbView;

    MyPreference myPreference;

    private ProgressDialog progressBar;

    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;

    @BindView(R.id.TextViewName)
    TextView TextViewName;
    @BindView(R.id.TextViewPoojaName)
    TextView TextViewPoojaName;
    @BindView(R.id.TextViewDateTime)
    TextView TextViewDateTime;
    @BindView(R.id.TextViewAddress)
    TextView TextViewAddress;

    @BindView(R.id.reject)
    LinearLayout reject;
    @BindView(R.id.accept)
    LinearLayout accept;

    String ImageURL = "", NotificationId;

    private AlertDialog SuccessDialog;

    private FirebaseAnalytics mFirebaseAnalytics;
    private static final String TAG = FlashActivity.class.getSimpleName();

    Bundle extras;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        myPreference = new MyPreference(FlashActivity.this);
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, displayFirebaseRegId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Purohit");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        ButterKnife.bind(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        extras = getIntent().getExtras();
        if (myPreference.getDefaultRunTimeValue()[0] != null){
            if(extras != null){
                if(extras.containsKey("Target"))
                {
                    if (extras.getString("Target") != null && !Objects.equals(extras.getString("Target"), "null")) {
                        NotificationId = extras.getString("NotificationId");
                        progressBar = new ProgressDialog(FlashActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.show();

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                        jsonObject.addProperty("BookingId", extras.getString("TargetID"));
                        jsonObject.addProperty("SenderType", "1");
                        API apiService = createServiceHeader(API.class);
                        Call<BookingDetailListPojo> call = apiService.BOOKING_DETAIL_LIST_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<BookingDetailListPojo>() {
                            @Override
                            public void onResponse(Call<BookingDetailListPojo> call, Response<BookingDetailListPojo> response) {
                                progressBar.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":
                                        if (response.body().getBookingDetails().length > 0) {
                                            if (!response.body().getBookingDetails()[0].getUserImage().isEmpty()){
                                                ImageURL = response.body().getBookingDetails()[0].getUserImage();
                                                Picasso.with(FlashActivity.this)
                                                        .load(ImageURL)
                                                        .into(ImageViewProfilePic);
                                            }else {
                                                ImageViewProfilePic.setVisibility(View.GONE);
                                            }

                                            TextViewPoojaName.setText(response.body().getBookingDetails()[0].getPoojaName() +" - "+ "Rs. " + response.body().getBookingDetails()[0].getAmount());
                                            setTitle(response.body().getBookingDetails()[0].getPoojaName());
                                            TextViewName.setText(response.body().getBookingDetails()[0].getUserName());

                                            TextViewAddress.setText(response.body().getBookingDetails()[0].getLocationName());
                                            try {
                                                //Date Processing
                                                String ConvertedDate = new SimpleDateFormat("dd-MM-yyyy",
                                                        Locale.getDefault()).format(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(response.body().getBookingDetails()[0].getBookingDate()));

                                                //Time Processing
                                                Date ConvertedTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).parse(new StringTokenizer(response.body().getBookingDetails()[0].getTime()).nextToken());
                                                Date CurrentTime = new SimpleDateFormat("hh:mm:ss", Locale.getDefault()).parse(new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()));
//                                            TextViewTime.setText(new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new SimpleDateFormat("hh:mm:ss", Locale.getDefault()).parse(new StringTokenizer(response.body().getBookingDetails()[0].getTime()).nextToken())));
                                                TextViewDateTime.setText(ConvertedDate +" - "+ new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new SimpleDateFormat("hh:mm:ss", Locale.getDefault()).parse(new StringTokenizer(response.body().getBookingDetails()[0].getTime()).nextToken())));
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;
                                    case "108":
                                        Toast.makeText(FlashActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                        Intent i = new Intent(FlashActivity.this, LoginActivity.class);
                                        i.putExtra("Target", "FlashActivity");
                                        i.putExtra("TargetID", extras.getString("TargetID"));
                                        startActivity(i);
                                        break;
                                    default:
                                        myPreference.ShowDialog(FlashActivity.this, "Oops", response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<BookingDetailListPojo> call, Throwable t) {
                                progressBar.dismiss();
                                try {
                                    myPreference.ShowDialog(FlashActivity.this, "Exception", t.getMessage());

                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
        }else {
            Toast.makeText(FlashActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(FlashActivity.this, LoginActivity.class);
            i.putExtra("Target", "FlashActivity");
            i.putExtra("TargetID", extras.getString("TargetID"));
            startActivity(i);
        }


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                NotificationUtils.clearNotifications(getApplicationContext());
                    LayoutInflater layoutInflater = LayoutInflater.from(FlashActivity.this);
                    View view1 = layoutInflater.inflate(R.layout.confirmation_popup_dialog, null);
                    TextView TextViewName = view1.findViewById(R.id.TextViewName);
                    TextViewName.setText(" You have Accepted "+FlashActivity.this.TextViewPoojaName.getText().toString());
                    TextView TextViewDate = view1.findViewById(R.id.TextViewDate);
                    TextViewDate.setText(FlashActivity.this.TextViewDateTime.getText().toString());
                    TextView TextViewPooja = view1.findViewById(R.id.TextViewPooja);
                    TextViewPooja.setText(FlashActivity.this.TextViewPoojaName.getText().toString());
                    TextView TextViewLocation = view1.findViewById(R.id.TextViewLocation);
                    TextViewLocation.setText(FlashActivity.this.TextViewAddress.getText().toString());
                    ImageView ImageViewProfilePic = view1.findViewById(R.id.ImageViewProfilePic);
                    if (!ImageURL.isEmpty()){
                        Picasso.with(FlashActivity.this)
                                .load(ImageURL)
                                .into(ImageViewProfilePic);
                    }else {
                        ImageViewProfilePic.setVisibility(View.GONE);
                    }


                    AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                    builder.setView(view1);
                    builder.setCancelable(false).
                            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    progressBar = new ProgressDialog(FlashActivity.this);
                                    progressBar.setCancelable(true);
                                    progressBar.setMessage("Processing ...");
                                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    progressBar.setProgress(0);
                                    progressBar.setMax(100);
                                    progressBar.show();

                                    JsonObject jsonObject = new JsonObject();
                                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                    jsonObject.addProperty("BookingId", extras.getString("TargetID"));
                                    jsonObject.addProperty("Status", "1");

                                    API apiService = createServiceHeader(API.class);
                                    Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
                                    call.enqueue(new Callback<LoginPojo>() {
                                        @Override
                                        public void onResponse(Call<LoginPojo> Requestcall, Response<LoginPojo> response) {
                                            progressBar.dismiss();
                                            switch (response.body().getCode()) {
                                                case "200":
                                                    JsonObject jsonObject = new JsonObject();
                                                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                                    jsonObject.addProperty("SenderType","1");
                                                    jsonObject.addProperty("ReadNotification","1");
                                                    JsonArray array = new JsonArray();
                                                    array.add(NotificationId);
                                                    jsonObject.add("NotificaitonIdArray", array);
                                                    API apiService = createServiceHeader(API.class);
                                                    Call<LoginPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                                                    call.enqueue(new Callback<LoginPojo>() {
                                                        @Override
                                                        public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {

                                                            switch (response.body().getCode()) {
                                                                case "200":
                                                                    int TempCount = Integer.parseInt(myPreference.getDefaultRunTimeValue()[8]);
                                                                    if (TempCount>0){
                                                                        TempCount = --TempCount;
                                                                    }else {
                                                                        TempCount = 0;
                                                                    }
                                                                    //Store Notification count in SharedPreferences
                                                                    SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                                    editor.putString("NotificationCount", String.valueOf(TempCount));
                                                                    Log.i("NotificationCount",String.valueOf(TempCount));
                                                                    editor.apply();
                                                                    //Update Notification Count in bell icon
                                                                    Intent pushNotification = new Intent(Config.NOTIFICATION_ICON);
                                                                    LocalBroadcastManager.getInstance(FlashActivity.this).sendBroadcast(pushNotification);
                                                                    Toast.makeText(FlashActivity.this, "Pooja accepted successfully.", Toast.LENGTH_SHORT).show();
                                                                    Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                                    startActivity(i);
                                                                    break;
                                                                case "108":
                                                                    Intent intent = new Intent(FlashActivity.this, LoginActivity.class);
                                                                    startActivity(intent);
                                                                default:
                                                                    myPreference.ShowDialog(FlashActivity.this,"Oops",response.body().getMessage());
                                                                    break;
                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<LoginPojo> call, Throwable t) {
                                                            try {
                                                                myPreference.ShowDialog(FlashActivity.this,"Oops",t.getMessage());
                                                            } catch (NullPointerException e) {
                                                                e.printStackTrace();
                                                            }

                                                        }
                                                    });
                                                    break;
                                                case "108":
                                                    Toast.makeText(FlashActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(FlashActivity.this, LoginActivity.class));
                                                    break;
                                                default:
                                                    AlertDialog.Builder imageDialog = new AlertDialog.Builder(FlashActivity.this, R.style.RajCustomDialog);
                                                    LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                                                    View dialogView = inflater.inflate(R.layout.custom_failor_dialog1, null);
                                                    imageDialog.setView(dialogView);
                                                    TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                                    TextviewSmallTitle1.setText(response.body().getMessage());
                                                    TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                                    TextviewBigTitle.setText("Oops");
                                                    Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                                    ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                            startActivity(i);
                                                            SuccessDialog.dismiss();
                                                        }
                                                    });
                                                    SuccessDialog = imageDialog.create();
                                                    SuccessDialog.show();
                                            }
                                        }
                                        @Override
                                        public void onFailure(Call<LoginPojo> call, Throwable t) {
                                            progressBar.dismiss();
                                            try {
                                                myPreference.ShowDialog(FlashActivity.this,"Exception",t.getMessage());

                                            } catch (NullPointerException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    AlertDialog alertDialog1 = builder.create();
                    alertDialog1.show();
                }


        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationUtils.clearNotifications(getApplicationContext());
                AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                builder.setMessage("Are you sure? \nYou going to reject this pooja??");
                builder.setIcon(R.mipmap.ic_launcher_round);
                builder.setCancelable(false).
                        setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                progressBar = new ProgressDialog(FlashActivity.this);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Processing ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();

                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                jsonObject.addProperty("BookingId", extras.getString("TargetID"));
                                jsonObject.addProperty("Status", "2");
                                Log.e("JSON", jsonObject.toString());
                                API apiService = createServiceHeader(API.class);
                                Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
                                call.enqueue(new Callback<LoginPojo>() {
                                    @Override
                                    public void onResponse(Call<LoginPojo> Requestcall, Response<LoginPojo> response) {
                                        progressBar.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
                                                JsonObject jsonObject = new JsonObject();
                                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                                jsonObject.addProperty("SenderType","1");
                                                jsonObject.addProperty("ReadNotification","1");
                                                JsonArray array = new JsonArray();
                                                array.add(NotificationId);
                                                jsonObject.add("NotificaitonIdArray", array);
                                                API apiService = createServiceHeader(API.class);
                                                Call<LoginPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                                                call.enqueue(new Callback<LoginPojo>() {
                                                    @Override
                                                    public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {

                                                        switch (response.body().getCode()) {
                                                            case "200":
                                                                int TempCount = Integer.parseInt(myPreference.getDefaultRunTimeValue()[8]);
                                                                if (TempCount>0){
                                                                    TempCount = --TempCount;
                                                                }else {
                                                                    TempCount = 0;
                                                                }
                                                                //Store Notification count in SharedPreferences
                                                                SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                                editor.putString("NotificationCount", String.valueOf(TempCount));
                                                                Log.i("NotificationCount",String.valueOf(TempCount));
                                                                editor.apply();
                                                                //Update Notification Count in bell icon
                                                                Intent pushNotification = new Intent(Config.NOTIFICATION_ICON);
                                                                LocalBroadcastManager.getInstance(FlashActivity.this).sendBroadcast(pushNotification);
                                                                Toast.makeText(FlashActivity.this, "Pooja was rejected successfully.", Toast.LENGTH_SHORT).show();
                                                                Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                                startActivity(i);
                                                                break;
                                                            case "108":
                                                                Intent intent = new Intent(FlashActivity.this, LoginActivity.class);
                                                                startActivity(intent);
                                                            default:
                                                                myPreference.ShowDialog(FlashActivity.this,"Oops",response.body().getMessage());
                                                                break;
                                                        }

                                                    }

                                                    @Override
                                                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                                                        try {
                                                            myPreference.ShowDialog(FlashActivity.this,"Oops", "Something went wrong!!!");
                                                        } catch (NullPointerException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                });
                                                break;
                                            case "108":
                                                Toast.makeText(FlashActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                                startActivity(new Intent(FlashActivity.this, LoginActivity.class));
                                                break;
                                            default:
                                                AlertDialog.Builder imageDialog = new AlertDialog.Builder(FlashActivity.this, R.style.RajCustomDialog);
                                                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                                                View dialogView = inflater.inflate(R.layout.custom_failor_dialog1, null);
                                                imageDialog.setView(dialogView);
                                                TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                                TextviewSmallTitle1.setText(response.body().getMessage());
                                                TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                                TextviewBigTitle.setText("Oops");
                                                Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                                ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                        startActivity(i);
                                                        SuccessDialog.dismiss();
                                                    }
                                                });
                                                SuccessDialog = imageDialog.create();
                                                SuccessDialog.show();

                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                                        progressBar.dismiss();
                                        try {
                                            myPreference.ShowDialog(FlashActivity.this,"Exception",t.getMessage());

                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog1 = builder.create();
                alertDialog1.show();
            }
        });
        seekBar = findViewById(R.id.seekBar);


        seekBar.setThumb(getMyDrawable(1));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(final SeekBar seekBar, int progress, boolean fromUser) {
                seekBar.setThumb(getMyDrawable(progress));

                // clear the notification area when the app is opened
                NotificationUtils.clearNotifications(getApplicationContext());
                if (progress == 2){
                    LayoutInflater layoutInflater = LayoutInflater.from(FlashActivity.this);
                    View view1 = layoutInflater.inflate(R.layout.confirmation_popup_dialog, null);
                    TextView TextViewName = view1.findViewById(R.id.TextViewName);
                    TextViewName.setText(FlashActivity.this.TextViewPoojaName.getText().toString());
                    TextView TextViewDate = view1.findViewById(R.id.TextViewDate);
                    TextViewDate.setText(FlashActivity.this.TextViewDateTime.getText().toString());
                    TextView TextViewPooja = view1.findViewById(R.id.TextViewPooja);
                    TextViewPooja.setText(FlashActivity.this.TextViewPoojaName.getText().toString());
                    TextView TextViewLocation = view1.findViewById(R.id.TextViewLocation);
                    TextViewLocation.setText(FlashActivity.this.TextViewAddress.getText().toString());
                    ImageView ImageViewProfilePic = view1.findViewById(R.id.ImageViewProfilePic);
                    if (!ImageURL.isEmpty()){
                        Picasso.with(FlashActivity.this)
                                .load(ImageURL)
                                .into(ImageViewProfilePic);
                    }else {
                        ImageViewProfilePic.setVisibility(View.GONE);
                    }


                    AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                    builder.setView(view1);
                    builder.setCancelable(false).
                            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressBar = new ProgressDialog(FlashActivity.this);
                            progressBar.setCancelable(true);
                            progressBar.setMessage("Processing ...");
                            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressBar.setProgress(0);
                            progressBar.setMax(100);
                            progressBar.show();

                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                            jsonObject.addProperty("BookingId", extras.getString("TargetID"));
                            jsonObject.addProperty("Status", "1");

                            API apiService = createServiceHeader(API.class);
                            Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
                            call.enqueue(new Callback<LoginPojo>() {
                                @Override
                                public void onResponse(Call<LoginPojo> Requestcall, Response<LoginPojo> response) {
                                    progressBar.dismiss();
                                    switch (response.body().getCode()) {
                                        case "200":
                                            JsonObject jsonObject = new JsonObject();
                                            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                            jsonObject.addProperty("SenderType","1");
                                            jsonObject.addProperty("ReadNotification","1");
                                            JsonArray array = new JsonArray();
                                            array.add(NotificationId);
                                            jsonObject.add("NotificaitonIdArray", array);
                                            API apiService = createServiceHeader(API.class);
                                            Call<LoginPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                                            call.enqueue(new Callback<LoginPojo>() {
                                                @Override
                                                public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {

                                                    switch (response.body().getCode()) {
                                                        case "200":
                                                            int TempCount = Integer.parseInt(myPreference.getDefaultRunTimeValue()[8]);
                                                            if (TempCount>0){
                                                                TempCount = --TempCount;
                                                            }else {
                                                                TempCount = 0;
                                                            }
                                                            //Store Notification count in SharedPreferences
                                                            SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                            editor.putString("NotificationCount", String.valueOf(TempCount));
                                                            Log.i("NotificationCount",String.valueOf(TempCount));
                                                            editor.apply();
                                                            //Update Notification Count in bell icon
                                                            Intent pushNotification = new Intent(Config.NOTIFICATION_ICON);
                                                            LocalBroadcastManager.getInstance(FlashActivity.this).sendBroadcast(pushNotification);
                                                            Toast.makeText(FlashActivity.this, "Pooja accepted successfully.", Toast.LENGTH_SHORT).show();
                                                            Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                            startActivity(i);
                                                            break;
                                                        case "108":
                                                            Intent intent = new Intent(FlashActivity.this, LoginActivity.class);
                                                            startActivity(intent);
                                                        default:
                                                            myPreference.ShowDialog(FlashActivity.this,"Oops",response.body().getMessage());
                                                            break;
                                                    }

                                                }

                                                @Override
                                                public void onFailure(Call<LoginPojo> call, Throwable t) {
                                                    try {
                                                        myPreference.ShowDialog(FlashActivity.this,"Oops",t.getMessage());
                                                    } catch (NullPointerException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            });
                                            break;
                                        case "108":
                                            Toast.makeText(FlashActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(FlashActivity.this, LoginActivity.class));
                                            break;
                                        default:
                                            AlertDialog.Builder imageDialog = new AlertDialog.Builder(FlashActivity.this, R.style.RajCustomDialog);
                                            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                                            View dialogView = inflater.inflate(R.layout.custom_failor_dialog1, null);
                                            imageDialog.setView(dialogView);
                                            TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                            TextviewSmallTitle1.setText(response.body().getMessage());
                                            TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                            TextviewBigTitle.setText("Oops");
                                            Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                            ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                    startActivity(i);
                                                    SuccessDialog.dismiss();
                                                }
                                            });
                                            SuccessDialog = imageDialog.create();
                                            SuccessDialog.show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<LoginPojo> call, Throwable t) {
                                    progressBar.dismiss();
                                    try {
                                        myPreference.ShowDialog(FlashActivity.this,"Exception",t.getMessage());

                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            seekBar.setProgress(1);
                        }
                    });

                    AlertDialog alertDialog1 = builder.create();
                    alertDialog1.show();
                }else if (progress == 0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                    builder.setMessage("Are you sure? \nYou going to reject this pooja??");
                    builder.setIcon(R.mipmap.ic_launcher_round);
                    builder.setCancelable(false).
                            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    progressBar = new ProgressDialog(FlashActivity.this);
                                    progressBar.setCancelable(true);
                                    progressBar.setMessage("Processing ...");
                                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    progressBar.setProgress(0);
                                    progressBar.setMax(100);
                                    progressBar.show();

                                    JsonObject jsonObject = new JsonObject();
                                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                    jsonObject.addProperty("BookingId", extras.getString("TargetID"));
                                    jsonObject.addProperty("Status", "2");
                                    Log.e("JSON", jsonObject.toString());
                                    API apiService = createServiceHeader(API.class);
                                    Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
                                    call.enqueue(new Callback<LoginPojo>() {
                                        @Override
                                        public void onResponse(Call<LoginPojo> Requestcall, Response<LoginPojo> response) {
                                            progressBar.dismiss();
                                            switch (response.body().getCode()) {
                                                case "200":
                                                    JsonObject jsonObject = new JsonObject();
                                                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                                    jsonObject.addProperty("SenderType","1");
                                                    jsonObject.addProperty("ReadNotification","1");
                                                    JsonArray array = new JsonArray();
                                                    array.add(NotificationId);
                                                    jsonObject.add("NotificaitonIdArray", array);
                                                    API apiService = createServiceHeader(API.class);
                                                    Call<LoginPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                                                    call.enqueue(new Callback<LoginPojo>() {
                                                        @Override
                                                        public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {

                                                            switch (response.body().getCode()) {
                                                                case "200":
                                                                    int TempCount = Integer.parseInt(myPreference.getDefaultRunTimeValue()[8]);
                                                                    if (TempCount>0){
                                                                        TempCount = --TempCount;
                                                                    }else {
                                                                        TempCount = 0;
                                                                    }
                                                                    //Store Notification count in SharedPreferences
                                                                    SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                                    editor.putString("NotificationCount", String.valueOf(TempCount));
                                                                    Log.i("NotificationCount",String.valueOf(TempCount));
                                                                    editor.apply();
                                                                    //Update Notification Count in bell icon
                                                                    Intent pushNotification = new Intent(Config.NOTIFICATION_ICON);
                                                                    LocalBroadcastManager.getInstance(FlashActivity.this).sendBroadcast(pushNotification);
                                                                    Toast.makeText(FlashActivity.this, "Pooja was rejected successfully.", Toast.LENGTH_SHORT).show();
                                                                    Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                                    startActivity(i);
                                                                    break;
                                                                case "108":
                                                                    Intent intent = new Intent(FlashActivity.this, LoginActivity.class);
                                                                    startActivity(intent);
                                                                default:
                                                                    myPreference.ShowDialog(FlashActivity.this,"Oops",response.body().getMessage());
                                                                    break;
                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<LoginPojo> call, Throwable t) {
                                                            try {
                                                                myPreference.ShowDialog(FlashActivity.this,"Oops", "Something went wrong!!!");
                                                            } catch (NullPointerException e) {
                                                                e.printStackTrace();
                                                            }

                                                        }
                                                    });
                                                    break;
                                                case "108":
                                                    Toast.makeText(FlashActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(FlashActivity.this, LoginActivity.class));
                                                    break;
                                                default:
                                                    AlertDialog.Builder imageDialog = new AlertDialog.Builder(FlashActivity.this, R.style.RajCustomDialog);
                                                    LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                                                    View dialogView = inflater.inflate(R.layout.custom_failor_dialog1, null);
                                                    imageDialog.setView(dialogView);
                                                    TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                                    TextviewSmallTitle1.setText(response.body().getMessage());
                                                    TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                                    TextviewBigTitle.setText("Oops");
                                                    Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                                    ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            Intent i = FlashActivity.this.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                                                            startActivity(i);
                                                            SuccessDialog.dismiss();
                                                        }
                                                    });
                                                    SuccessDialog = imageDialog.create();
                                                    SuccessDialog.show();

                                            }
                                        }
                                        @Override
                                        public void onFailure(Call<LoginPojo> call, Throwable t) {
                                            progressBar.dismiss();
                                            try {
                                                myPreference.ShowDialog(FlashActivity.this,"Exception",t.getMessage());

                                            } catch (NullPointerException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            seekBar.setProgress(1);
                        }
                    });

                    AlertDialog alertDialog1 = builder.create();
                    alertDialog1.show();
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public Drawable getMyDrawable(int value){
        thumbView = LayoutInflater.from(FlashActivity.this).inflate(R.layout.layout_seekbar_thumb, null, false);
        switch (value){
            case 0:
                ((ImageView) thumbView.findViewById(R.id.ImageViewIcon)).setImageResource(R.mipmap.ic_close);
                break;
            case 2:
                ((ImageView) thumbView.findViewById(R.id.ImageViewIcon)).setImageResource(R.mipmap.ic_tick);
                break;
            default:
                ((ImageView) thumbView.findViewById(R.id.ImageViewIcon)).setImageResource(R.drawable.ic_user_white);
        }

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);
        return new BitmapDrawable(getResources(), bitmap);
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }
}
