package panditapp.codegen.com.panditapp.Pojo;

public class SearchAreaListResponseAPI {
    private String AreaId;

    private String AreaName;

    public String getAreaId ()
    {
        return AreaId;
    }

    public void setAreaId (String AreaId)
    {
        this.AreaId = AreaId;
    }

    public String getAreaName ()
    {
        return AreaName;
    }

    public void setAreaName (String AreaName)
    {
        this.AreaName = AreaName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [AreaId = "+AreaId+", AreaName = "+AreaName+"]";
    }
}
