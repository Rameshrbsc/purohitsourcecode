package panditapp.codegen.com.panditapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import panditapp.codegen.com.panditapp.Activity.ProfileEditActivity;
import panditapp.codegen.com.panditapp.R;

public class DropDownListAdapter extends BaseAdapter {
    private Context context;
    ViewHolder holder;
    TextView TextviewSkills;
    private String[] items;

    public DropDownListAdapter(ProfileEditActivity profileEditActivity, String[] items, TextView textViewSkillsDropDownBox) {
        this.context = profileEditActivity;
        this.items =items;
        this.TextviewSkills = textViewSkillsDropDownBox;

    }


    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView SelectOption;
        CheckBox checkbox;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.drop_down_list_row, viewGroup, false);
            holder = new ViewHolder();
            holder.checkbox = (CheckBox) view.findViewById(R.id.checkbox);
            holder.SelectOption = (TextView)view.findViewById(R.id.SelectOption);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.SelectOption.setText(items[position]);
        ;
       holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @SuppressLint("DefaultLocale")
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               int count = 0;
               if(isChecked){
                   count++;
               }else{
                   count--;
               }
               TextviewSkills.setText(String.format("%d", count));
           }
       });




        return view;
    }
}
