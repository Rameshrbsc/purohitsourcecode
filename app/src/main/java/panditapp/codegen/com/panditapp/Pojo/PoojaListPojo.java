package panditapp.codegen.com.panditapp.Pojo;

public class PoojaListPojo {
    private String Message;

    private PoojaListResponsePojo[] Response;

    private String Code;

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public PoojaListResponsePojo[] getResponse ()
    {
        return Response;
    }

    public void setResponse (PoojaListResponsePojo[] Response)
    {
        this.Response = Response;
    }

    public String getCode ()
    {
        return Code;
    }

    public void setCode (String Code)
    {
        this.Code = Code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Message = "+Message+", Response = "+Response+", Code = "+Code+"]";
    }
    
}
