package panditapp.codegen.com.panditapp.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.BuildConfig;
import panditapp.codegen.com.panditapp.Pojo.GoogleMapDirection;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.FloatingOverMapIconService;
import panditapp.codegen.com.panditapp.Receiver.SupportTrackingReceiver;
import panditapp.codegen.com.panditapp.SupportClass.DirectionsJSONParser;
import panditapp.codegen.com.panditapp.SupportClass.MyLocation;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createService;
import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class TrackingActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    String BookingID, CustomerLatitude, CustomerLongitude, CustomerName, MyLatitude, MyLongitude,
            CustomerAddress, CustomerNumber, LocationAccessToken;

    MyPreference myPreference;

    private ProgressDialog progressBar;
    Polyline polyline = null;

    //Bind LinearLayout
    @BindView(R.id.bottom_sheet)
    LinearLayout bottom_sheet;
    BottomSheetBehavior sheetBehavior;

    //Bind ImageView
    @BindView(R.id.ImageViewCall)
    ImageView ImageViewCall;
    @BindView(R.id.ImageViewNavigate)
    ImageView ImageViewNavigate;
    @BindView(R.id.ImageViewAbort)
    ImageView ImageViewAbort;
    @BindView(R.id.ImageViewFinish)
    ImageView ImageViewFinish;

    //Bind TextView
    @BindView(R.id.TextViewAddress)
    TextView TextViewAddress;
    @BindView(R.id.TextViewName)
    TextView TextViewName;
    @BindView(R.id.TextViewNavigate)
    TextView TextViewNavigate;
    @BindView(R.id.TextViewAbort)
    TextView TextViewAbort;
    @BindView(R.id.TextViewFinish)
    TextView TextViewFinish;

    String[] permissionsRequired = new String[]{Manifest.permission.CALL_PHONE};
    private static final int REQUEST_PERMISSION_SETTING = 151;
    private static final int REQUEST_PHONE_ACCESS = 150;

    private boolean IntentFromGMap = false;
    private boolean ImageViewNavigateCheck = false;
    private int MapRequestCode = 305, SettingRequestCode = 4545, REQUEST_CALL_ACCESS = 306;

    private BroadcastReceiver mGetServiceClick;

    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // bind the view using butterknife
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("BookingID")) {
            BookingID = extras.getString("BookingID");
            CustomerLatitude = extras.getString("CustomerLatitude");
            CustomerLongitude = extras.getString("CustomerLongitude");
            CustomerName = extras.getString("CustomerName");
            TextViewName.setText(CustomerName);
            CustomerAddress = extras.getString("CustomerAddress");
            TextViewAddress.setText(CustomerAddress);
            MyLatitude = extras.getString("MyLatitude");
            MyLongitude = extras.getString("MyLongitude");
            CustomerNumber = extras.getString("CustomerNumber");
            LocationAccessToken = extras.getString("LocationAccessToken");
        }

        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        myPreference = new MyPreference(TrackingActivity.this);

        ImageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CustomerNumber != null && !CustomerNumber.equals("") && !CustomerNumber.equals("null") && isValidMobile(CustomerNumber)){
                    ActivityCompat.requestPermissions(TrackingActivity.this, permissionsRequired, REQUEST_PHONE_ACCESS);
//                    // Requesting ACCESS_FINE_LOCATION using Dexter library
//                    Dexter.withActivity(TrackingActivity.this)
//                            .withPermission(Manifest.permission.CALL_PHONE)
//                            .withListener(new PermissionListener() {
//                                @Override
//                                public void onPermissionGranted(PermissionGrantedResponse response) {
//                                    Intent intent = new Intent(Intent.ACTION_CALL);
//                                    intent.setData(Uri.parse("tel:" + CustomerNumber));
//                                    if (ActivityCompat.checkSelfPermission(TrackingActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                                        // TODO: Consider calling
//                                        //    ActivityCompat#requestPermissions
//                                        // here to request the missing permissions, and then overriding
//                                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                        //                                          int[] grantResults)
//                                        // to handle the case where the user grants the permission. See the documentation
//                                        // for ActivityCompat#requestPermissions for more details.
//                                        return;
//                                    }
//                                    startActivity(intent);
//                                }
//                                @Override
//                                public void onPermissionDenied(PermissionDeniedResponse response) {
//                                    // open device settings when the permission is
//                                    // denied permanently
//                                    Intent intent = new Intent();
//                                    intent.setAction(
//                                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                    Uri uri = Uri.fromParts("package",
//                                            BuildConfig.APPLICATION_ID, null);
//                                    intent.setData(uri);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivityForResult(intent, REQUEST_CALL_ACCESS);
//                                }
//
//                                @Override
//                                public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {
//                                    token.continuePermissionRequest();
//                                }
//                            }).check();
                }

            }
        });

        ImageViewNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    // if OS is pre-marshmallow then create the floating icon, no permission is needed
                    createFloatingBackButton();
                } else {
                    if (!Settings.canDrawOverlays(TrackingActivity.this)) {
                        // asking for DRAW_OVER permission in settings
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getApplicationContext().getPackageName()));
                        startActivityForResult(intent, SettingRequestCode);
                    } else {
                        createFloatingBackButton();
                    }
                }
            }
        });

        ImageViewAbort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar = new ProgressDialog(TrackingActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Connecting to server ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("BookingId", BookingID);
                jsonObject.addProperty("Latitude", MyLatitude);
                jsonObject.addProperty("Longitude", MyLongitude);
                jsonObject.addProperty("CurrentTime", new SimpleDateFormat("kk:mm:ss", Locale.getDefault()).format(new Date()));
                jsonObject.addProperty("Status", "2");
                jsonObject.addProperty("LocationAccessToken", myPreference.getLocationAccessToken());


                API apiService = createServiceHeader(API.class);
                Call<LoginPojo> TrackingCall = apiService.TRACKING_POJO_CALL(jsonObject);
                TrackingCall.enqueue(new Callback<LoginPojo>() {
                    @Override
                    public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                Intent myIntent = new Intent(TrackingActivity.this, SupportTrackingReceiver.class);
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(TrackingActivity.this, 100, myIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);

                                assert alarmManager != null;
                                alarmManager.cancel(pendingIntent);
                                onBackPressed();
                                break;
                            case "108":
                                Toast.makeText(TrackingActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(TrackingActivity.this, LoginActivity.class));
                                break;
                            default:
                                myPreference.ShowDialog(TrackingActivity.this,"Oops",response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(TrackingActivity.this,"Exception",t.getMessage());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        ImageViewFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar = new ProgressDialog(TrackingActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Connecting to server ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("BookingId", BookingID);
                jsonObject.addProperty("Latitude", MyLatitude);
                jsonObject.addProperty("Longitude", MyLongitude);
                jsonObject.addProperty("CurrentTime", new SimpleDateFormat("kk:mm:ss", Locale.getDefault()).format(new Date()));
                jsonObject.addProperty("Status", "0");
                jsonObject.addProperty("LocationAccessToken", myPreference.getLocationAccessToken());


                API apiService = createServiceHeader(API.class);
                Call<LoginPojo> TrackingCall = apiService.TRACKING_POJO_CALL(jsonObject);
                TrackingCall.enqueue(new Callback<LoginPojo>() {
                    @Override
                    public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                Intent myIntent = new Intent(TrackingActivity.this, SupportTrackingReceiver.class);
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(TrackingActivity.this, 100, myIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);

                                assert alarmManager != null;
                                alarmManager.cancel(pendingIntent);
                                onBackPressed();
                                break;
                            case "108":
                                Toast.makeText(TrackingActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(TrackingActivity.this, LoginActivity.class));
                                break;
                            default:
                                myPreference.ShowDialog(TrackingActivity.this,"Oops",response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(TrackingActivity.this,"Exception",t.getMessage());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        mGetServiceClick = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Intent selfIntent = new Intent(TrackingActivity.this, TrackingActivity.class);
                selfIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(selfIntent);

                Bundle bundle = intent.getExtras();
                if (bundle != null && bundle.containsKey("BookingID")) {

                    progressBar = new ProgressDialog(TrackingActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Getting your location ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
                        @Override
                        public void gotLocation(Location location) {
                            //Got the location!
                            progressBar.dismiss();
                            Log.i("Latitude", String.valueOf(location.getLatitude()));
                            Log.i("Longitude", String.valueOf(location.getLongitude()));
                            MyLatitude = String.valueOf(location.getLatitude());//13.008906 //13.990483
                            MyLongitude = String.valueOf(location.getLongitude());//80.217450 //77.527165
                            if (polyline != null){
                                polyline.remove();
                            }
                            if (mMap != null){
                                MapIsReadyToUse(mMap);
                                API apiService = createService(API.class);
                                Call<GoogleMapDirection> DirectionCall = apiService.GOOGLE_MAP_DIRECTION_CALL(MyLatitude+","+MyLongitude, CustomerLatitude+","+CustomerLongitude, "AIzaSyChzRKOIuTzIGf2vSHqSkURNUM_Tv9vNiE");
                                DirectionCall.enqueue(new Callback<GoogleMapDirection>() {
                                    @Override
                                    public void onResponse(Call<GoogleMapDirection> call, Response<GoogleMapDirection> response) {
                                        if (response.body().getStatus().equals("OK")){
                                            if (!response.body().getRoutes()[0].getLegs()[0].getDistance().getValue().isEmpty() && !response.body().getRoutes()[0].getLegs()[0].getDistance().getValue().equals("0")){
                                                Log.e("Current Distance",response.body().getRoutes()[0].getLegs()[0].getDistance().getText());
                                                if ((Integer.parseInt(response.body().getRoutes()[0].getLegs()[0].getDistance().getValue().trim()))<=100){
                                                    ImageViewFinish.setVisibility(View.VISIBLE);
                                                    TextViewFinish.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<GoogleMapDirection> call, Throwable t) {

                                    }
                                });
                            }

                        }};

                    MyLocation myLocation = new MyLocation(getApplication());
                    myLocation.getLocation(TrackingActivity.this, locationResult);
                }
                Log.e("Stage", "Inside the Service Call back");
            }
        };
    }

    private void createFloatingBackButton() {
        //Backend service to create FloatingButton
        Intent iconServiceIntent = new Intent(TrackingActivity.this, FloatingOverMapIconService.class);
        iconServiceIntent.putExtra("RIDE_ID", "924552");
        iconServiceIntent.putExtra("BookingID", getIntent().getExtras().getString("BookingID"));
        iconServiceIntent.putExtra("MyLatitude", MyLatitude);
        iconServiceIntent.putExtra("MyLongitude", MyLongitude);
        iconServiceIntent.putExtra("CustomerLatitude", CustomerLatitude);
        iconServiceIntent.putExtra("CustomerLongitude", CustomerLongitude);
        iconServiceIntent.putExtra("CustomerName", CustomerName);
        iconServiceIntent.putExtra("CustomerNumber", CustomerNumber);
        iconServiceIntent.putExtra("CustomerAddress", CustomerAddress);
        iconServiceIntent.putExtra("LocationAccessToken", LocationAccessToken);

        //Google Map request navigation call
        IntentFromGMap = true;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,Uri.parse("google.navigation:q="+ CustomerAddress));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivityForResult(intent, MapRequestCode);
        //Uri.parse("http://maps.google.com/maps?daddr="+CustomerLatitude+","+CustomerLongitude)


        startService(iconServiceIntent);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //We need common function for this
        //In order to handle OnActivityResult functionality
        MapIsReadyToUse(googleMap);
    }

    private void MapIsReadyToUse(GoogleMap googleMap) {
        mMap = googleMap;

        // Always focus the camera to user current location
        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(Double.parseDouble(MyLatitude), Double.parseDouble(MyLongitude))).zoom(15).build();

        // create marker
        MarkerOptions marker = new MarkerOptions()
                .position(new LatLng(Double.parseDouble(CustomerLatitude), Double.parseDouble(CustomerLongitude)))
                .title(CustomerName)
                .snippet(CustomerLatitude +" "+CustomerLongitude).rotation(1.5f).anchor(0.5f, 0.5f).flat(true);

        // Changing marker icon
        marker.icon(bitmapDescriptorFromVector1(TrackingActivity.this,R.drawable.ic_house));

        // adding marker
        mMap.addMarker(marker);

        mMap.getUiSettings().setMapToolbarEnabled(false);

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if (ActivityCompat.checkSelfPermission(TrackingActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(TrackingActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        LatLng dest = new LatLng(Double.parseDouble(CustomerLatitude), Double.parseDouble(CustomerLongitude));
        final LatLng origin = new LatLng(Double.parseDouble(MyLatitude), Double.parseDouble(MyLongitude));
        String url = getDirectionsUrl(origin, dest);
        Log.d("MapRequest", url);
        DownloadTask downloadTask = new DownloadTask();

        downloadTask.execute(url);
    }

    private BitmapDescriptor bitmapDescriptorFromVector1(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        return "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters+ "&key=AIzaSyChzRKOIuTzIGf2vSHqSkURNUM_Tv9vNiE";
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(TrackingActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Loading ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
        }

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressBar.dismiss();

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception in url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(TrackingActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Getting Live Location ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            progressBar.dismiss();
            ArrayList<LatLng> points = new ArrayList<LatLng>();
            PolylineOptions lineOptions = new PolylineOptions();
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(20);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            if (polyline != null){
                polyline.remove();
                polyline = mMap.addPolyline(lineOptions);
            }else {
                polyline = mMap.addPolyline(lineOptions);
            }
            KeepTrackFunction();
        }
    }

    public void KeepTrackFunction(){
        Intent myIntent = new Intent(this, SupportTrackingReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 100, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        assert manager != null;
        long time = System.currentTimeMillis();
        manager.setRepeating(AlarmManager.RTC_WAKEUP,  time, 60000, pendingIntent);
        Log.d("RequestingTime ", new Date().toString());
        // AlarmManager.INTERVAL_DAY  and 24 * 60 * 60 * 1000 are equal

    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MapRequestCode) {
            // no result is returned by google map, as google don't provide any apis or documentation
            // for it.

        }else if (requestCode == SettingRequestCode){
            // as permissions from Settings don't provide any callbacks, hence checking again for the permission
            // so that we can draw our floating without asking user to click on the previously clicked view
            // again
            if (Settings.canDrawOverlays(this)) {
                createFloatingBackButton();
            } else {
                //permission is not provided by user, do your task
                //GlobalVariables.alert(mContext, "This permission is necessary for this application's functioning");
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Kindly grand permission to overlay this app on Google Maps\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                                    // if OS is pre-marshmallow then create the floating icon, no permission is needed
                                    createFloatingBackButton();
                                } else {
                                    if (!Settings.canDrawOverlays(TrackingActivity.this)) {
                                        // asking for DRAW_OVER permission in settings
                                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                                Uri.parse("package:" + getApplicationContext().getPackageName()));
                                        startActivityForResult(intent, SettingRequestCode);
                                    } else {
                                        createFloatingBackButton();
                                    }
                                }
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }else if (requestCode == REQUEST_PERMISSION_SETTING){
            if (checkPermissions()){
                ContinueToCall();
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PHONE_ACCESS){
            if (checkPermissions()){
                ContinueToCall();
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(TrackingActivity.this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Phone Call access permission is required to call the customer from application. Enable it to call\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openSettings();
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

    void ContinueToCall(){
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + CustomerNumber));
        if (ActivityCompat.checkSelfPermission(TrackingActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", TrackingActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);

    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Stage", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mGetServiceClick,
                new IntentFilter(FloatingOverMapIconService.BROADCAST_ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGetServiceClick);
        Log.e("Stage", "onDestroy");
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
}
