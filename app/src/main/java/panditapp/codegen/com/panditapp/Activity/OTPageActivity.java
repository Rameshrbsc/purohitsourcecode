package panditapp.codegen.com.panditapp.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.PurohitResendOTPPojo;
import panditapp.codegen.com.panditapp.Pojo.SendOTPPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class OTPageActivity extends AppCompatActivity {
    @BindView(R.id.TextViewTitle)
    TextView TextViewTitle;
    @BindView(R.id.TextViewLoginClickTitle)
    TextView TextViewLoginClickTitle;
    @BindView(R.id.EditTextUserOTP)
    EditText EditTextUserOTP;
    @BindView(R.id.ButtonVeriftyOTP)
    Button ButtonVeriftyOTP;
    @BindView(R.id.ButtonResendOTP)
    Button ButtonResendOTP;
    String first,next,first1,next1,AccessToken,OTP,newOTP,OTPgotForgetNewPassword,TempAccessToken,MoblieNumber;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile"  , CreatedBy, NotificationCount, ForgetPassword = null;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purohit_otpage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPreference = new MyPreference(OTPageActivity.this);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        try{
            if(intent != null){
//                    OTP = intent.getExtras().get("OTPgot").toString();
                TempAccessToken = intent.getExtras().get("TempAccessToken").toString();
                MoblieNumber = intent.getExtras().get("MoblieNumber").toString();
                 CreatedBy = intent.getExtras().get("CreatedBy").toString();
                NotificationCount = intent.getExtras().getString("NotificationCount");
                ForgetPassword = intent.getExtras().getString("ForgetPassword");

            }else{
                Toast.makeText(this, "Intent null", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){e.printStackTrace();}


        first = "We have sent an OTP via SMS to ";
        next = "<font color='#F08400'>"+MoblieNumber+"</font>";
        TextViewTitle.setText(Html.fromHtml(first + next));
        first1 = "\u2190 Back to login Please ";
        next1 = "<font color='#F08400'> click here?</font>";
        TextViewLoginClickTitle.setText(Html.fromHtml(first1 + next1));
        TextViewLoginClickTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ButtonVeriftyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()) {

                    progressBar = new ProgressDialog(OTPageActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                    jsonObject.addProperty("Otp", EditTextUserOTP.getText().toString());
                    jsonObject.addProperty("AccessToken",TempAccessToken);
                    if (ForgetPassword != null){
                        jsonObject.addProperty("ForgetPassword",ForgetPassword);
                    }else {
                        jsonObject.addProperty("ForgetPassword", "0");
                    }
                    API apiService = createServiceHeader(API.class);
                    Log.e("JSON", jsonObject.toString());
                    Call<SendOTPPojo> call = apiService.SEND_OTP_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<SendOTPPojo>() {
                        @Override
                        public void onResponse(Call<SendOTPPojo> call, Response<SendOTPPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
                                    if(CreatedBy == null){


//                                        final Dialog dialog = new Dialog(OTPageActivity.this);
//                                        dialog.setContentView(R.layout.custom_dialog_success);
//                                        Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
//                                        TextView TextviewBigTitle = (TextView)dialog.findViewById(R.id.TextviewBigTitle);
//                                        TextviewBigTitle.setText("Thank You");
//                                        TextView TextviewSmallTitle1 = (TextView)dialog.findViewById(R.id.TextviewSmallTitle1);
//                                        TextviewSmallTitle1.setText(response.body().getMessage());
//                                        dialogButton.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//                                                dialog.dismiss();
//                                                Intent intent = new Intent(OTPageActivity.this, LoginActivity.class);
//                                                startActivity(intent);
//
//                                            }
//                                        });
//                                        dialog.show();

                                        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(OTPageActivity.this, R.style.RajCustomDialog);
                                        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                                        final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
                                        imageDialog.setView(dialogView);
                                        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                        TextviewSmallTitle1.setText(response.body().getMessage());
                                        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                        TextviewBigTitle.setText("Thank You");
                                        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                        ButtonOk.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                                Intent intent = new Intent(OTPageActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                            }
                                        });
                                        dialog = imageDialog.create();
                                        dialog.show();

                                    }else{
                                        Intent intent1 = new Intent(OTPageActivity.this,NavigationActivity.class);
                                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                        editor.putString("AccessToken", TempAccessToken);
                                        editor.putString("DeviceId", displayFirebaseRegId());
                                        editor.putString("NotificationCount", NotificationCount);
                                        editor.apply();
                                        startActivity(intent1);
                                    }

                                    break;
                                case "108":
                                    Intent intent = new Intent(OTPageActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(OTPageActivity.this,"Oops",response.body().getMessage());

                                    break;


                            }
                        }

                        @Override
                        public void onFailure(Call<SendOTPPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                myPreference.ShowDialog(OTPageActivity.this,"Oops",t.getMessage());

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }else {
                    final Dialog dialog = new Dialog(OTPageActivity.this);
                    dialog.setContentView(R.layout.custom_failor_dialog);
                    Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
                    TextView TextviewSmallTitle1 = (TextView)dialog.findViewById(R.id.TextviewSmallTitle1);
                    TextviewSmallTitle1.setText("There is no internet connection");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                }

            }
        });

        ButtonResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()) {
                    progressBar = new ProgressDialog(OTPageActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId",displayFirebaseRegId());
                    jsonObject.addProperty("AccessToken", TempAccessToken);
                    API apiService = createServiceHeader(API.class);
                    Call<PurohitResendOTPPojo> call = apiService.RESEND_OTP_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<PurohitResendOTPPojo>() {
                        @Override
                        public void onResponse(Call<PurohitResendOTPPojo> call, Response<PurohitResendOTPPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {

                                case "200":
                                    Log.i("newOTP",""+response.body().getMessage());
//                                    newOTP = response.body().getOtp();
                                    break;
                                case "108":
                                    Intent intent = new Intent(OTPageActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(OTPageActivity.this,"Oops",response.body().getMessage());

                                    break;


                            }
                        }

                        @Override
                        public void onFailure(Call<PurohitResendOTPPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                myPreference.ShowDialog(OTPageActivity.this,"Oops",t.getMessage());
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }else {
                    myPreference.ShowDialog(OTPageActivity.this,"Oops!!","There is no internet connection");

                }
            }
        });


    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e("OTP Activity", "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e("OTP Activity", "Firebase Reg Id is not received yet!");
        return regId;
    }

}
