package panditapp.codegen.com.panditapp.SupportClass;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.R;

import static android.content.Context.MODE_PRIVATE;

public class MyPreference {

    private Context context;
    private AlertDialog dialog;

    public static String MY_PREFS_NAME = "MyPrefsFile", AccessToken = null, DeviceId = null, MY_LOCATION = "LocationAccessToken";

    public MyPreference(Context context){
        this.context = context;
    }

    public String[] getDefaultRunTimeValue() {
        String[] Value = new String[9];
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        String restoredText = prefs.getString("AccessToken", null);
        if (restoredText != null) {
            Value[0] = prefs.getString("AccessToken", "No AccessToken Defined");//"No AccessToken Defined" is the default value.
            Value[1] = prefs.getString("DeviceId", "No DeviceId Defined");//"No DeviceId Defined" is the default value.
            Value[2] = prefs.getString("AccessType", "Blank Value");
            Value[3] = prefs.getString("ProfilePic","");
            Value[4] = prefs.getString("MoblieNumber","");
            Value[5] = prefs.getString("Password","");
            Value[6] = prefs.getString("CheckBoxTrue","0");
            Value[7] = prefs.getString("FacebookLogout","fblogout");
            Value[8] = prefs.getString("NotificationCount","0");




        }
        return Value;
    }

    public String getLocationAccessToken() {
        String Value = null;
        SharedPreferences prefs = context.getSharedPreferences(MY_LOCATION, context.MODE_PRIVATE);
        String restoredText = prefs.getString("AccessToken", null);
        if (restoredText != null) {
            Value = prefs.getString("AccessToken", "No AccessToken Defined");//"No AccessToken Defined" is the default value.
        }
        return Value;
    }

    public final boolean isInternetOn() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                // Toast.makeText(getApplicationContext(), activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                //Toast.makeText(getApplicationContext(), activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return false;
    }

    public boolean checkEditTextEmpty(EditText editText){
        return editText.getText().toString().trim().length() > 0;
    }

    public boolean isValidEmail(EditText editText) {
        return Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString()).matches();
    }

    public void ShowDialog(Context context, String Title, String message) {
        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.custom_failor_dialog1, null);
        imageDialog.setView(dialogView);
        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
        TextviewSmallTitle1.setText(message);
        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
        TextviewBigTitle.setText(Title);
        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
        ButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = imageDialog.create();
        dialog.show();
    }


    public void ShowSuccessDialog(Context context ,String Title,String message){

        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
        imageDialog.setView(dialogView);
        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
        TextviewSmallTitle1.setText(message);
        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
        TextviewBigTitle.setText(Title);
        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
        ButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog = imageDialog.create();
        dialog.show();

    }

    public void setAllBookingDetails(Context mContext,
                                     String saveAlldetails) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("save", saveAlldetails);
        prefsEditor.commit();
    }

    public String getAllBookingDetails(Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String inactiveAccounts = mPrefs.getString("save", "");
        return inactiveAccounts;
    }

    public void ShowSuccessDialognew(final Context context , String Title, String message){

        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
        imageDialog.setView(dialogView);
        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
        TextviewSmallTitle1.setText(message);
        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
        TextviewBigTitle.setText(Title);
        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
        ButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog = imageDialog.create();
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

    }
}
