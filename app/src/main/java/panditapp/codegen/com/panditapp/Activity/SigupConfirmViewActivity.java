package panditapp.codegen.com.panditapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.SignUpPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;


public class SigupConfirmViewActivity extends AppCompatActivity {
    @BindView(R.id.TextViewUserName)
    TextView TextViewUserName;
    @BindView(R.id.TextViewUserEmailID)
    TextView TextViewUserEmailID;
    @BindView(R.id.TextViewUserPassword)
    TextView TextViewUserPassword;
    @BindView(R.id.TextViewUserMoblieNumber)
    TextView TextViewUserMoblieNumber;
    @BindView(R.id.TextViewUserNatchtathiram)
    TextView TextViewUserNatchtathiram;
    @BindView(R.id.TextViewUserGothram)
    TextView TextViewUserGothram;
    @BindView(R.id.TextViewUserSection)
    TextView TextViewUserSection;
    @BindView(R.id.TextViewUserAadharNumber)
    TextView TextViewUserAadharNumber;
    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;
    @BindView(R.id.TextViewPancard)
    TextView TextViewPancard;
    @BindView(R.id.TextViewUserLanguage)
    TextView TextViewUserLanguage;
    private ProgressDialog progressBar;
    MyPreference myPreference;
    String EmailIDsaved,EmailIDToken, SocialType,encodedImage, UserName, UserEmailID, UserMoblieNumber,UserPassword, UserNatchtathiram,UserGothram, UserSection, UserAadhar,TempUri,UserLanguage,UserPanCard;
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    Uri tempUri;
    String MY_PREFS_NAME = "MyPrefsFile",UserPicURI;
    File  file;
    private static final String TAG = SigupConfirmViewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sigup_confirm_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("SignUp Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);
        final Intent intent = getIntent();
        try{
            boolean boo  =  intent.getExtras().getBoolean("_fbSourceApplicationHasBeenSet");
//            if( boo == true && EmailIDToken!=null && EmailIDsaved !=null && SocialType!=null){
            if(boo == true ){
                EmailIDsaved = intent.getExtras().get("EmailID").toString();
                EmailIDToken = intent.getExtras().get("EmailIDUnique").toString();
                SocialType = intent.getExtras().get("SocialType").toString();
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        SharedPreferences myperf=this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        UserName = myperf.getString("PurohitName", "Default");
        UserEmailID = myperf.getString("PurohitEmailID", "Default");
        UserMoblieNumber = myperf.getString("PurohitMoblieNumber", "Default");
        UserPassword = myperf.getString("PurohitPassword", "Default");
        UserNatchtathiram = myperf.getString("PurohitNatchtathiram", "Default");
        UserGothram = myperf.getString("PurohitGothram", "Default");
        UserSection = myperf.getString("PurohitSection", "Default");
        UserAadhar = myperf.getString("PurohitAadhar", "Default");
        UserPanCard = myperf.getString("PurohitPanCard", "Default");
        UserLanguage = myperf.getString("PurohitLanguage", "Default");
        TempUri = myperf.getString("ImageUriPath",null);
        try {
            file = new File(TempUri);
            Picasso.with(this).load(new File(TempUri)).transform(new SignUpActivity.CircleTransform()).into(ImageViewProfilePic);
            EncodedMethodCall();
        }catch (Exception e){
            e.printStackTrace();
        }

        TextViewUserName.setText(UserName);
        TextViewUserEmailID.setText(UserEmailID);
        TextViewUserPassword.setText(UserPassword);
        TextViewUserMoblieNumber.setText(UserMoblieNumber);
        TextViewUserNatchtathiram.setText(UserNatchtathiram);
        TextViewUserGothram.setText(UserGothram);
        TextViewUserSection.setText(UserSection);
        TextViewUserAadharNumber.setText(UserAadhar);
        TextViewPancard.setText(UserPanCard);
        TextViewUserLanguage.setText(UserLanguage);

        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myPreference.isInternetOn()) {//normal Submission
                    progressBar = new ProgressDialog(SigupConfirmViewActivity.this);
                    progressBar.setCancelable(false);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                    jsonObject.addProperty("PurohitName", UserName);
                    jsonObject.addProperty("PurohitEmailID",UserEmailID);
                    jsonObject.addProperty("PurohitPhoneNumber", UserMoblieNumber);
                    jsonObject.addProperty("PurohitPassword", UserPassword);
                    jsonObject.addProperty("PurohitSection", UserSection);//send section data
                    jsonObject.addProperty("PurohitNatcharam",UserNatchtathiram);
                    jsonObject.addProperty("PurohitGothram", UserGothram);
                    jsonObject.addProperty("PurohitAadharNumber", UserAadhar);
                    jsonObject.addProperty("PurohitPanNumber",UserPanCard);
                    jsonObject.addProperty("PurohitLanguage",UserLanguage);
                    if (encodedImage != null) {
                        jsonObject.addProperty("PurohitProfilePic", encodedImage);
                    }
                    else {
                        jsonObject.addProperty("PurohitProfilePic", "");
                    }
                    if(EmailIDsaved!=null){
                        jsonObject.addProperty("PurohitsocialId", EmailIDToken);
                        jsonObject.addProperty("PurohitsocialEmail", EmailIDsaved);
                        if(SocialType.equals("1")){
                            jsonObject.addProperty("PurohitsocialType", "1");
                        }else{
                            jsonObject.addProperty("PurohitsocialType", "2");
                        }

                    }else{
                        jsonObject.addProperty("PurohitsocialId", "");
                        jsonObject.addProperty("PurohitsocialEmail", "");
                        jsonObject.addProperty("PurohitsocialType", "");

                    }



                    API apiService = createServiceHeader(API.class);
                    Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<SignUpPojo>() {
                        @Override
                        public void onResponse(Call<SignUpPojo> call, Response<SignUpPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":
                                    Toast.makeText(SigupConfirmViewActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
//                                                    editor.putString("AccessToken", response.body().getAccessToken());
//                                                    editor.putString("DeviceId", Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID));
//                                                    String OTP = response.body().getOtp();
//                                                    Log.i("OTPget", "" + OTP);
//                                                    editor.apply();
                                    Intent intentOTP = new Intent(SigupConfirmViewActivity.this, OTPageActivity.class);
                                    intentOTP.putExtra("TempAccessToken",response.body().getAccessToken());
                                    intentOTP.putExtra("MoblieNumber",TextViewUserMoblieNumber.getText().toString());
                                    startActivity(intentOTP);
                                    break;

                                case "108":
                                    Toast.makeText(SigupConfirmViewActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(SigupConfirmViewActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    break;
                                default:
                                    myPreference.ShowDialog(SigupConfirmViewActivity.this,"Oops",response.body().getMessage());
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<SignUpPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                myPreference.ShowDialog(SigupConfirmViewActivity.this,"Oops",t.getMessage());
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }else{
                    progressBar.dismiss();
                    myPreference.ShowDialog(SigupConfirmViewActivity.this, "Oops", "There is no Internet Connection!!!");
                }
            }
        });

    }
    private void EncodedMethodCall() {
        Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 60, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        Log.i("encodedImage",""+encodedImage);


    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }

}
