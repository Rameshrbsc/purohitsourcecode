package panditapp.codegen.com.panditapp.Pojo;

public class AreaModel {
    private boolean isSelected;
    private String AreaName;
    private String AreaCode;
    private double lattitute;
    private double langitute;

    public boolean isSelected() {
        return isSelected;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        AreaCode = areaCode;
    }

    public double getLattitute() {
        return lattitute;
    }

    public void setLattitute(double lattitute) {
        this.lattitute = lattitute;
    }

    public double getLangitute() {
        return langitute;
    }

    public void setLangitute(double langitute) {
        this.langitute = langitute;
    }

    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        this.AreaName = areaName;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
