package panditapp.codegen.com.panditapp.Pojo;

public class PurohitSchedulingCalendarResponsePojo {
    private String UserPhoneNumber;

    private String Time;

    private String BookingId;

    private String BookingDate;

    private String Address;

    private String SubCatogeryName;

    private String UserName;

    private String CatogeryName;

    private String PoojaName;

    public String getUserPhoneNumber ()
    {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber (String UserPhoneNumber)
    {
        this.UserPhoneNumber = UserPhoneNumber;
    }

    public String getTime ()
    {
        return Time;
    }

    public void setTime (String Time)
    {
        this.Time = Time;
    }

    public String getBookingId ()
    {
        return BookingId;
    }

    public void setBookingId (String BookingId)
    {
        this.BookingId = BookingId;
    }

    public String getBookingDate ()
    {
        return BookingDate;
    }

    public void setBookingDate (String BookingDate)
    {
        this.BookingDate = BookingDate;
    }

    public String getAddress ()
    {
        return Address;
    }

    public void setAddress (String Address)
    {
        this.Address = Address;
    }

    public String getSubCatogeryName ()
    {
        return SubCatogeryName;
    }

    public void setSubCatogeryName (String SubCatogeryName)
    {
        this.SubCatogeryName = SubCatogeryName;
    }

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public String getCatogeryName ()
    {
        return CatogeryName;
    }

    public void setCatogeryName (String CatogeryName)
    {
        this.CatogeryName = CatogeryName;
    }

    public String getPoojaName ()
    {
        return PoojaName;
    }

    public void setPoojaName (String PoojaName)
    {
        this.PoojaName = PoojaName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UserPhoneNumber = "+UserPhoneNumber+", Time = "+Time+", BookingId = "+BookingId+", BookingDate = "+BookingDate+", Address = "+Address+", SubCatogeryName = "+SubCatogeryName+", UserName = "+UserName+", CatogeryName = "+CatogeryName+", PoojaName = "+PoojaName+"]";
    }
}
