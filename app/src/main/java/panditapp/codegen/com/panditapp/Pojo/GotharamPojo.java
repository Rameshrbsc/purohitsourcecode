package panditapp.codegen.com.panditapp.Pojo;

public class GotharamPojo {
    String  gothram_code;

    private Gothram_data[]  gothram_data;

    public String getGothram_code() {
        return gothram_code;
    }

    public void setGothram_code(String gothram_code) {
        this.gothram_code = gothram_code;
    }

    public Gothram_data[] getGothram_data() {
        return gothram_data;
    }

    public void setGothram_data(Gothram_data[] gothram_data) {
        this.gothram_data = gothram_data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [gothram_code = "+gothram_code+", gothram_data = "+gothram_data+"]";
    }
}
