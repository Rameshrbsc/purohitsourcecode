package panditapp.codegen.com.panditapp.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.CommuntyPojoClass;
import panditapp.codegen.com.panditapp.Pojo.SoicalLoginPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.DateDialog;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

//import com.facebook.CallbackManager;
//import com.facebook.login.widget.LoginButton;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextLanguage)
    EditText EditTextLanguage;
    @BindView(R.id.EditTextEmailId)
    EditText EditTextEmailId;
    @BindView(R.id.EditTextPassword)
    EditText EditTextPassword;
    @BindView(R.id.EditTextNatchathiram)
    EditText EditTextNatchathiram;
    @BindView(R.id.EditTextGothram)
    EditText EditTextGothram;
    @BindView(R.id.EditTextSection)
    EditText EditTextSection;
    @BindView(R.id.EditTextAadhar1)
    EditText EditTextAadhar1;
    @BindView(R.id.EditTextAadhar2)
    EditText EditTextAadhar2;
    @BindView(R.id.EditTextAadhar3)
    EditText EditTextAadhar3;
    @BindView(R.id.EditTextPanCard)
    EditText EditTextPanCard;
    @BindView(R.id.FrameLayout1)
    FrameLayout FrameLayout1;
    @BindView(R.id.ButtonProfilePic)
    Button ButtonProfilePic;
    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;
    @BindView(R.id.closebtn)
    Button closebtn;
    @BindView(R.id.ButtonCreateAccount)
    Button ButtonCreateAccount;
    @BindView(R.id.RelativelayoutImageView)
    RelativeLayout RelativelayoutImageView;
    @BindView(R.id.TextViewBackLoginPage)
    TextView TextViewBackLoginPage;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.EditTextDate)
    EditText EditTextDate;
    @BindView(R.id.spinnerMothertoung)
    Spinner spinnerMothertoung;
    @BindView(R.id.login_here)
    TextView login_here;

    String userChoosenTask, encodedImage, FacebookID, Facebookemail, FacebookName, GmailpersonName, GmailEmail, GmailId, FilePath = null, TempUri;
    private Uri currentImageUri;
    Uri tempUri;
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    AlertDialog alertDialog;
    Bitmap thumbnail = null, bitmap;

    private static final int REQUEST_PERMISSION_SETTING = 151;
    static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

    File file;
    File camimage;
    @BindView(R.id.ImageViewGoogleLoginButton)
    ImageView ImageViewGoogleLoginButton;
    //google login api and instance
    private GoogleApiClient googleApiClient;
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile", encodedImagecheck;
    private ProgressDialog progressBar;
    String EmailIDsaved, EmailNameSaved, EmailIDToken, SocialType;

    LoginButton loginButton;
    CallbackManager callbackManager;
    private static final String EMAIL = "email";

    @BindView(R.id.ImageViewFacebookButton)
    ImageView ImageViewFacebookButton;
    File dir = new File(Environment.getExternalStorageDirectory() + "/Purohit User App/.Images");
    String FacebookUserName;
    private RadioGroup radioGroup;
    RadioButton radioGender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        ButterKnife.bind(this);
        myPreference = new MyPreference(this);
//fb initiztals
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        ImageViewFacebookButton.setOnClickListener(this);
        callbackManager = CallbackManager.Factory.create();


        radioGroup = (RadioGroup) findViewById(R.id.radioGrp);

        radioGender = (RadioButton) findViewById(R.id.radioM);






        /*otherCommunityRadioButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup




            }

        });*/

        EditTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateDialog dialog = new DateDialog(v);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "DatePicker");

            }
        });

        login_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent1);
            }
        });

        closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageViewProfilePic.setImageBitmap(null);
                closebtn.setVisibility(View.GONE);
                RelativelayoutImageView.setVisibility(View.GONE);
                camimage = null;
                currentImageUri = null;
                FilePath = null;
            }
        });

        final Intent intent = getIntent();
        try {
//            EmailNameSaved = intent.getExtras().get("EmailName").toString();
            EmailIDsaved = intent.getExtras().get("EmailID").toString();
            EmailIDToken = intent.getExtras().get("EmailIDUnique").toString();
            SocialType = intent.getExtras().get("SocialType").toString();
            FacebookUserName = intent.getExtras().get("username").toString();
            EditTextUserName.setText(FacebookUserName);
            EditTextEmailId.setText(EmailIDsaved);


        } catch (Exception e) {
            e.printStackTrace();
        }


        ButtonProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(SignUpActivity.this, permissionsRequired, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        });
        EditTextAadhar1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (EditTextAadhar1.getText().toString().length() == 4) {
                    EditTextAadhar2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditTextAadhar2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (EditTextAadhar2.getText().toString().length() == 4) {
                    EditTextAadhar3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    EditTextAadhar1.requestFocus();
                }
            }
        });

        TextViewBackLoginPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(SignUpActivity.this, LoginActivity.class);
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("AccessToken", null);
                editor.putString("DeviceId", null);
                LoginManager.getInstance().logOut();
                editor.putString("FacebookLogout", "fblogout");
                editor.apply();
                startActivity(intent1);
            }
        });
        EditTextAadhar3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    EditTextAadhar2.requestFocus();
                }

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              /*  Intent intent1=new Intent(SignUpActivity.this,PersonalActivity.class);
                startActivity(intent1);*/
                /*int selectedId = radioGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                otherCommunityRadioButton = (RadioButton) findViewById(selectedId);
                String gender=otherCommunityRadioButton.getText().toString();
                if (gender != null) {

                }*/

                if (!Check(EditTextUserName)) {
                    myPreference.ShowDialog(SignUpActivity.this, "field is mandatory", "Enter UserName");

                } else if (!isMobValid(EditTextMoblieNumber.getText().toString())) {
                    myPreference.ShowDialog(SignUpActivity.this, "field is mandatory", "Enter 10 digit MoblieNumber");

                } else if (!isValidEmail(EditTextEmailId.getText().toString())) {
                    myPreference.ShowDialog(SignUpActivity.this, "field is mandatory", "Enter Valid EmailId");

                } else if (!Check(EditTextPassword)) {

                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Enter Password");

                } else if (EditTextPassword.getText().toString().length()<8) {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Password length must have atleast 8 character");
                } else if (!Check(EditTextNatchathiram)) {
                    myPreference.ShowDialog(SignUpActivity.this, "field is mandatory", "Enter Location");

                } else if (!Check(EditTextDate)) {
                    myPreference.ShowDialog(SignUpActivity.this, "field is mandatory", "Select date of birth");

                } else if (spinnerMothertoung.getSelectedItem().toString().equals("select your mother tongue")) {
                    myPreference.ShowDialog(SignUpActivity.this, "field is mandatory", "Select your mother tongue ");

                } else {

                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("PurohitName", EditTextUserName.getText().toString());
                    editor.putString("PurohitMoblieNumber", EditTextMoblieNumber.getText().toString());
                    editor.putString("PurohitEmailID", EditTextEmailId.getText().toString());
                    editor.putString("PurohitPassword", EditTextPassword.getText().toString());
                    editor.putString("PurohitLocation", EditTextNatchathiram.getText().toString());

                    //   editor.putString("PurohitPanCard",EditTextPanCard.getText().toString());
                    editor.putString("PurohitGender", radioGender.getText().toString());
                    editor.putString("PurohitDate", EditTextDate.getText().toString());
                    // editor.putString("PurohitLanguage",EditTextLanguage.getText().toString());
                    editor.putString("MotherTounge", EditTextDate.getText().toString());
                    editor.apply();

                    Intent intent1 = new Intent(SignUpActivity.this, PersonalActivity.class);
                    startActivity(intent1);


                }

                /*Intent intent1=new Intent(SignUpActivity.this,PersonalActivity.class);
                startActivity(intent1);*/
            }
        });

        ButtonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = radioGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                radioGender = (RadioButton) findViewById(selectedId);
                String gender = radioGender.getText().toString();

                if (Check(EditTextUserName)) {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Enter UserName");

                } else if (isMobValid(EditTextMoblieNumber.getText().toString())) {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Enter MoblieNumber");

                } else if (isValidEmail(EditTextEmailId.getText().toString())) {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Enter EmailId");

                } else if (!Check(EditTextPassword)) {

                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Enter Password");

                } else if (isValidPassword(EditTextPassword.getText().toString().trim())) {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Password length must have atleast 8 character, one special character, one uppercase character, one lowercase character and number !!");
                } else if (Check(EditTextLanguage)) {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Enter Language");

                } else if (gender.equals("Select Your Mother Tongue")) {
                    myPreference.ShowDialog(SignUpActivity.this, "Oops", "Enter Language");

                } else {

                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("PurohitName", EditTextUserName.getText().toString());
                    editor.putString("PurohitEmailID", EditTextEmailId.getText().toString());
                    editor.putString("PurohitMoblieNumber", EditTextMoblieNumber.getText().toString());
                    editor.putString("PurohitPassword", EditTextPassword.getText().toString());
            /*    editor.putString("PurohitNatchtathiram", EditTextNatchathiram.getText().toString() );
                editor.putString("PurohitGothram", EditTextGothram.getText().toString());
                editor.putString("PurohitSection", EditTextSection.getText().toString() );*/

                    //   editor.putString("PurohitPanCard",EditTextPanCard.getText().toString());
                    editor.putString("PurohitGender", gender);
                    editor.putString("PurohitLanguage", EditTextLanguage.getText().toString());
                    editor.putString("PurohitDate", EditTextDate.getText().toString());
                    editor.putString("MotherTounge", spinnerMothertoung.getSelectedItem().toString());
                    editor.apply();

                    Intent intent1 = new Intent(SignUpActivity.this, PersonalActivity.class);
                    startActivity(intent1);


                }








                /*final String AadhaNumber  = EditTextAadhar1.getText().toString()+EditTextAadhar2.getText().toString()+EditTextAadhar3.getText().toString();
                Log.i("AadhaNumber",""+AadhaNumber);
                if(isValidPassword(EditTextPassword.getText().toString().trim())){

                    if(AadhaNumber.length()==12){

                        if(Check(EditTextUserName) &&Check(EditTextPassword)  && Check(EditTextNatchathiram)  &&Check(EditTextGothram) &&Check(EditTextLanguage) && Check(EditTextSection) && Check(EditTextPanCard)  ){
                        if(isMobValid(EditTextMoblieNumber.getText().toString()) && isValidEmail(EditTextEmailId.getText().toString())){
                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putString("PurohitName",EditTextUserName.getText().toString() );
                            editor.putString("PurohitEmailID", EditTextEmailId.getText().toString());
                            editor.putString("PurohitMoblieNumber",EditTextMoblieNumber.getText().toString() );
                            editor.putString("PurohitPassword", EditTextPassword.getText().toString());
                            editor.putString("PurohitNatchtathiram", EditTextNatchathiram.getText().toString() );
                            editor.putString("PurohitGothram", EditTextGothram.getText().toString());
                            editor.putString("PurohitSection", EditTextSection.getText().toString() );
                            editor.putString("PurohitPanCard",EditTextPanCard.getText().toString());
                            editor.putString("PurohitLanguage",EditTextLanguage.getText().toString());
                            editor.putString("PurohitAadhar", AadhaNumber);
                            if(FilePath != null){
                                editor.putString("ImageUriPath", FilePath);
                            }else {
                                editor.putString("ImageUriPath", null);
                            }
                            editor.apply();
                            Intent intentConfirm = new Intent(SignUpActivity.this,SigupConfirmViewActivity.class);
                            intentConfirm.putExtra("EmailID",EmailIDsaved);
                            intentConfirm.putExtra("EmailIDUnique",EmailIDToken);
                            intentConfirm.putExtra("SocialType",SocialType);
                            startActivity(intentConfirm);
                        }else{
                            Toast.makeText(SignUpActivity.this, "Email and mobile enter correct", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(SignUpActivity.this, "Missing fields", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(SignUpActivity.this, "Invalid Aadhar Number", Toast.LENGTH_SHORT).show();
                } }else{
                    myPreference.ShowDialog(SignUpActivity.this,"Oops","Password length must have atleast 8 character, one special character, one uppercase character, one lowercase character and number !!");
                }*/


            }
        });

        //1.google sign in option for creating gso
        ImageViewGoogleLoginButton.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
//2.google apo client create
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.i(SignUpActivity.this.getPackageName(), "On Success");
                Log.i(SignUpActivity.this.getPackageName(), loginResult.getAccessToken().getToken());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                            Facebookemail = object.getString("email");
                            FacebookName = object.getString("name");
                            FacebookID = object.getString("id");
                            Log.i("FacebookID", "" + FacebookID);

                            if (myPreference.isInternetOn()) {
                                progressBar = new ProgressDialog(SignUpActivity.this);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Processing ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();
                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                                jsonObject.addProperty("socialEmail", Facebookemail);
                                jsonObject.addProperty("socialId", FacebookID);
                                jsonObject.addProperty("socialType", 1);
                                API apiService = createServiceHeader(API.class);
                                Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                                call.enqueue(new Callback<SoicalLoginPojo>() {
                                    @Override
                                    public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                                        progressBar.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
                                                Log.i("", "" + response.body().getAccessToken());
                                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                editor.putString("AccessToken", response.body().getAccessToken());
                                                editor.putString("DeviceId", displayFirebaseRegId());
                                                editor.putString("NotificationCount", response.body().getNotificationCount());
                                                editor.apply();
                                                Intent intentOTP = new Intent(SignUpActivity.this, NavigationActivity.class);
                                                startActivity(intentOTP);
                                                ImageViewFacebookButton.setVisibility(View.GONE);
                                                ImageViewGoogleLoginButton.setVisibility(View.GONE);
                                                break;
                                            case "300":
                                                EmailIDsaved = Facebookemail;
                                                EmailIDToken = FacebookID;
                                                SocialType = "1";
                                                EditTextEmailId.setText(Facebookemail);
                                                EditTextUserName.setText(FacebookName);

//                                                final Dialog dialognew = new Dialog(SignUpActivity.this);
//                                                dialognew.setContentView(R.layout.custom_dialog_success);
//                                                Button dialogButtonnew = (Button) dialognew.findViewById(R.id.ButtonOk);
//                                                TextView TextviewBigTitlenew = (TextView)dialognew.findViewById(R.id.TextviewBigTitle);
//                                                TextviewBigTitlenew.setText("Success");
//                                                TextView TextviewSmallTitle1new = (TextView)dialognew.findViewById(R.id.TextviewSmallTitle1);
//                                                TextviewSmallTitle1new.setText(""+response.message());
//                                                dialogButtonnew.setOnClickListener(new View.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(View v) {
//                                                        dialognew.dismiss();
//                                                    }
//                                                });
//                                                dialognew.show();
                                                ImageViewFacebookButton.setVisibility(View.GONE);
                                                ImageViewGoogleLoginButton.setVisibility(View.GONE);
                                                break;

                                            case "108":
                                                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                                break;
                                            default:
                                                myPreference.ShowDialog(SignUpActivity.this, "Oops", response.body().getMessage());
                                                break;
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                                        progressBar.dismiss();
                                        myPreference.ShowDialog(SignUpActivity.this, "Oops", "Something went wrong, try again later");
                                        ;
                                    }
                                });


                            } else {
                                myPreference.ShowDialog(SignUpActivity.this, "Oops!!", "There is no internet connection");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.i(SignUpActivity.this.getPackageName(), "On Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.i(SignUpActivity.this.getPackageName(), "On Error");
                Log.i(SignUpActivity.this.getPackageName(), exception.toString());
            }
        });
    }


    private void selectImage() {
        final String[] value = getResources().getStringArray(R.array.Camera);
        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.camera_popup, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        alertDialogBuilder.setView(promptsView);
        ListView lv = (ListView) promptsView.findViewById(R.id.selectmode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, value);
        lv.setAdapter(adapter);
        lv.setSelector(R.color.colorPrimary);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
//                    boolean result= Utility.checkPermission(SignUpActivity.this);
                    userChoosenTask = "Take Photo";
                    alertDialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    dir.mkdirs();

                    if (dir.isDirectory()) {
                        Log.i("FileCreated", "FileCreated");

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        camimage = new File(dir, "PurhoitAPP" + timeStamp + ".jpg");
                        currentImageUri = Uri.fromFile(camimage);//file
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    } else {
                        Log.i("FileNotCreated", "FileNotCreated");
                    }


                } else if (position == 1) {

                    userChoosenTask = "Choose from Library";
//                    boolean result= Utility.checkPermission(SignUpActivity.this);


                    alertDialog.dismiss();
                    startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), SELECT_FILE);
                    Log.i("onsave", "going to gallery ");
                } else if (position == 2) {
                    alertDialog.dismiss();
                    userChoosenTask = "Cancel";


                }
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("dataset", "" + data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            try {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handlerResult(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                if (currentImageUri != null) {
                    try {
                        FilePath = currentImageUri.getPath();
                        ;
                        CamLoadImage(BitmapFactory.decodeStream(getContentResolver().openInputStream(currentImageUri)), currentImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            } else if (requestCode == SELECT_FILE) {
                currentImageUri = data.getData();
                try {
                    thumbnail = MediaStore.Images.Media.getBitmap(SignUpActivity.this.getApplicationContext().getContentResolver(), data.getData());
                    Log.i("onsave", "data got from gallery ");

                    FilePath = getRealPathFromURI(SignUpActivity.this, currentImageUri);
                    camimage = new File(FilePath);
                    ImageFileFilter imageFileFilter = new ImageFileFilter(camimage);
                    if (imageFileFilter.accept(camimage)) {
                        Log.i("GalleryImageLoaded", "GalleryImageLoaded");
                        LoadImage(currentImageUri);
                    } else {
                        Toast.makeText(this, "Unsupported File", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_PERMISSION_SETTING) {
                if (checkPermissions()) {
                    selectImage();
                }
            }
        }
    }

    private Target mTarget;

    private void LoadImage(Uri thumbnail) {//gallery
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                ImageViewProfilePic.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
//        Picasso.with(SignUpActivity.this)
//                .load(thumbnail)
//                .into(mTarget);
        Picasso.with(this).load(thumbnail).transform(new SignUpActivity.SquareTransform()).into(mTarget);

//        ImageViewProfilePic.setImageBitmap(thumbnail);
        RelativelayoutImageView.setVisibility(View.VISIBLE);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
        Log.i("onsave", " viewwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }


    private void CamLoadImage(Bitmap thumbnail, Uri currentImageUri) {//camera
        ExifInterface ei = null;
        int orientation = 0;
        try {
            ei = new ExifInterface(FilePath);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                thumbnail = rotateImage(thumbnail, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                thumbnail = rotateImage(thumbnail, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                thumbnail = rotateImage(thumbnail, 270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                thumbnail = thumbnail;
        }
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                ImageViewProfilePic.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
//        Picasso.with(SignUpActivity.this)
//                .load(currentImageUri)
//                .into(mTarget);
        Picasso.with(this).load(currentImageUri).transform(new SignUpActivity.SquareTransform()).into(mTarget);

//        ImageViewProfilePic.setImageBitmap(thumbnail);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
        RelativelayoutImageView.setVisibility(View.VISIBLE);
        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }

    //Some device image get rotated
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    //validations
    boolean Check(EditText testEditText) {
        return testEditText.getText().toString().length() > 0 && !testEditText.getText().toString().isEmpty();
    }

    //moblie number validations
    public static boolean isMobValid(String s) {
        Pattern p = Pattern.compile("[0-9]{10}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }

    //email vaild
    public static boolean isValidEmail(String email) {
        String emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pat = Pattern.compile(emailRegex);
        return email != null && pat.matcher(email).matches();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ImageViewGoogleLoginButton:
                signin();
                break;
            case R.id.ImageViewFacebookButton:
                loginButton.performClick();
                break;
        }

    }

    private void signin() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //7.this will be get the data from google account and result set in to our user interface
    private void handlerResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            GmailpersonName = acct.getDisplayName();
            GmailEmail = acct.getEmail();
            GmailId = acct.getId();
            Log.e(TAG, "Name: " + GmailpersonName + ", email: " + GmailEmail);
            UpdateUI(true);
        }

    }

    private void UpdateUI(boolean isLogin) {

        if (isLogin) {
            ImageViewGoogleLoginButton.setVisibility(View.GONE);

            if (myPreference.isInternetOn()) {
                progressBar = new ProgressDialog(SignUpActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                jsonObject.addProperty("socialEmail", GmailEmail);
                jsonObject.addProperty("socialId", GmailId);
                jsonObject.addProperty("socialType", 2);
                API apiService = createServiceHeader(API.class);
                Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                call.enqueue(new Callback<SoicalLoginPojo>() {
                    @Override
                    public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                        progressBar.dismiss();
                        Log.i("Code", "" + response.body().getCode());
                        switch (response.body().getCode()) {
                            case "200":
                                Log.i("accesstotken", "" + response.body().getAccessToken());
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("AccessToken", response.body().getAccessToken());
                                editor.putString("DeviceId", displayFirebaseRegId());
                                editor.putString("NotificationCount", response.body().getNotificationCount());
                                editor.apply();
                                Intent intentUserProfile = new Intent(SignUpActivity.this, NavigationActivity.class);
                                startActivity(intentUserProfile);

                                break;
                            case "300":
                                EmailIDsaved = GmailEmail;
                                EmailIDToken = GmailId;
                                SocialType = "2";
                                break;

                            case "108":
                                Toast.makeText(SignUpActivity.this, "Login Reqired", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                startActivity(intent);
                                break;
                            default:
                                myPreference.ShowDialog(SignUpActivity.this, "Oops", response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        myPreference.ShowDialog(SignUpActivity.this, "Oops", "Something went wrong!!!");
                    }
                });

            } else {
                myPreference.ShowDialog(SignUpActivity.this, "Oops", "There is no internet connection");
            }
        } else {
            ImageViewGoogleLoginButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static class ImageFileFilter implements FileFilter {
        File file;
        private final String[] okFileExtensions = new String[]{"jpg", "png", "jpeg"};

        public ImageFileFilter(File newfile) {
            this.file = newfile;
        }

        public boolean accept(File file) {
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static class SquareTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawPaint(paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "square";
        }
    }

//    public static class Utility {
//
//
//        public static boolean checkPermission(final Context context) {
////            AlertDialog alert;
//            int currentAPIVersion = Build.VERSION.SDK_INT;
//            if (currentAPIVersion >= Build.VERSION_CODES.M) {
//                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
//                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//                    } else {
//                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//                    }
//                    return false;
//
//                } else {
//                    return true;
//                }
//            } else {
//                return true;
//            }
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (checkPermissions()) {
                selectImage();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage access permission is required to read and write files from your mobile. Enable it to upload photo\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

    private String getRealPathFromURI(FragmentActivity activity, Uri selectedImageUri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId)) {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        } else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }

    //password validation
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    public void getCommunity() {


        API apiService = createServiceHeader(API.class);
        Call<CommuntyPojoClass> call = apiService.COMMUNTY_POJO_CLASS_CALL();
        call.enqueue(new Callback<CommuntyPojoClass>() {
            @Override
            public void onResponse(Call<CommuntyPojoClass> call, Response<CommuntyPojoClass> response) {
                progressBar.dismiss();
                Log.i("Code", "" + response.body().getCommunites_code());

            }

            @Override
            public void onFailure(Call<CommuntyPojoClass> call, Throwable t) {
                progressBar.dismiss();
                myPreference.ShowDialog(SignUpActivity.this, "Oops", "Something went wrong!!!");
            }
        });


    }

}
