package panditapp.codegen.com.panditapp.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.Pojo.PoojaListPojo;
import panditapp.codegen.com.panditapp.Pojo.PoojaListResponsePojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class LocationCreateGroupingActivity extends AppCompatActivity {

    @BindView(R.id.parent_linear_layout)
    LinearLayout parentLinearLayout;

    @BindView(R.id.ButtonAddField)
    Button ButtonAddField;
    @BindView(R.id.EditTextLocation)
    EditText EditTextLocation;
    @BindView(R.id.SpinnerPoojaList)
    Spinner SpinnerPoojaList;
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.ButtonCancel)
    Button ButtonCancel;

    //TextView
    @BindView(R.id.TextViewLocationID)
    TextView TextViewLocationID;
    @BindView(R.id.TextViewPoojaID)
    TextView TextViewPoojaID;
    @BindView(R.id.TextViewLatitude)
    TextView TextViewLatitude;
    @BindView(R.id.TextViewLongitude)
    TextView TextViewLongitude;

    String AreaPoojaID[] = new String[20];
    int AreaPoojaIDCount = 0;

    int NewPosition = 1;
    public static int RequestingAreaCode = 900;

    View addView;

    String[] StringPoojaList, StringPoojaIDList;

    ProgressDialog progressBar;

    MyPreference myPreference;
    Bundle bundle;

    AlertDialog dialog;

    String ServiceID, RequestingType, LocationName, LocationID, PoojaName, PoojaID, Latitude, Longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_create_grouping);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        setTitle("Create Multiple Location");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        myPreference = new MyPreference(this);

        bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("RequestingType")){
            RequestingType = bundle.getString("RequestingType");
            assert RequestingType != null;
            if (RequestingType.equals("Update")){
                ServiceID = bundle.getString("ServiceID");
                LocationName = bundle.getString("LocationName");
                EditTextLocation.setText(LocationName);
                Latitude = bundle.getString("Latitude");
                TextViewLatitude.setText(Latitude);
                Longitude = bundle.getString("Longitude");
                TextViewLongitude.setText(Longitude);
                LocationID = bundle.getString("LocationID");
                TextViewLocationID.setText(LocationID);
                PoojaName = bundle.getString("PoojaName");
                PoojaID = bundle.getString("PoojaID");
                ButtonAddField.setVisibility(View.GONE);
            }
        }


        EditTextLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocationCreateGroupingActivity.this, LocationSearchingActivity.class);
                startActivityForResult(intent, RequestingAreaCode);

            }
        });
        ButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();
//                EditTextLocation

//                ((LinearLayout)addView.findViewById(NewPosition).findViewById(R.id.ButtonDeleteField)).get
//                Toast.makeText(LocationCreateGroupingActivity.this, , Toast.LENGTH_SHORT).show();

                switch (RequestingType){
                    case "Create":
                        int childCount = parentLinearLayout.getChildCount();
                        if (myPreference.isInternetOn()){
                            if (childCount > 0){
                               /* progressBar = new ProgressDialog(LocationCreateGroupingActivity.this);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Processing your request...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();*/
                                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(LocationCreateGroupingActivity.this, R.style.RajCustomDialog);
                                final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                                final View dialogView = inflater.inflate(R.layout.custom_progress, null);
                                imageDialog.setView(dialogView);
                                final AlertDialog alertdialog = imageDialog.create();
                                alertdialog.show();
                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);

                                JsonArray jsonElements = new JsonArray();
                                for(int i=0; i< parentLinearLayout.getChildCount();i++){
                                    View thisChild = parentLinearLayout.getChildAt(i);
                                    TextView TextViewLocationID = (TextView) thisChild.findViewById(R.id.TextViewLocationID);
                                    TextView TextViewPoojaID = (TextView) thisChild.findViewById(R.id.TextViewPoojaID);
                                    TextView TextViewLatitude = (TextView) thisChild.findViewById(R.id.TextViewLatitude);
                                    TextView TextViewLongitude = (TextView) thisChild.findViewById(R.id.TextViewLongitude);
                                    Log.e("EditValue", TextViewLocationID.getText().toString());

                                    JsonObject AreaPoojaId = new JsonObject();
                                    AreaPoojaId.addProperty("Area", TextViewLocationID.getText().toString());
                                    AreaPoojaId.addProperty("Pooja", TextViewPoojaID.getText().toString());
                                    AreaPoojaId.addProperty("Latitude", TextViewLatitude.getText().toString());
                                    AreaPoojaId.addProperty("Longitude", TextViewLongitude.getText().toString());
                                    jsonElements.add(AreaPoojaId);
                                }
                                jsonObject.add("AreaPoojaId", jsonElements);
                                Log.e("JSON", jsonObject.toString());
                                API apiService = createServiceHeader(API.class);
                                Call<LoginPojo> call = apiService.CREATE_GROUPING_POJO_CALL(jsonObject);

                                call.enqueue(new retrofit2.Callback<LoginPojo>() {
                                    @Override
                                    public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                                        alertdialog.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
                                                onBackPressed();
                                                LocationCreateGroupingActivity.this.finish();
                                                break;
                                            case "110":
                                                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(LocationCreateGroupingActivity.this, R.style.RajCustomDialog);
                                                final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                                                final View dialogView = inflater.inflate(R.layout.custom_warning_dialog, null);
                                                imageDialog.setView(dialogView);
                                                TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                                TextviewSmallTitle1.setText(response.body().getMessage());
                                                TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                                TextviewBigTitle.setText("Warning");
                                                Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                                ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                        onBackPressed();
                                                        LocationCreateGroupingActivity.this.finish();
                                                    }
                                                });
                                                dialog = imageDialog.create();
                                                dialog.show();
                                                break;
                                            case "108":
                                                Toast.makeText(LocationCreateGroupingActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(LocationCreateGroupingActivity.this, LoginActivity.class);
                                                startActivity(intent);
                                                finish();
                                                break;
                                            default:
                                                myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", response.body().getMessage());
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                                        alertdialog.dismiss();
                                        myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", "Something went wrong");
                                    }
                                });
                            }else {
                                myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", "At least add single data and press + button");
                            }
                        }else {
                            myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", "Turn on the internet connection");
                        }
                        break;
                    case "Update":

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                        jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                        JsonArray jsonElements = new JsonArray();

                        JsonObject AreaPoojaId = new JsonObject();
                        AreaPoojaId.addProperty("ServiceId", ServiceID);
                        AreaPoojaId.addProperty("Area", TextViewLocationID.getText().toString());
                        AreaPoojaId.addProperty("Pooja", TextViewPoojaID.getText().toString());
                        AreaPoojaId.addProperty("Latitude", TextViewLatitude.getText().toString());
                        AreaPoojaId.addProperty("Longitude", TextViewLongitude.getText().toString());
                        jsonElements.add(AreaPoojaId);

                        jsonObject.add("ServiceAreaPoojaId", jsonElements);
                        Log.e("JSON", jsonObject.toString());
                       /* progressBar = new ProgressDialog(LocationCreateGroupingActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.show();*/

                        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(LocationCreateGroupingActivity.this, R.style.RajCustomDialog);
                        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                        final View dialogView = inflater.inflate(R.layout.custom_progress, null);
                        imageDialog.setView(dialogView);
                        final AlertDialog alertdialog = imageDialog.create();
                        alertdialog.show();
                        if (myPreference.isInternetOn()){
                            API apiService = createServiceHeader(API.class);
                            Call<LoginPojo> call = apiService.UPDATE_GROUPING_POJO_CALL(jsonObject);
                            call.enqueue(new retrofit2.Callback<LoginPojo>() {
                                @Override
                                public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                                    alertdialog.dismiss();
                                    switch (response.body().getCode()) {
                                        case "200":
//                                            Toast.makeText(LocationCreateGroupingActivity.this, "Updated Successfully!!!", Toast.LENGTH_SHORT).show();
                                            onBackPressed();
                                            LocationCreateGroupingActivity.this.finish();
                                            break;
                                        case "108":
                                            Toast.makeText(LocationCreateGroupingActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(LocationCreateGroupingActivity.this, LoginActivity.class));
                                            break;
                                        default:
                                            myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops",response.body().getMessage());
                                    }
                                }
                                @Override
                                public void onFailure(Call<LoginPojo> call, Throwable t) {
                                    alertdialog.dismiss();
                                    myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", "Something went wrong, Try again later");
                                }
                            });
                        }else {
                            myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", "Check your internet connection");
                        }
                        break;
                }

            }
        });


        ButtonAddField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!EditTextLocation.getText().toString().isEmpty() && !SpinnerPoojaList.getSelectedItem().toString().equals("----- Select Pooja -----")){
                    if ( AreaPoojaIDCount < 20){
                        LayoutInflater layoutInflater =
                                (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        addView = layoutInflater.inflate(R.layout.custom_add_field, null);

                        Button ButtonDeleteField = (Button)addView.findViewById(R.id.ButtonDeleteField);
                        final EditText EditTextLocation = (EditText) addView.findViewById(R.id.EditTextLocation);
                        TextView TextViewLatitude = (TextView) addView.findViewById(R.id.TextViewLatitude);
                        TextView TextViewLongitude = (TextView) addView.findViewById(R.id.TextViewLongitude);
                        TextView TextViewPoojaName = (TextView) addView.findViewById(R.id.TextViewPoojaName);
                        TextView TextViewLocationID = (TextView) addView.findViewById(R.id.TextViewLocationID);
                        TextView TextViewPoojaID = (TextView) addView.findViewById(R.id.TextViewPoojaID);

                        if (AreaPoojaIDCount == 0 || !Arrays.asList(AreaPoojaID).contains(LocationCreateGroupingActivity.this.TextViewPoojaID.getText().toString() +","+ LocationCreateGroupingActivity.this.TextViewLocationID.getText().toString())){
                            AreaPoojaID[AreaPoojaIDCount] = LocationCreateGroupingActivity.this.TextViewPoojaID.getText().toString() +","+ LocationCreateGroupingActivity.this.TextViewLocationID.getText().toString();
                            Log.e("AreaPooja", AreaPoojaID[AreaPoojaIDCount]);
                            AreaPoojaIDCount++;
                            //Location Value - EditText
                            EditTextLocation.setText(LocationCreateGroupingActivity.this.EditTextLocation.getText().toString());
                            LocationCreateGroupingActivity.this.EditTextLocation.setText("");
                            //Pooja Value - Spinner
                            TextViewPoojaName.setText(SpinnerPoojaList.getSelectedItem().toString());
                            SpinnerPoojaList.setSelection(0);
                            //Location ID - TextView
                            TextViewLocationID.setText(LocationCreateGroupingActivity.this.TextViewLocationID.getText().toString());
                            LocationCreateGroupingActivity.this.TextViewLocationID.setText("");
                            //Pooja ID - TextView
                            TextViewPoojaID.setText(LocationCreateGroupingActivity.this.TextViewPoojaID.getText().toString());
                            LocationCreateGroupingActivity.this.TextViewPoojaID.setText("");
                            //Latitude Value - TextView
                            TextViewLatitude.setText(LocationCreateGroupingActivity.this.TextViewLatitude.getText());
                            LocationCreateGroupingActivity.this.TextViewLatitude.setText("");
                            //Longitude Value - TextView
                            TextViewLongitude.setText(LocationCreateGroupingActivity.this.TextViewLongitude.getText());
                            LocationCreateGroupingActivity.this.TextViewLongitude.setText("");
//                            Toast.makeText(LocationCreateGroupingActivity.this, EditTextLocation.getText().toString(), Toast.LENGTH_SHORT).show();
                            //                    addView.setId(NewPosition++);
                            parentLinearLayout.addView(addView, parentLinearLayout.getChildCount() - 1);

                        }else {
                            myPreference.ShowDialog(LocationCreateGroupingActivity.this, "Error", "Both Area and Pooja already present");
                        }

                        ButtonDeleteField.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                            ((LinearLayout)addView.getParent()).removeView(addView);
                                parentLinearLayout.removeView(addView);
                            }
                        });
                    }else {
                        myPreference.ShowDialog(LocationCreateGroupingActivity.this, "Warning", "You can create 20 entry at a time");
                    }
                }else {
                    myPreference.ShowDialog(LocationCreateGroupingActivity.this, "Warning", "Field shouldn't be empty");
                }
            }
        });

        if (myPreference.isInternetOn()){
            /*progressBar = new ProgressDialog(LocationCreateGroupingActivity.this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Fetching Pooja details ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();*/

            final AlertDialog.Builder imageDialog = new AlertDialog.Builder(LocationCreateGroupingActivity.this, R.style.RajCustomDialog);
            final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
            final View dialogView = inflater.inflate(R.layout.custom_progress, null);
            imageDialog.setView(dialogView);
            final AlertDialog alertdialog = imageDialog.create();
            alertdialog.show();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);

            API apiService = createServiceHeader(API.class);
            Call<PoojaListPojo> call = apiService.SEARCH_POOJA_POJO_CALL(jsonObject);

            call.enqueue(new retrofit2.Callback<PoojaListPojo>() {
                @Override
                public void onResponse(Call<PoojaListPojo> call, retrofit2.Response<PoojaListPojo> response) {
                    alertdialog.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            PoojaListResponsePojo[] item = response.body().getResponse();
                            StringPoojaList = new String[item.length+1];
                            StringPoojaList[0] = "----- Select Pooja -----";
                            StringPoojaIDList = new String[item.length+1];
                            StringPoojaIDList[0] = "0";
                            for (int i = 0; i<item.length; i++){
                                StringPoojaList[i+1] = item[i].getPoojaName();
                                StringPoojaIDList[i+1] = item[i].getPoojaId();
                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(LocationCreateGroupingActivity.this,
                                        R.layout.simple_dropdown_item_line, StringPoojaList);

                                // Drop down layout style - list view with radio button
                                arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
                                SpinnerPoojaList.setAdapter(arrayAdapter);
                            }

                            if (RequestingType.equals("Update")){
                                SpinnerPoojaList.setSelection(Arrays.asList(StringPoojaIDList).indexOf(PoojaID));
                            }

                            break;
                        case "108":
                            Toast.makeText(LocationCreateGroupingActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LocationCreateGroupingActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                            break;
                        default:
                            myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", response.body().getMessage());
                    }
                }
                @Override
                public void onFailure(Call<PoojaListPojo> call, Throwable t) {
                    alertdialog.dismiss();
                    myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", "Something went wrong");
                }
            });
        }else {
            myPreference.ShowDialog(LocationCreateGroupingActivity.this,"Oops", "Check your internet connection");
        }

        SpinnerPoojaList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >0){
                    TextViewPoojaID.setText(StringPoojaIDList[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestingAreaCode) {
            if(resultCode == RESULT_OK) {
//                Toast.makeText(this, data.getDoubleExtra("Latitude",0.0)+"\n"+data.getDoubleExtra("Longitude", 0.0), Toast.LENGTH_SHORT).show();
                TextViewLatitude.setText(String.valueOf(data.getDoubleExtra("Latitude",0.0)));
                TextViewLongitude.setText(String.valueOf(data.getDoubleExtra("Longitude", 0.0)));
                EditTextLocation.setText(data.getStringExtra("AreaName"));
                TextViewLocationID.setText(data.getStringExtra("AreaID"));
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
