package panditapp.codegen.com.panditapp.Pojo;

public class Routes {
    private String summary;

    private String copyrights;

    private String[] waypoint_order;

    private Legs[] legs;

    private String[] warnings;

    public String getSummary ()
    {
        return summary;
    }

    public void setSummary (String summary)
    {
        this.summary = summary;
    }

    public String getCopyrights ()
    {
        return copyrights;
    }

    public void setCopyrights (String copyrights)
    {
        this.copyrights = copyrights;
    }

    public String[] getWaypoint_order ()
    {
        return waypoint_order;
    }

    public void setWaypoint_order (String[] waypoint_order)
    {
        this.waypoint_order = waypoint_order;
    }

    public Legs[] getLegs ()
    {
        return legs;
    }

    public void setLegs (Legs[] legs)
    {
        this.legs = legs;
    }

    public String[] getWarnings ()
    {
        return warnings;
    }

    public void setWarnings (String[] warnings)
    {
        this.warnings = warnings;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [summary = "+summary+", copyrights = "+copyrights+", waypoint_order = "+waypoint_order+", legs = "+legs+", warnings = "+warnings+"]";
    }
}
