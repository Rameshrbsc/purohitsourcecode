package panditapp.codegen.com.panditapp.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import panditapp.codegen.com.panditapp.Pojo.AreaModel;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.interfac.UnCheckCallBAck;

/**
 * Created by Parsania Hardik on 29-Jun-17.
 */
public class AreaCheckBoxAdapter extends RecyclerView.Adapter<AreaCheckBoxAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    public static List<AreaModel> imageModelArrayList;
    private Context ctx;
    UnCheckCallBAck  unCheckCallBAck;

    public AreaCheckBoxAdapter(Context ctx, List<AreaModel> imageModelArrayList, UnCheckCallBAck unCheckCallBAck) {

        inflater = LayoutInflater.from(ctx);
        this.imageModelArrayList = imageModelArrayList;
        this.unCheckCallBAck = unCheckCallBAck;
        this.ctx = ctx;
    }

    @Override
    public AreaCheckBoxAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.area_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.checkBox.setText(imageModelArrayList.get(position).getAreaName());
        holder.checkBox.setChecked(true);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.checkBox.isChecked()){

                }else {
                    unCheckCallBAck.oncheckbox("",true);

                }
            }
        });




        //holder.checkBox.setChecked(imageModelArrayList.get(position).getSelected());
        // holder.tvAnimal.setText(imageModelArrayList.get(position).getAreaName());

        // holder.checkBox.setTag(R.integer.btnplusview, convertView);
        //  holder.checkBox.setTag(position);

/* holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer pos = (Integer) holder.checkBox.getTag();
                Toast.makeText(ctx, imageModelArrayList.get(pos).getAreaName() + " clicked!", Toast.LENGTH_SHORT).show();

                if (imageModelArrayList.get(pos).getSelected()) {
                    imageModelArrayList.get(pos).setSelected(false);
                } else {
                    imageModelArrayList.get(pos).setSelected(true);
                }
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return imageModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        protected CheckBox checkBox;
        private TextView tvAnimal;

        public MyViewHolder(View itemView) {
            super(itemView);

            checkBox = (CheckBox) itemView.findViewById(R.id.cb);
            tvAnimal = (TextView) itemView.findViewById(R.id.areaName);
        }

    }
}





