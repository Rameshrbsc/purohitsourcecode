package panditapp.codegen.com.panditapp.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.List;

import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LocationCreateGroupingActivity;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.Pojo.ServiceAreaPoojaId;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class LocationGroupingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private List<ServiceAreaPoojaId> serviceAreaPoojaIds;
    private ProgressDialog progressBar;
    private MyPreference myPreference;
    private RefreshData refreshData;
    public LocationGroupingAdapter(FragmentActivity activity, List<ServiceAreaPoojaId> serviceAreaPoojaIds, RefreshData refreshData) {
        this.context = activity;
        this.serviceAreaPoojaIds = serviceAreaPoojaIds;
        this.refreshData = refreshData;
        myPreference = new MyPreference(activity);
    }

    public interface RefreshData {

        void Refresh_Data();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewPoojaName, TextViewAreaName;
        ImageView ImageViewEdit, ImageViewDelete;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewPoojaName = (TextView) itemView.findViewById(R.id.TextViewPoojaName);
            TextViewAreaName = (TextView) itemView.findViewById(R.id.TextViewAreaName);

            ImageViewEdit = (ImageView) itemView.findViewById(R.id.ImageViewEdit);
            ImageViewDelete = (ImageView) itemView.findViewById(R.id.ImageViewDelete);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_service_grouping_list_card, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.TextViewAreaName.setText(serviceAreaPoojaIds.get(position).getAreaName());
        viewHolder.TextViewPoojaName.setText(serviceAreaPoojaIds.get(position).getPoojaName());
        viewHolder.ImageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Service Operation")
                        .setMessage("Are you sure want to delete this service?")
                        .setIcon(R.mipmap.ic_launcher)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                progressBar = new ProgressDialog(context);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Processing ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();

                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                jsonObject.addProperty("ServiceId", serviceAreaPoojaIds.get(position).getServiceId());
                                Log.e("JSON", jsonObject.toString());
                                if (myPreference.isInternetOn()){
                                    API apiService = createServiceHeader(API.class);
                                    Call<LoginPojo> call = apiService.DELETE_GROUPING_POJO_CALL(jsonObject);
                                    call.enqueue(new retrofit2.Callback<LoginPojo>() {
                                        @Override
                                        public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                                            progressBar.dismiss();
                                            switch (response.body().getCode()) {
                                                case "200":
                                                    refreshData.Refresh_Data();
                                                    myPreference.ShowSuccessDialog(context, "Success", "Deleted Successfully");
                                                    break;
                                                case "108":
                                                    Toast.makeText(context, "Login Required", Toast.LENGTH_SHORT).show();
                                                    context.startActivity(new Intent(context, LoginActivity.class));
                                                    break;
                                                default:
                                                    myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                                            }
                                        }
                                        @Override
                                        public void onFailure(Call<LoginPojo> call, Throwable t) {
                                            progressBar.dismiss();
                                            myPreference.ShowDialog(context,"Oops", "Something went wrong, Try again later");
                                        }
                                    });
                                }else {
                                    myPreference.ShowDialog(context,"Oops", "Check your internet connection");
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        });

        viewHolder.ImageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LocationCreateGroupingActivity.class);
                intent.putExtra("RequestingType", "Update");
                intent.putExtra("ServiceID", serviceAreaPoojaIds.get(position).getServiceId());
                intent.putExtra("LocationName", serviceAreaPoojaIds.get(position).getAreaName());
                intent.putExtra("LocationID", serviceAreaPoojaIds.get(position).getAreaId());
                intent.putExtra("Latitude", serviceAreaPoojaIds.get(position).getLatitude());
                intent.putExtra("Longitude", serviceAreaPoojaIds.get(position).getLongtitude());
                intent.putExtra("PoojaName", serviceAreaPoojaIds.get(position).getPoojaName());
                intent.putExtra("PoojaID", serviceAreaPoojaIds.get(position).getPoojaId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return serviceAreaPoojaIds.size();
    }
}
