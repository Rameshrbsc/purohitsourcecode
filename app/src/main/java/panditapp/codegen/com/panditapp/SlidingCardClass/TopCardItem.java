package panditapp.codegen.com.panditapp.SlidingCardClass;

public class TopCardItem {

    private String mCustomerName;
    private String mDescription;
    private String mPojoName;
    private String mAmount;
    private String mUserImage;
    private String mBookingId;
    private String mAddress;
    String date;
    String time;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmBookingId() {
        return mBookingId;
    }

    public void setmBookingId(String mBookingId) {
        this.mBookingId = mBookingId;
    }

    public String getmUserImage() {
        return mUserImage;
    }

    public void setmUserImage(String mUserImage) {
        this.mUserImage = mUserImage;
    }

    public String getmCustomerName() {
        return mCustomerName;
    }

    public void setmCustomerName(String mCustomerName) {
        this.mCustomerName = mCustomerName;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmPojoName() {
        return mPojoName;
    }

    public void setmPojoName(String mPojoName) {
        this.mPojoName = mPojoName;
    }

    public String getmAmount() {
        return mAmount;
    }

    public void setmAmount(String mAmount) {
        this.mAmount = mAmount;
    }

    public TopCardItem(String BookingId, String mUserImage, String mCustomerName, String mDescription, String mPojoName, String mAmount, String mAddress,String date,String time) {
        this.mBookingId = BookingId;
        this.mUserImage = mUserImage;
        this.mCustomerName = mCustomerName;
        this.mDescription = mDescription;
        this.mPojoName = mPojoName;
        this.mAmount = mAmount;
        this.mAddress = mAddress;
        this.date = date;
        this.time = time;
    }
}
