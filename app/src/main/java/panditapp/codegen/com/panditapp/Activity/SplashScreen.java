package panditapp.codegen.com.panditapp.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import io.fabric.sdk.android.Fabric;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.PurohitSessionAuthPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import panditapp.codegen.com.panditapp.Utils.NotificationUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class SplashScreen extends AppCompatActivity {
    MyPreference myPreference;
    private ProgressDialog progressBar;

    Trace myTrace;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final String TAG = SplashScreen.class.getSimpleName();

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true /* optional */)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPreference = new MyPreference(this);

        myTrace = FirebasePerformance.getInstance().newTrace("Purohit_Trace");
        myTrace.start();
        myTrace.incrementMetric("item_cache_miss", 1);

        //getting version name and code for force updated
        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int versionNumber = pinfo.versionCode;
            String versionName = pinfo.versionName;
            Log.i("Versions",""+versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }



        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    SharedPreferences.Editor editor = getSharedPreferences("MyPrefsFile", MODE_PRIVATE).edit();
                    editor.putString("DeviceId", displayFirebaseRegId());
                    editor.apply();

                }else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    Log.e(TAG,"Message " + intent.getStringExtra("body"));
                }
            }
        };
        displayFirebaseRegId();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }

    private class PrefetchData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Intent intent = new Intent(SplashScreen.this, SkipActivity.class);
            startActivity(intent);
            finish();
            return null;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        String[] Area = {"1","3","5"};
        String[] Pooja = {"2", "4", "5"};

        JsonObject jsonObject1 = new JsonObject();
        JsonArray AreaArray = null;
        JsonArray jsonElements = new JsonArray();
        for (int i=0; i<Area.length; i++){
            AreaArray = new JsonArray();
            JsonObject raj = new JsonObject();
            raj.addProperty("Area", Area[i]);
            raj.addProperty("Pooja", Pooja[i]);
            AreaArray.add(Area[i]);
            AreaArray.add(Pooja[i]);
            jsonElements.add(raj);
        }
        jsonObject1.add("AreaPooja", jsonElements);
        jsonObject1.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[0]);
        jsonObject1.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
        jsonObject1.add("NotificaitonIdArray", AreaArray);
        jsonObject1.addProperty("Test", "Test");

        if (myPreference.getDefaultRunTimeValue()[0] != null){
            Log.d("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            if (myPreference.isInternetOn()){
                progressBar = new ProgressDialog(SplashScreen.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                Log.e("JSON" , jsonObject.toString());
                API apiService = createServiceHeader(API.class);
                Call<PurohitSessionAuthPojo> call = apiService.USER_SESSION_AUTH_POJO_CALL(jsonObject);
                call.enqueue(new Callback<PurohitSessionAuthPojo>() {
                    @Override
                    public void onResponse(Call<PurohitSessionAuthPojo> call, Response<PurohitSessionAuthPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":

                                SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("NotificationCount", response.body().getNotificationCount());
                                Log.i("NotificationCount",response.body().getNotificationCount());
                                editor.apply();
                                if(response.body().getUpdateFlag().equals("0")){
                                    Intent intentUpdate = new Intent(SplashScreen.this, ProfileEditActivity.class);
                                    intentUpdate.putExtra("Operation","NewUpdate");
                                    startActivity(intentUpdate);
                                }else {

                                    Intent intentHome = new Intent(SplashScreen.this, NavigationActivity.class);
                                    startActivity(intentHome);

                                }

                                break;

                            case "108":
                                Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                                startActivity(intent);
                                break;
                            default:
                                myPreference.ShowDialog(SplashScreen.this,"Oops",response.body().getMessage());
                                break;

                        }

                    }

                    @Override
                    public void onFailure(Call<PurohitSessionAuthPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(SplashScreen.this,"Oops", "Something went wrong!!!\nLogin again");
                            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                            startActivity(intent);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }else{



                final Dialog dialog = new Dialog(SplashScreen.this);
                dialog.setContentView(R.layout.custom_failor_dialog);
                Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
                TextView TextviewSmallTitle1 = (TextView)dialog.findViewById(R.id.TextviewSmallTitle1);
                TextviewSmallTitle1.setText("There is no internet connection");
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }

        }else{


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new PrefetchData().execute();
                    Log.i("LoginRequired","LoginRequired");

                }

            },4000);


//            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//            startActivity(intent);
//            finish();
//            Toast.makeText(this, "LoginRequired", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Stage", "onResume");
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));


        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myTrace.stop();
    }
}
