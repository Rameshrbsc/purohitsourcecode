package panditapp.codegen.com.panditapp.Fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.FullScreenImageViewActivity;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Activity.ProfileEditActivity;
import panditapp.codegen.com.panditapp.Activity.SignUpActivity;
import panditapp.codegen.com.panditapp.Pojo.PurohitProfileViewPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitProfileViewResponsePojo;
import panditapp.codegen.com.panditapp.Pojo.SkillListPojo;
import panditapp.codegen.com.panditapp.Pojo.SkillListResponsePojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileViewFragment extends Fragment {
    MyPreference myPreference;
    private ProgressDialog progressBar;
    Unbinder unbinder;
    PurohitProfileViewResponsePojo purohitProfileViewResponsePojo;

    private List<PurohitProfileViewResponsePojo> purohitProfileViewResponsePojos;
    String ImageURL;
    @BindView(R.id.TextViewUserName)
    TextView TextViewUserName;
    @BindView(R.id.TextViewUserEmailID)
    TextView TextViewUserEmailID;
    @BindView(R.id.TextViewUserNatchtathiram)
    TextView TextViewUserNatchtathiram;
    @BindView(R.id.TextViewUserGothram)
    TextView TextViewUserGothram;
    @BindView(R.id.TextViewUserMoblieNumber)
    TextView TextViewUserMoblieNumber;
    @BindView(R.id.TextViewUserSection)
    TextView TextViewUserSection;
    @BindView(R.id.TextViewUserAadharNumber)
    TextView TextViewUserAadharNumber;

    @BindView(R.id.TextViewPanCard)
    TextView TextViewPanCard;
    @BindView(R.id.TextViewLocation)
    TextView TextViewLocation;
    @BindView(R.id.TextViewLanguage)
    TextView TextViewLanguage;
    @BindView(R.id.TextViewSkills)
    TextView TextViewSkills;
    @BindView(R.id.TextViewExperience)
    TextView TextViewExperience;
    @BindView(R.id.TextViewAbout)
    TextView TextViewAbout;
    @BindView(R.id.ImageViewProfilePic)
    android.widget.ImageView ImageViewProfilePic;
    @BindView(R.id.ButtonEdit)
    Button ButtonEdit;
    String[] SkillListName,SkillListId;
    private List<String> SkillIdResponseId,SkillIdResponseName;
    public ProfileViewFragment() {
        // Required empty public constructor
    }

    String TextViewValue = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_purohit_profile_view, container, false);
        unbinder = ButterKnife.bind(this,view);
        myPreference = new MyPreference(getActivity());
        setHasOptionsMenu(true);
        getActivity().setTitle("Profile");
        //this is changing the keyboard has done or tick icon ready to submit
        TextViewAbout.setImeOptions(EditorInfo.IME_ACTION_DONE);

        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            Log.e("AccessToken",myPreference.getDefaultRunTimeValue()[0]);
            API apiService = createServiceHeader(API.class);
            Call<PurohitProfileViewPojo> call = apiService.USER_PROFILE_VIEW_POJO_CALL(jsonObject);
            call.enqueue(new Callback<PurohitProfileViewPojo>() {
                @Override
                public void onResponse(Call<PurohitProfileViewPojo> call, Response<PurohitProfileViewPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":

                            purohitProfileViewResponsePojo = response.body().getResponse();
                            TextViewUserName.setText(purohitProfileViewResponsePojo.getPurohitName());
                            TextViewUserMoblieNumber.setText(purohitProfileViewResponsePojo.getPurohitPhoneNumber());
                            TextViewUserEmailID.setText(purohitProfileViewResponsePojo.getPurohitEmailID());
                            TextViewUserSection.setText(purohitProfileViewResponsePojo.getPurohitSection());
                            TextViewUserNatchtathiram.setText(purohitProfileViewResponsePojo.getPurohitNatcharam());
                            TextViewUserGothram.setText(purohitProfileViewResponsePojo.getPurohitGothram());
                            TextViewUserAadharNumber.setText(purohitProfileViewResponsePojo.getPurohitAadharNumber());
                            TextViewPanCard.setText(purohitProfileViewResponsePojo.getPurohitPanNumber());
                            TextViewLanguage.setText(purohitProfileViewResponsePojo.getPurohitLanguage());
                            TextViewLocation.setText(purohitProfileViewResponsePojo.getPurohitLocation());
                            TextViewExperience.setText(purohitProfileViewResponsePojo.getPurohitExperience());
                            TextViewAbout.setText(purohitProfileViewResponsePojo.getPurohitAbout());
                            String SkillsFullString = purohitProfileViewResponsePojo.getPurohitSkills();
                            String[] values = SkillsFullString.split(",");

                            LoadSkillListSpinner(values);

                            try{
                                Log.i("Image",""+purohitProfileViewResponsePojo.getPurohitProfileThumbPic());
                                ImageURL = purohitProfileViewResponsePojo.getPurohitProfileThumbPic();
                                if(purohitProfileViewResponsePojo.getPurohitProfilePic() != null){
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        Picasso.with(getActivity()).load(purohitProfileViewResponsePojo.getPurohitProfileThumbPic()).transform(new SignUpActivity.CircleTransform()).into(ImageViewProfilePic);
                                    }
                                }else{
                                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.panditlogo);

                                    RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getActivity().getResources(),getclip(largeIcon));
                                    roundDrawable.setCircular(true);
                                    ImageViewProfilePic.setImageDrawable(roundDrawable);
                                }

                            }catch (Exception e){e.printStackTrace();}

                            break;
                        case "108":
                            Intent intentemg = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intentemg);
                            break;
                        default:
                            myPreference.ShowDialog(getActivity(),"Oops",response.body().getMessage());
                            break;
                    }

                }


                @Override
                public void onFailure(Call<PurohitProfileViewPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        myPreference.ShowDialog(getActivity(),"Oops",t.getMessage());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            });

        }else {
            myPreference.ShowDialog(getActivity(),"Oops","There is no internet connection");
        }


        ImageViewProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(getActivity(), FullScreenImageViewActivity.class);
                    intent.putExtra("Path", ImageURL);
                    startActivity(intent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });



        ButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
                intent.putExtra("Operation", "Update");
                intent.putExtra("Name", TextViewUserName.getText().toString());
                intent.putExtra("EmailID", TextViewUserEmailID.getText().toString());
                intent.putExtra("Natchtathiram", TextViewUserNatchtathiram.getText().toString());
                intent.putExtra("Gothram", TextViewUserGothram.getText().toString());
                intent.putExtra("MoblieNumber", TextViewUserMoblieNumber.getText().toString());
                intent.putExtra("Section", TextViewUserSection.getText().toString());
                intent.putExtra("Aadhar", TextViewUserAadharNumber.getText().toString());
                intent.putExtra("PanCard", TextViewPanCard.getText().toString());
                intent.putExtra("Location", TextViewLocation.getText().toString());
                intent.putExtra("Language", TextViewLanguage.getText().toString());
                intent.putExtra("Skills", TextViewSkills.getText().toString());
                intent.putStringArrayListExtra("SkillIdResponseId", (ArrayList<String>) SkillIdResponseId);
                intent.putStringArrayListExtra("SkillIdResponseName", (ArrayList<String>) SkillIdResponseName);
//                intent.putExtra("SkillIdResponseID",  SkillListId);
                intent.putExtra("Experience", TextViewExperience.getText().toString());
                intent.putExtra("About", TextViewAbout.getText().toString());
                intent.putExtra("PicURI",ImageURL);
                startActivity(intent);
            }
        });


        return view;
    }

    private void LoadSkillListSpinner(final String[] skilListFromResponse) {
//        final String[] TempTextViewValue = {""};
//        final String[] TextViewValue = {""};
        if (myPreference.isInternetOn()) {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            API apiService = createServiceHeader(API.class);
            Call<SkillListPojo> call = apiService.SKILL_LIST_POJO_CALL(jsonObject);
            call.enqueue(new Callback<SkillListPojo>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<SkillListPojo> call, Response<SkillListPojo> response) {

                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            SkillIdResponseId = new ArrayList<>();
                            SkillIdResponseName = new ArrayList<>();

                            SkillListResponsePojo[] skillListResponsePojos = response.body().getResponse();
                            SkillListName = new String[skillListResponsePojos.length];
                            SkillListId = new String[skillListResponsePojos.length];

                            for(int i =0; i<skillListResponsePojos.length;i++){
                                SkillListName[i] = skillListResponsePojos[i].getSkillName();
                                SkillListId[i]= skillListResponsePojos[i].getSkillId();
                            }

                            for (int i=0;i<SkillListId.length;i++) {
                                for (String aSkilListFromResponse : skilListFromResponse) {
                                    if (SkillListId[i].equals(aSkilListFromResponse)) {
                                        Log.e("SkillListName", SkillListName[i]);
                                        SkillIdResponseId.add(SkillListId[i]);
                                        SkillIdResponseName.add(SkillListName[i]);
                                        TextViewValue = TextViewValue.concat(SkillListName[i] + ",");
                                        if (SkillIdResponseId.size() == skilListFromResponse.length){
                                            TextViewSkills.setText(TextViewValue.substring(0, TextViewValue.length() -1));
                                        }
                                        break;
                                    }
                                }
                            }
                            break;
                        case "108":
                            Intent logoutintent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(logoutintent );
                            break;
                        default:
                            myPreference.ShowDialog(getActivity(),"Oops",response.body().getMessage());
                            break;
                    }

                }


                @Override
                public void onFailure(Call<SkillListPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        myPreference.ShowDialog(getActivity(),"Oops",t.getMessage());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });

        } else{
            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.custom_failor_dialog);
            Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
            TextView TextviewSmallTitle1 = (TextView) dialog.findViewById(R.id.TextviewSmallTitle1);
            TextviewSmallTitle1.setText("There is no internet connection");
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
//        return TextViewValue;
    }




    public static Bitmap getclip(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 4, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }




}