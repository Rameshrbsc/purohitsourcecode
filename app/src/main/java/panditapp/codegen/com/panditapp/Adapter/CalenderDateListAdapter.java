package panditapp.codegen.com.panditapp.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import panditapp.codegen.com.panditapp.Pojo.PurohitSchedulingCalendarResponsePojo;
import panditapp.codegen.com.panditapp.R;

public class CalenderDateListAdapter extends BaseAdapter {
    FragmentActivity activity;
    List<PurohitSchedulingCalendarResponsePojo> temppurohitlist;
    ViewHolder holder;
    public CalenderDateListAdapter(FragmentActivity activity, List<PurohitSchedulingCalendarResponsePojo> temppurohitlist){
        this.activity = activity;
        this.temppurohitlist = temppurohitlist;
    }


    private class ViewHolder {
        TextView TextViewUserName,TextViewCatogeryName,TextViewUserPhoneNumber,TextViewAddress,TextViewPoojaName,TextViewBookingDate,TextViewTime;
    }

    @Override
    public int getCount() {
        return temppurohitlist.size();
    }

    @Override
    public Object getItem(int position) {
        return temppurohitlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_datedetails_card, viewGroup, false);
            holder = new ViewHolder();
            holder.TextViewUserName = (TextView)view.findViewById(R.id.TextViewUserName);
            holder.TextViewPoojaName = (TextView)view.findViewById(R.id.TextViewPoojaName);
            holder.TextViewUserPhoneNumber = (TextView)view.findViewById(R.id.TextViewUserPhoneNumber);
            holder.TextViewAddress = (TextView)view.findViewById(R.id.TextViewAddress);
//            holder.TextViewSubCatogeryName = (TextView)view.findViewById(R.id.TextViewSubCatogeryName);
//            holder.TextViewBookingDate = (TextView)view.findViewById(R.id.TextViewBookingDate);
            holder.TextViewTime = (TextView)view.findViewById(R.id.TextViewTime);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.TextViewUserName.setText(temppurohitlist.get(position).getUserName());
        holder.TextViewPoojaName.setText(temppurohitlist.get(position).getPoojaName());
        holder.TextViewUserPhoneNumber.setText(temppurohitlist.get(position).getUserPhoneNumber());
        holder.TextViewAddress.setText(temppurohitlist.get(position).getAddress());
//        holder.TextViewSubCatogeryName.setText(temppurohitlist.get(position).getSubCatogeryName());
//        holder.TextViewBookingDate.setText(temppurohitlist.get(position).getBookingDate());

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm:ss");
            final Date dateObj = sdf.parse(temppurohitlist.get(position).getTime());
            System.out.println(dateObj);
//            System.out.println(new SimpleDateFormat("K:mm").format(dateObj));
            holder.TextViewTime.setText(new SimpleDateFormat("HH:mm:a").format(dateObj));
        } catch (final ParseException e) {
            e.printStackTrace();
        }




        return view;
    }


}
