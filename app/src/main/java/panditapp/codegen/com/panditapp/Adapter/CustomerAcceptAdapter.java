package panditapp.codegen.com.panditapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import panditapp.codegen.com.panditapp.Activity.PoojaDetailActivity;
import panditapp.codegen.com.panditapp.Pojo.CustomerAcceptList;
import panditapp.codegen.com.panditapp.R;

public class CustomerAcceptAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<CustomerAcceptList> customerAcceptLists;
    private Context context;
    public CustomerAcceptAdapter(FragmentActivity activity, List<CustomerAcceptList> customerAcceptLists) {
        this.context = activity;
        this.customerAcceptLists = customerAcceptLists;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewDate, TextViewPooja, TextViewLocation;
        ImageView ImageViewStatus;
        CardView My_card_view;
        View ViewBorder;
        private ViewHolder(View itemView) {
            super(itemView);

            TextViewDate = (TextView) itemView.findViewById(R.id.TextViewDate);
            TextViewPooja = (TextView) itemView.findViewById(R.id.TextViewPooja);
            TextViewLocation = (TextView) itemView.findViewById(R.id.TextViewLocation);

            ImageViewStatus = (ImageView) itemView.findViewById(R.id.ImageViewStatus);

            ViewBorder = (View) itemView.findViewById(R.id.ViewBorder);

            My_card_view = (CardView) itemView.findViewById(R.id.My_card_view);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_customer_accepted_list_card, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;

        try {
            viewHolder.TextViewDate.setText(new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(customerAcceptLists.get(position).getBookingDate()))
                    );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.TextViewLocation.setText(customerAcceptLists.get(position).getAddress());
        viewHolder.TextViewPooja.setText(customerAcceptLists.get(position).getPoojaName());

        viewHolder.ViewBorder.setBackgroundColor(context.getResources().getColor(R.color.TickGreen));

        viewHolder.ImageViewStatus.setImageResource(R.mipmap.ic_tick);

        viewHolder.My_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PoojaDetailActivity.class);
                intent.putExtra("BookingID", customerAcceptLists.get(position).getBookingId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return customerAcceptLists.size();
    }
}
