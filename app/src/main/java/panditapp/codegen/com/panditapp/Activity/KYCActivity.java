package panditapp.codegen.com.panditapp.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.SignUpPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class KYCActivity extends AppCompatActivity {

    String MY_PREFS_NAME = "MyPrefsFile",UserPicURI;
    @BindView(R.id.EditTextAadhar1)
    EditText EditTextAadhar1;
    @BindView(R.id.EditTextAadhar2)
    EditText EditTextAadhar2;
    @BindView(R.id.EditTextAadhar3)
    EditText EditTextAadhar3;
    @BindView(R.id.skip)
    TextView Back;

    @BindView(R.id.panCardNumber)
    EditText panCardNumber;

    @BindView(R.id.ButtonCreateAccount)
    Button ButtonCreateAccount;
    MyPreference myPreference;
    private ProgressDialog progressBar; String EmailIDsaved,EmailIDToken, SocialType,encodedImage, UserName, UserEmailID, UserMoblieNumber,UserPassword, UserNatchtathiram,UserGothram, UserSection, UserAadhar,TempUri,UserLanguage,UserPanCard;

    private  String PurohitCaste,PurohitLocation,PurohitGender,PurohitDate,MotherTounge,Purohitvedha,PurohitSubCaste,PurohitExperience,PurohitOther;
    private static final String TAG = KYCActivity.class.getSimpleName();

     String AadhaNumber="",pancradNumber="";
    private android.support.v7.app.AlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_kyc);

        ButterKnife.bind(this);

        SharedPreferences myperf=this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        UserName = myperf.getString("PurohitName", "Default");
        UserEmailID = myperf.getString("PurohitEmailID", "Default");
        UserMoblieNumber = myperf.getString("PurohitMoblieNumber", "Default");
        UserPassword = myperf.getString("PurohitPassword", "Default");

        PurohitLocation = myperf.getString("PurohitLocation", "Default");
        PurohitGender = myperf.getString("PurohitGender", "Default");
        PurohitDate = myperf.getString("PurohitDate", "Default");
        MotherTounge = myperf.getString("MotherTounge", "Default");



        UserNatchtathiram = myperf.getString("PurohitNatchtathiram", "Default");
        UserGothram = myperf.getString("PurohitGothram", "Default");
        Purohitvedha = myperf.getString("Purohitvedha", "Default");
        PurohitCaste= myperf.getString("PurohitCaset", "Default");
        PurohitSubCaste = myperf.getString("PurohitSubCaste", "Default");
        PurohitExperience = myperf.getString("PurohitExperience", "Default");
        PurohitOther = myperf.getString("PurohitOther", "Default");
        TempUri = myperf.getString("ImageUriPath",null);

        /*UserAadhar = myperf.getString("PurohitAadhar", "Default");
        UserPanCard = myperf.getString("PurohitPanCard", "Default");
        UserLanguage = myperf.getString("PurohitLanguage", "Default");*/



        myPreference = new MyPreference(this);
        ButtonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                submit();
               // myPreference.ShowSuccessDialognew(KYCActivity.this, "Register Success", "Thank you for registering, our executive will reach out to you shortly.");

                //alertdiaaloubg("");
            }
        });
        EditTextAadhar1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (EditTextAadhar1.getText().toString().length() == 4) {
                    EditTextAadhar2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        EditTextAadhar2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (EditTextAadhar2.getText().toString().length() == 4) {
                    EditTextAadhar3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    EditTextAadhar1.requestFocus();
                }
            }
        });

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        EditTextAadhar3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    EditTextAadhar2.requestFocus();
                }

            }
        });

    }

    public void alertdiaaloubg(String message) {


        final AlertDialog alertDialog = new AlertDialog.Builder(KYCActivity.this).create();
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog.setMessage("Thank you for registering, our executive will reach out to you shortly.");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Go To Login",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        Intent intent = new Intent(KYCActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });

        alertDialog.show();

    }

    public void submit(){
        progressBar = new ProgressDialog(KYCActivity.this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Processing ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        AadhaNumber  = EditTextAadhar1.getText().toString()+EditTextAadhar2.getText().toString()+EditTextAadhar3.getText().toString();
        pancradNumber=panCardNumber.getText().toString();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
        jsonObject.addProperty("PurohitName", UserName);
        jsonObject.addProperty("PurohitEmailID",UserEmailID);
        jsonObject.addProperty("PurohitPhoneNumber", UserMoblieNumber);
        jsonObject.addProperty("PurohitPassword", UserPassword);
        jsonObject.addProperty("PurohitSection", "");


        jsonObject.addProperty("PurohitNatcharam",UserNatchtathiram);
        jsonObject.addProperty("PurohitGothram", UserGothram);
        jsonObject.addProperty("purohit_location", UserGothram);
        jsonObject.addProperty("purohit_marital_status", UserGothram);
        jsonObject.addProperty("purohit_dob", PurohitDate);
        jsonObject.addProperty("purohit_vedha", Purohitvedha);
        jsonObject.addProperty("purohit_community", PurohitCaste);
        jsonObject.addProperty("purohit_subcommunity", PurohitSubCaste);
        jsonObject.addProperty("purohit_experience", PurohitExperience);
        jsonObject.addProperty("other_community_service", PurohitOther);
        jsonObject.addProperty("PurohitAadharNumber", AadhaNumber);
        jsonObject.addProperty("PurohitPanNumber",pancradNumber);
        jsonObject.addProperty("PurohitLanguage",MotherTounge);
        if (encodedImage != null) {
            jsonObject.addProperty("PurohitProfilePic", encodedImage);
        }
        else {
            jsonObject.addProperty("PurohitProfilePic", "");
        }
        if(EmailIDsaved!=null){
            jsonObject.addProperty("PurohitsocialId", EmailIDToken);
            jsonObject.addProperty("PurohitsocialEmail", EmailIDsaved);
            if(SocialType.equals("1")){
                jsonObject.addProperty("PurohitsocialType", "1");
            }else{
                jsonObject.addProperty("PurohitsocialType", "2");
            }

        }else{
            jsonObject.addProperty("PurohitsocialId", "");
            jsonObject.addProperty("PurohitsocialEmail", "");
            jsonObject.addProperty("PurohitsocialType", "");

        }



        Log.i("kjskdjsdj",jsonObject.toString());
        API apiService = createServiceHeader(API.class);
        Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL(jsonObject);
        call.enqueue(new Callback<SignUpPojo>() {
            @Override
            public void onResponse(Call<SignUpPojo> call, Response<SignUpPojo> response) {
                progressBar.dismiss();
                switch (response.body().getCode()) {
                    case "200":
                        Toast.makeText(KYCActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                                                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
//                                                    editor.putString("AccessToken", response.body().getAccessToken());
//                                                    editor.putString("DeviceId", Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID));
//                                                    String OTP = response.body().getOtp();
//                                                    Log.i("OTPget", "" + OTP);
//                                                    editor.apply();
                        /*Intent intentOTP = new Intent(KYCActivity.this, OTPageActivity.class);
                        intentOTP.putExtra("TempAccessToken",response.body().getAccessToken());
                        intentOTP.putExtra("MoblieNumber",UserMoblieNumber);
                        startActivity(intentOTP);*/

                        String test1="Thank you for registering with us. ";
                        String regsId=" ";
                        String test2=" Your Application is under process, You will be shortly notified !";
                        final android.support.v7.app.AlertDialog.Builder imageDialog = new android.support.v7.app.AlertDialog.Builder(KYCActivity.this, R.style.RajCustomDialog);
                        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                        final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
                        imageDialog.setView(dialogView);
                        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                        TextviewSmallTitle1.setText(response.body().getMessage());
                        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                        TextviewBigTitle.setText(test1+test2);
                        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                        ButtonOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("login", "1");
                                editor.apply();
                                editor.commit();
                                dialog.dismiss();
                                Intent intent = new Intent(KYCActivity.this, LoginActivity.class);
                                startActivity(intent);



                            }
                        });
                        dialog = imageDialog.create();
                        dialog.show();

                        break;

                    case "101":
                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("login", "1");
                        editor.apply();
                        editor.commit();
                        succeessMetho();

                        break;

                    case "108":
                        Toast.makeText(KYCActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(KYCActivity.this, LoginActivity.class);
                        startActivity(intent);
                        break;
                    default:
                        myPreference.ShowDialog(KYCActivity.this,"Oops",response.body().getMessage());
                        break;
                }
            }

            @Override
            public void onFailure(Call<SignUpPojo> call, Throwable t) {
                progressBar.dismiss();
                try {
                    myPreference.ShowDialog(KYCActivity.this,"Oops",t.getMessage());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        });




    }

    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }


    public void succeessMetho(){


        String test1="Thank you for registering with us. ";
        String regsId=" ";
        String test2=" Your Application is under process, You will be shortly notified !";
        final android.support.v7.app.AlertDialog.Builder imageDialog = new android.support.v7.app.AlertDialog.Builder(KYCActivity.this, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
        imageDialog.setView(dialogView);
        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
       // TextviewSmallTitle1.setText("");
        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
        TextviewBigTitle.setText(test1+test2);
        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
        ButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("login", "1");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(KYCActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        dialog = imageDialog.create();
        dialog.show();
    }
}
