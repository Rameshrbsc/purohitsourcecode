package panditapp.codegen.com.panditapp.Pojo;

public class ServiceAreaPoojaId {
    private String PoojaId;

    private String AreaId;

    private String AreaName;

    private String Latitude;

    private String Longtitude;

    private String ServiceId;

    private String PoojaName;

    public String getPoojaId ()
    {
        return PoojaId;
    }

    public void setPoojaId (String PoojaId)
    {
        this.PoojaId = PoojaId;
    }

    public String getAreaId ()
    {
        return AreaId;
    }

    public void setAreaId (String AreaId)
    {
        this.AreaId = AreaId;
    }

    public String getAreaName ()
    {
        return AreaName;
    }

    public void setAreaName (String AreaName)
    {
        this.AreaName = AreaName;
    }

    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }

    public String getLongtitude ()
    {
        return Longtitude;
    }

    public void setLongtitude (String Longtitude)
    {
        this.Longtitude = Longtitude;
    }

    public String getServiceId ()
    {
        return ServiceId;
    }

    public void setServiceId (String ServiceId)
    {
        this.ServiceId = ServiceId;
    }

    public String getPoojaName ()
    {
        return PoojaName;
    }

    public void setPoojaName (String PoojaName)
    {
        this.PoojaName = PoojaName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PoojaId = "+PoojaId+", AreaId = "+AreaId+", AreaName = "+AreaName+", Latitude = "+Latitude+", Longtitude = "+Longtitude+", ServiceId = "+ServiceId+", PoojaName = "+PoojaName+"]";
    }
}
