package panditapp.codegen.com.panditapp.Pojo;

public class Subcommunites_data {
    String SubCommunityId;
    String SubCommunityName;
    String IsActive;
    String IsDelete;
    String CommunityId;

    public String getCommunityId() {
        return CommunityId;
    }

    public void setCommunityId(String communityId) {
        CommunityId = communityId;
    }

    public String getSubCommunityId() {
        return SubCommunityId;
    }

    public void setSubCommunityId(String subCommunityId) {
        SubCommunityId = subCommunityId;
    }

    public String getSubCommunityName() {
        return SubCommunityName;
    }

    public void setSubCommunityName(String subCommunityName) {
        SubCommunityName = subCommunityName;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getIsDelete() {
        return IsDelete;
    }

    public void setIsDelete(String isDelete) {
        IsDelete = isDelete;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [SubCommunityId = "+SubCommunityId+", CommunityId = "+CommunityId+", SubCommunityName = "+SubCommunityName+", IsActive = "+IsActive+", IsDelete = "+IsDelete+"]";
    }
}
