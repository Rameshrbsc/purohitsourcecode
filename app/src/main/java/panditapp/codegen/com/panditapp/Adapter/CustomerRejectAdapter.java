package panditapp.codegen.com.panditapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import panditapp.codegen.com.panditapp.Pojo.CustomerAcceptList;
import panditapp.codegen.com.panditapp.Pojo.CustomerRejectList;
import panditapp.codegen.com.panditapp.R;

public class CustomerRejectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<CustomerRejectList> customerRejectLists;
    private Context context;

    public CustomerRejectAdapter(FragmentActivity activity, List<CustomerRejectList> customerRejectLists) {
        this.context = activity;
        this.customerRejectLists = customerRejectLists;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewDate, TextViewPooja, TextViewLocation;
        ImageView ImageViewStatus;
        View ViewBorder;
        private ViewHolder(View itemView) {
            super(itemView);

            TextViewDate = (TextView) itemView.findViewById(R.id.TextViewDate);
            TextViewPooja = (TextView) itemView.findViewById(R.id.TextViewPooja);
            TextViewLocation = (TextView) itemView.findViewById(R.id.TextViewLocation);

            ImageViewStatus = (ImageView) itemView.findViewById(R.id.ImageViewStatus);

            ViewBorder = (View) itemView.findViewById(R.id.ViewBorder);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_customer_accepted_list_card, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;

        try {
            viewHolder.TextViewDate.setText(new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(customerRejectLists.get(position).getBookingDate()))
            );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.TextViewLocation.setText(customerRejectLists.get(position).getAddress());
        viewHolder.TextViewPooja.setText(customerRejectLists.get(position).getPoojaName());

        viewHolder.ViewBorder.setBackgroundColor(context.getResources().getColor(R.color.CancelRed));

        viewHolder.ImageViewStatus.setImageResource(R.mipmap.ic_close);
    }

    @Override
    public int getItemCount() {
        return customerRejectLists.size();
    }
}
