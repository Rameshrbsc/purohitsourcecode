package panditapp.codegen.com.panditapp.Service;

public class Config {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String NOTIFICATION_ICON = "notification_icon";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";

    public static final String PurohitName = "PurohitName";
    public static final String PurohitEmailID = "PurohitEmailID";
    public static final String PurohitMoblieNumber = "PurohitMoblieNumber";
    public static final String PurohitPassword = "ah_firebase";
    public static final String PurohitNatchtathiram = "PurohitNatchtathiram";
    public static final String PurohitGothram = "PurohitGothram";
    public static final String PurohitSection = "PurohitSection";
    public static final String PurohitAadhar = "PurohitAadhar";
    public static final String PurohitPanCard = "PurohitPanCard";
    public static final String PurohitLanguage = "PurohitLanguage";
    public static final String ImageUriPath = "ImageUriPath";
    public static final String Language = "Language";
    public static final String Location = "Location";
    public static final String Gothram = "Gothram";
    public static final String Natchtathiram = "Natchtathiram";
    public static final String DOB = "DOB";
    public static final String Experience = "Experience";
    public static final String About = "About";




}
