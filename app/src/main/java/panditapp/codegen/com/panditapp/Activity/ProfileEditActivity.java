package panditapp.codegen.com.panditapp.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.PurohitProfileUpdatePojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitProfileViewPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitProfileViewResponsePojo;
import panditapp.codegen.com.panditapp.Pojo.SkillListPojo;
import panditapp.codegen.com.panditapp.Pojo.SkillListResponsePojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Activity.SignUpActivity.rotateImage;
import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class ProfileEditActivity extends AppCompatActivity {
    private ProgressDialog progressBar;
    MyPreference myPreference;
    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextUserEmail)
    EditText EditTextUserEmail;
    @BindView(R.id.EditTextNatchtathiram)
    EditText EditTextNatchtathiram;
    @BindView(R.id.EditTextGothram)
    EditText EditTextGothram;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextSection)
    EditText EditTextSection;
    @BindView(R.id.EditTextAadhar1)
    EditText EditTextAadhar1;
    @BindView(R.id.EditTextAadhar2)
    EditText EditTextAadhar2;
    @BindView(R.id.EditTextAadhar3)
    EditText EditTextAadhar3;
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.ImageViewProfilePic)
    ImageView ImageViewProfilePic;
    @BindView(R.id.EditTextPanCard)
    EditText EditTextPanCard;
    @BindView(R.id.EditTextLocaiton)
    EditText EditTextLocaiton;
    @BindView(R.id.EditTextLanguage)
    EditText EditTextLanguage;
    @BindView(R.id.EditTextExperience)
    EditText EditTextExperience;
    @BindView(R.id.EditTextAbout)
    EditText EditTextAbout;
    @BindView(R.id.TextViewSkillsDropDownBox)
    EditText TextViewSkillsDropDownBox;
    protected ArrayList<CharSequence> SelectedName = new ArrayList<CharSequence>();
    protected ArrayList<CharSequence> selectedSkillID = new ArrayList<CharSequence>();
    String [] SkillListId;
    Object[] SkillId;
    String [] SkillListName;
    @BindView(R.id.FloatingActionButtonEditProfile)
    FloatingActionButton FloatingActionButtonEditProfile;
    String userChoosenTask;
    private Uri currentImageUri;
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    AlertDialog alertDialog,dialog;
    Boolean wantToCloseDialog = false;
    Bitmap thumbnail = null, bitmap;
    File dir = new File(Environment.getExternalStorageDirectory() + "/PurohitUserApp/.Images");
    //keep track of cropping intent
    String encodedImage,FilePath;
    File camimage;
    PurohitProfileViewResponsePojo purohitProfileViewResponsePojo;
    Bundle bundle;

    private static final int REQUEST_PERMISSION_SETTING = 151;

    static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

    ArrayList<String> SkillResponseId,SkillIdResponseName;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purohit_profile_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Profile Edit");
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);
//        final Intent intent = getIntent();
        toolbar.setNavigationIcon(R.drawable.ic_back_while);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
        bundle = getIntent().getExtras();
        FloatingActionButtonEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(ProfileEditActivity.this, permissionsRequired, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        });

        if(bundle.containsKey("Operation") && bundle.getString("Operation", "").equals("NewUpdate")){
            if (myPreference.isInternetOn()) {
                progressBar = new ProgressDialog(this);
                progressBar.setCancelable(false);
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.setMessage("Processing...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                API apiService = createServiceHeader(API.class);
                Call<PurohitProfileViewPojo> call = apiService.USER_PROFILE_VIEW_POJO_CALL(jsonObject);
                call.enqueue(new Callback<PurohitProfileViewPojo>() {
                    @Override
                    public void onResponse(Call<PurohitProfileViewPojo> call, Response<PurohitProfileViewPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                purohitProfileViewResponsePojo = response.body().getResponse();
                                EditTextUserName.setText(purohitProfileViewResponsePojo.getPurohitName());
                                EditTextMoblieNumber.setText(purohitProfileViewResponsePojo.getPurohitPhoneNumber());
                                EditTextUserEmail.setText(purohitProfileViewResponsePojo.getPurohitEmailID());
                                EditTextSection.setText(purohitProfileViewResponsePojo.getPurohitSection());
                                EditTextNatchtathiram.setText(purohitProfileViewResponsePojo.getPurohitNatcharam());
                                EditTextGothram.setText(purohitProfileViewResponsePojo.getPurohitGothram());
                                String Aadharnumber = purohitProfileViewResponsePojo.getPurohitAadharNumber();
//                                String[] separate = Arrays.toString(splitToNChar(Aadharnumber,4)).split(",");
                                if (Aadharnumber != null && Aadharnumber.length() == 12){
                                    EditTextAadhar1.setText(Aadharnumber.substring(0,4));
                                    EditTextAadhar2.setText(Aadharnumber.substring(4,8));
                                    EditTextAadhar3.setText(Aadharnumber.substring(8,12));
                                }
                                EditTextPanCard.setText(purohitProfileViewResponsePojo.getPurohitPanNumber());
                                try{
                                    Log.i("Image",""+purohitProfileViewResponsePojo.getPurohitProfilePic());
                                    if(purohitProfileViewResponsePojo.getPurohitProfilePic() != null){
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                            Picasso.with(ProfileEditActivity.this).load(purohitProfileViewResponsePojo.getPurohitProfilePic()).transform(new CircleTransform()).into(ImageViewProfilePic);
                                        }

                                    }else{
                                        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.panditlogo);
                                        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), getclip(largeIcon));
                                        roundDrawable.setCircular(true);
                                        ImageViewProfilePic.setImageDrawable(roundDrawable);
                                    }

                                }catch (Exception e){e.printStackTrace();}

                                LoadSkillListSpinner();
                                break;
                            case "108":
                                Intent intentemg = new Intent(ProfileEditActivity.this, LoginActivity.class);
                                startActivity(intentemg);
                                break;
                            default:
                                myPreference.ShowDialog(ProfileEditActivity.this,"Oops",response.body().getMessage());
                                break;
                        }

                    }

                    @Override
                    public void onFailure(Call<PurohitProfileViewPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(ProfileEditActivity.this,"Oops",t.getMessage());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    }
                });

            }else {
                myPreference.ShowDialog(ProfileEditActivity.this,"Oops","There is no internet connection");


            }
        }else if(bundle.containsKey("Operation") && bundle.getString("Operation", "").equals("Update")){
            EditTextUserName.setText(bundle.getString("Name"));
            EditTextMoblieNumber.setText(bundle.getString("MoblieNumber"));
            EditTextUserEmail.setText(bundle.getString("EmailID"));
            EditTextSection.setText(bundle.getString("Gothram"));
            EditTextNatchtathiram.setText(bundle.getString("Natchtathiram"));
            EditTextGothram.setText(bundle.getString("Section"));
            String Aadharnumber = bundle.getString("Aadhar");
//            String[] separate = Arrays.toString(splitToNChar(Aadharnumber,4)).split(", ");
            if (Aadharnumber != null && Aadharnumber.length() == 12){
                EditTextAadhar1.setText(Aadharnumber.substring(0,4));
                EditTextAadhar2.setText(Aadharnumber.substring(4,8));
                EditTextAadhar3.setText(Aadharnumber.substring(8,12));
            }
            EditTextPanCard.setText(bundle.getString("PanCard"));
            EditTextLanguage.setText(bundle.getString("Language"));
            EditTextLocaiton.setText(bundle.getString("Location"));
            EditTextExperience.setText(bundle.getString("Experience"));
            EditTextAbout.setText(bundle.getString("About"));

            SkillResponseId = bundle.getStringArrayList("SkillIdResponseId");
            SkillIdResponseName = bundle.getStringArrayList("SkillIdResponseName");
            String[] values = bundle.getString("Skills").split(",");
            SelectedName.addAll(Arrays.asList(values));
            selectedSkillID.addAll(SkillResponseId);
            StringBuilder stringBuilder = new StringBuilder();
            for(CharSequence colour : SelectedName)
                stringBuilder.append(colour + ",");
            TextViewSkillsDropDownBox.setText(stringBuilder.deleteCharAt(stringBuilder.length()-1).toString());
            try{
                Log.i("PicURI",""+bundle.getString("PicURI"));
                if(bundle.getString("PicURI") != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        Picasso.with(ProfileEditActivity.this).load(bundle.getString("PicURI")).transform(new CircleTransform()).into(ImageViewProfilePic);
                    }

                }else{
                    Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.panditlogo);
                    RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), getclip(largeIcon));
                    roundDrawable.setCircular(true);
                    ImageViewProfilePic.setImageDrawable(roundDrawable);
                }

            }catch (Exception e){e.printStackTrace();}
            LoadSkillListSpinner();
        }

        TextViewSkillsDropDownBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean[] checkedColours = new boolean[SkillListName.length];
                for(int i = 0; i < SkillListName.length; i++)
                    checkedColours[i] = SelectedName.contains(SkillListName[i]);
                try {
                    DialogInterface.OnMultiChoiceClickListener coloursDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            checkedColours[which] = isChecked;

//                            if(isChecked){
//                                 SelectedName.add(SkillListName[which]);
//                                selectedSkillID.add(SkillListId[which]);
//                                StringBuilder stringBuilder = new StringBuilder();
//                                for(CharSequence colour : SelectedName)
//                                    stringBuilder.append(colour + ",");
//                                TextViewSkillsDropDownBox.setText(stringBuilder.deleteCharAt(stringBuilder.length()-1).toString());
//                            }else{
//                                SelectedName.remove(SkillListName[which]);
//                                selectedSkillID.add(SkillListId[which]);
//                                StringBuilder stringBuilder = new StringBuilder();
//                                for(CharSequence colour : SelectedName)
//                                    stringBuilder.append(colour + ",");
//                                TextViewSkillsDropDownBox.setText(stringBuilder.deleteCharAt(stringBuilder.length()-1).toString());
//                            }
                        }

                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProfileEditActivity.this);
                    builder.setTitle("Select Your Skills");
                    builder.setMultiChoiceItems(SkillListName, checkedColours, coloursDialogListener);
                    builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

//                            SelectedName.add(SkillListName[which]);
//                            selectedSkillID.add(SkillListId[which]);
//                            StringBuilder stringBuilder = new StringBuilder();
//                            for(CharSequence colour : SelectedName)
//                                stringBuilder.append(colour + ",");
//                            TextViewSkillsDropDownBox.setText(stringBuilder.deleteCharAt(stringBuilder.length()-1).toString());

                        }
                    });
                    dialog = builder.create();
                    dialog.show();

                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SelectedName = new ArrayList<CharSequence>();
                            selectedSkillID = new ArrayList<CharSequence>();
                            if (areAllTrue(checkedColours)) {
                                // true

                                for(int i =0 ;i<checkedColours.length ; i ++){

                                    if(checkedColours[i]){
                                        SelectedName.add(SkillListName[i]);
                                        selectedSkillID.add(SkillListId[i]);
                                        StringBuilder stringBuilder = new StringBuilder();
                                        for(CharSequence colour : SelectedName)
                                            stringBuilder.append(colour + ",");
                                        TextViewSkillsDropDownBox.setText(stringBuilder.deleteCharAt(stringBuilder.length()-1).toString());
                                        dialog.dismiss();
                                    }

                                }
                            }else{
                                myPreference.ShowDialog(ProfileEditActivity.this,"Oops","Must Select Atleast One Skill");
                            }

                        }
                    });

                }catch (Exception e){e.printStackTrace();}



            }
        });


        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()) {
                    final String AadhaNumber = EditTextAadhar1.getText().toString()  + EditTextAadhar2.getText().toString()  + EditTextAadhar3.getText().toString();

                    if(AadhaNumber.length()==12){
                        if(Check(EditTextUserName) &&Check(EditTextUserEmail)&&Check(EditTextMoblieNumber) && Check(EditTextSection) &&Check(EditTextUserName) &&Check(EditTextNatchtathiram)
                                && Check(EditTextGothram)&& Check(EditTextPanCard)&&Check(EditTextLocaiton)&&Check(EditTextExperience) && Check(EditTextAbout)&& Check(TextViewSkillsDropDownBox
                        )){
                            if(selectedSkillID.size() > 0 ){
                                ButtonSubmit.setVisibility(View.GONE);
                                progressBar = new ProgressDialog(ProfileEditActivity.this);
                                progressBar.setCancelable(false);
                                progressBar.setCanceledOnTouchOutside(false);
                                progressBar.setMessage("Processing ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();
                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                jsonObject.addProperty("PurohitName", EditTextUserName.getText().toString());
                                jsonObject.addProperty("PurohitEmailID", EditTextUserEmail.getText().toString());
                                jsonObject.addProperty("PurohitPhoneNumber", EditTextMoblieNumber.getText().toString());
                                jsonObject.addProperty("PurohitSection", EditTextSection.getText().toString());//send section data
                                jsonObject.addProperty("PurohitNatcharam", EditTextNatchtathiram.getText().toString());
                                jsonObject.addProperty("PurohitGothram", EditTextGothram.getText().toString());
                                jsonObject.addProperty("PurohitLanguage",EditTextLanguage.getText().toString());
                                jsonObject.addProperty("PurohitAadharNumber", AadhaNumber);
                                jsonObject.addProperty("PurohitPanNumber",EditTextPanCard.getText().toString());
                                jsonObject.addProperty("PurohitLocation",EditTextLocaiton.getText().toString());

                                StringBuilder stringBuilder = new StringBuilder();
                                for(CharSequence PoojaID : selectedSkillID)
                                    stringBuilder.append(PoojaID).append(",");
                                Log.e("PoojaID", stringBuilder.deleteCharAt(stringBuilder.length()-1).toString());
                                jsonObject.addProperty("PurohitSkills",stringBuilder.toString());

                                jsonObject.addProperty("PurohitExperience",EditTextExperience.getText().toString());
                                jsonObject.addProperty("PurohitAbout",EditTextAbout.getText().toString());
                                if (encodedImage != null) {
                                    jsonObject.addProperty("PurohitProfilePic", encodedImage);
                                } else {
                                    jsonObject.addProperty("PurohitProfilePic", "");
                                }
                                API apiService = createServiceHeader(API.class);
                                Call<PurohitProfileUpdatePojo> call = apiService.USER_PROFILE_UPDATE_POJO_CALL(jsonObject);
                                Log.e("json",jsonObject.toString());
                                Log.i("jsonValues",""+jsonObject.toString());
                                call.enqueue(new Callback<PurohitProfileUpdatePojo>() {
                                    @Override
                                    public void onResponse(Call<PurohitProfileUpdatePojo> call, Response<PurohitProfileUpdatePojo> response) {
                                        ButtonSubmit.setVisibility(View.VISIBLE);
                                        progressBar.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
                                                Intent intent = new Intent(ProfileEditActivity.this, NavigationActivity.class);
                                                startActivity(intent);
                                                break;
                                            case "108":
                                                Intent logoutintent = new Intent(ProfileEditActivity.this, LoginActivity.class);
                                                startActivity(logoutintent );
                                                break;
                                            default:
                                                myPreference.ShowDialog(ProfileEditActivity.this,"Oops",response.body().getMessage());

                                                break;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<PurohitProfileUpdatePojo> call, Throwable t) {
                                        progressBar.dismiss();
                                        try {
                                            myPreference.ShowDialog(ProfileEditActivity.this,"Oops",t.getMessage());

                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }else{
                                Toast.makeText(ProfileEditActivity.this, "Must Select one skill", Toast.LENGTH_SHORT).show();

                            }
                        }  else{
                            Toast.makeText(ProfileEditActivity.this, "Field missing", Toast.LENGTH_SHORT).show();

                        }
                    }else{
                        Toast.makeText(ProfileEditActivity.this, "Invalid Aadhar Number", Toast.LENGTH_SHORT).show();

                    }



                }
                else {
                    myPreference.ShowDialog(ProfileEditActivity.this,"Oops!!","There is no internet connection");
                }
            }
        });


    }

    private void LoadSkillListSpinner() {
        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(this);
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            API apiService = createServiceHeader(API.class);
            Call<SkillListPojo> call = apiService.SKILL_LIST_POJO_CALL(jsonObject);
            call.enqueue(new Callback<SkillListPojo>() {
                @Override
                public void onResponse(Call<SkillListPojo> call, Response<SkillListPojo> response) {

                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            SkillListResponsePojo[] skillListResponsePojos = response.body().getResponse();
                            SkillListName = new String[skillListResponsePojos.length];
                            SkillListId = new String[skillListResponsePojos.length];
                            for(int i =0; i<skillListResponsePojos.length;i++){
                                SkillListName[i] = skillListResponsePojos[i].getSkillName();
                                SkillListId[i]= skillListResponsePojos[i].getSkillId();
                                Log.i("SkillListName",""+SkillListName[i]);
                            }

                            TextViewSkillsDropDownBox.setTextColor(Color.BLACK);
                            break;
                        case "108":
                            Intent logoutintent = new Intent(ProfileEditActivity.this, LoginActivity.class);
                            startActivity(logoutintent );
                            break;
                        default:
                            myPreference.ShowDialog(ProfileEditActivity.this,"Oops",response.body().getMessage());

                            break;
                    }

                }

                @Override
                public void onFailure(Call<SkillListPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        myPreference.ShowDialog(ProfileEditActivity.this,"Oops!!",t.getMessage());

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });

        } else{
            myPreference.ShowDialog(ProfileEditActivity.this,"Oops!!","There is no internet connection");
        }

    }





    private void selectImage() {
        final String [] value= getResources().getStringArray(R.array.Camera);
        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.camera_popup, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileEditActivity.this);
        alertDialogBuilder.setView(promptsView);
        ListView lv = (ListView) promptsView.findViewById(R.id.selectmode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, value);
        lv.setAdapter(adapter);
        lv.setSelector(R.color.colorPrimary);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    alertDialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    dir.mkdirs();

                    if (dir.isDirectory()){
                        Log.i("FileCreated","FileCreated");

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        camimage = new File(dir,"PurhoitAPP"+ timeStamp + ".jpg");
                        currentImageUri = Uri.fromFile( camimage );//file
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    }else {
                        Log.i("FileNotCreated","FileNotCreated");
                    }

                } else if (position == 1) {
                    alertDialog.dismiss();
                    startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), SELECT_FILE);
                    Log.i("onsave","going to gallery ");
                }else  if(position == 2){
                    alertDialog.dismiss();
                    userChoosenTask="Cancel";


                }
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {//camera
                camimage= new File(currentImageUri.getPath());
                Log.i("Imagetest",""+camimage);
//                try {
////                    thumbnail = (Bitmap) data.getExtras().get("data");
////                    Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(currentImageUri));
//                    CamLoadImage(BitmapFactory.decodeStream(getContentResolver().openInputStream(currentImageUri)));
//                    EncodedMethodCall();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
                if(currentImageUri!=null){
                    try {
                        FilePath = currentImageUri.getPath();
                        LoadImage(camimage);
                        EncodedMethodCall();
//                        EncodedMethodCall();
//
//                        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
//                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//
//                        CamLoadImage(decodedByte);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (requestCode == SELECT_FILE) {
                currentImageUri = data.getData();
                try {
                    thumbnail = MediaStore.Images.Media.getBitmap(ProfileEditActivity.this.getApplicationContext().getContentResolver(), data.getData());
                    Log.i("onsave","data got from gallery ");

                    FilePath = getRealPathFromURI(ProfileEditActivity.this,currentImageUri);
                    camimage = new File(FilePath);
                    ImageFileFilter imageFileFilter = new ImageFileFilter(camimage);
                    if(imageFileFilter.accept(camimage)){
                        Log.i("GalleryImageLoaded","GalleryImageLoaded");
                        LoadImage(camimage);
                        EncodedMethodCall();
                    }else{
                        Toast.makeText(this, "Unsupported File", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }else if (requestCode == REQUEST_PERMISSION_SETTING){
            if (checkPermissions()){
                selectImage();
            }
        }
    }
    private void LoadImage(File Path) {
        Picasso.with(this).load(Path).transform(new SignUpActivity.CircleTransform()).into(ImageViewProfilePic);
        alertDialog.dismiss();
    }
    private void CamLoadImage(Bitmap thumbnail) {
        ExifInterface ei = null;
        int orientation = 0;
        try {
            ei = new ExifInterface(FilePath);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                thumbnail = rotateImage(thumbnail, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                thumbnail = rotateImage(thumbnail, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                thumbnail = rotateImage(thumbnail, 270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                thumbnail = thumbnail;
        }

        ImageViewProfilePic.setImageBitmap(thumbnail);
        ImageViewProfilePic.setVisibility(View.VISIBLE);
//        RelativelayoutImageView.setVisibility(View.VISIBLE);
//        closebtn.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }

    private String getRealPathFromURI(FragmentActivity activity, Uri selectedImageUri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private void EncodedMethodCall() {
        Bitmap bm = BitmapFactory.decodeFile(camimage.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        Log.i("encodedImage",""+encodedImage);
    }

    class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public class ImageFileFilter implements FileFilter {
        File file;
        private final String[] okFileExtensions = new String[]{"jpg", "png", "jpeg"};

        public ImageFileFilter(File newfile) {
            this.file = newfile;
        }

        public boolean accept(File file) {
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }
    }

    private static String[] splitToNChar(String text, int size) {
        List<String> parts;
        parts = new ArrayList<>();

        int length = text.length();
        for (int i = 0; i < length; i += size) {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }

    //validations
    boolean Check(EditText testEditText){
        return testEditText.getText().toString().length()>0 && !testEditText.getText().toString().isEmpty();
    }
    boolean CheckTextView(TextView testEditText){
        return testEditText.getText().toString().length()>0 && !testEditText.getText().toString().isEmpty() && !testEditText.equals("Select your Skills Here") ;
    }
    public static Bitmap getclip(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 4, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static boolean areAllTrue(boolean[] array)
    {
        for(boolean b : array){
            if(b){
                return true;
            }
        }
        return false;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE){
            if (checkPermissions()){
                selectImage();
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage access permission is required to read and write files from your mobile. Enable it to upload photo\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

}