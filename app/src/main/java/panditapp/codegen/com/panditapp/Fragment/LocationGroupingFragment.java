package panditapp.codegen.com.panditapp.Fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LocationCreateGroupingActivity;
import panditapp.codegen.com.panditapp.Activity.LocationSearchingActivity;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Activity.SeerviceMappingActivity;
import panditapp.codegen.com.panditapp.Adapter.LocationGroupingAdapter;
import panditapp.codegen.com.panditapp.Pojo.DashboardListPojo;
import panditapp.codegen.com.panditapp.Pojo.ServiceAreaPoojaId;
import panditapp.codegen.com.panditapp.Pojo.ServiceMappingListPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationGroupingFragment extends Fragment {

    Unbinder unbinder;
    //Bind FloatingActionButton
    @BindView(R.id.ButtonAddLocation)
    FloatingActionButton ButtonAddLocation;

    //Bind RecyclerView
    @BindView(R.id.RecyclerViewService)
    RecyclerView RecyclerViewService;

    //Bind SwipeRefreshLayout
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    //Bind LinearLayout
    @BindView(R.id.mainHolder)
    LinearLayout linearLayout;
    @BindView(R.id.LinearLayoutNoData)
    LinearLayout LinearLayoutNoData;

    ProgressDialog progressBar;

    List<ServiceAreaPoojaId> serviceAreaPoojaIds;

    LocationGroupingAdapter locationGroupingAdapter;

    MyPreference myPreference;

    public LocationGroupingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location_grouping, container, false);
        unbinder = ButterKnife.bind(this, view);

        getActivity().setTitle("Service Mapping");

        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerViewService.setLayoutManager(llm);
        RecyclerViewService.setHasFixedSize(true);

        myPreference = new MyPreference(getActivity());

        ButtonAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LocationCreateGroupingActivity.class);
                intent.putExtra("RequestingType", "Create");
                startActivity(intent);
            }
        });

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                clearData();

                swipeContainer.setRefreshing(true);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        check();
                        swipeContainer.setRefreshing(false);
                    }
                }, 1000);


            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_orange_light,

                android.R.color.holo_green_dark);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("Stage", "onResume");
        check();
    }

    /*final AlertDialog.Builder imageDialog = new AlertDialog.Builder(getContext(), R.style.RajCustomDialog);
    final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE );
    final View dialogView = inflater.inflate(R.layout.custom_progress, null);
        imageDialog.setView(dialogView);
    final AlertDialog alertDialog = imageDialog.create();*/
    Dialog dialog;

    private void check() {
        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(getContext(), R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.custom_progress, null);
        imageDialog.setView(dialogView);
        dialog = imageDialog.create();

        if (myPreference.isInternetOn()) {
            linearLayout.setVisibility(View.VISIBLE);
          /*  progressBar = new ProgressDialog(getContext());
            progressBar.setCancelable(true);
            progressBar.setMessage("Loading ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();*/

            dialog.show();
            ServerData();
        } else {
            myPreference.ShowDialog(getActivity(), "Oops", "Please turn on the internet connection");
            linearLayout.setVisibility(View.GONE);
        }
    }

    private void ServerData() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);

        API apiService = createServiceHeader(API.class);
        Call<ServiceMappingListPojo> call = apiService.VIEW_GROUPING_POJO_CALL(jsonObject);
        call.enqueue(new Callback<ServiceMappingListPojo>() {
            @Override
            public void onResponse(Call<ServiceMappingListPojo> call, Response<ServiceMappingListPojo> response) {
                dialog.dismiss();
                switch (response.body().getCode()) {
                    case "200":
                        if (response.body().getServiceAreaPoojaId().length > 0) {
                            swipeContainer.setVisibility(View.VISIBLE);
                            LinearLayoutNoData.setVisibility(View.GONE);
                            serviceAreaPoojaIds = new ArrayList<>();
                            Collections.addAll(serviceAreaPoojaIds, response.body().getServiceAreaPoojaId());
                            locationGroupingAdapter = new LocationGroupingAdapter(getActivity(), serviceAreaPoojaIds, new LocationGroupingAdapter.RefreshData() {
                                @Override
                                public void Refresh_Data() {
                                    check();
                                }
                            });
                            RecyclerViewService.setAdapter(locationGroupingAdapter);
                        } else {
                            swipeContainer.setVisibility(View.GONE);
                            LinearLayoutNoData.setVisibility(View.VISIBLE);
                        }
                        break;
                    case "108":
                        Toast.makeText(getActivity(), "Login Required", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), LoginActivity.class));
//                            getActivity().finish();
                        break;
                    default:
                        myPreference.ShowDialog(getActivity(), "Oops", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ServiceMappingListPojo> call, Throwable t) {
                dialog.dismiss();
                try {
                    myPreference.ShowDialog(getActivity(), "Exception", "Something went wrong, Try again later!!!");

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbind the view to free some memory
        unbinder.unbind();
    }
}
