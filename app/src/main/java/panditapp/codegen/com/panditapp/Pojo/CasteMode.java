package panditapp.codegen.com.panditapp.Pojo;

public class CasteMode {

    String id;
    String comunityId;
    String subCommunityName;
    String isActive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComunityId() {
        return comunityId;
    }

    public void setComunityId(String comunityId) {
        this.comunityId = comunityId;
    }

    public String getSubCommunityName() {
        return subCommunityName;
    }

    public void setSubCommunityName(String subCommunityName) {
        this.subCommunityName = subCommunityName;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
