package panditapp.codegen.com.panditapp.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.facebook.CallbackManager;
//import com.facebook.login.widget.LoginButton;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitSessionAuthPojo;
import panditapp.codegen.com.panditapp.Pojo.SoicalLoginPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity  implements View.OnClickListener ,GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.TextViewSignup)
    TextView TextViewSignup;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextPassword)
    EditText EditTextPassword;
    @BindView(R.id.ButtonLogin)
    Button ButtonLogin;
    @BindView(R.id.checkboxRememberPassword)
    CheckBox checkboxRememberPassword;
    @BindView(R.id.FrameLayout1)
    FrameLayout FrameLayout1;
    @BindView(R.id.TextViewForgetpassword)
    TextView TextViewForgetpassword;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile";
    String MoblieNumber,Password,Checkbox,FacebookID,Facebookemail,FacebookName,GmailpersonName ,GmailEmail,GmailId;
    @BindView(R.id.ImageViewGoogleLoginButton)
    ImageView ImageViewGoogleLoginButton;
    private ProgressDialog progressBar;
    //google login api and instance
    private GoogleApiClient googleApiClient;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;

    CallbackManager callbackManager;
    LoginButton loginButton;
    private static final String EMAIL = "email";

    //fb
    @BindView(R.id.ImageViewFacebookButton)
    ImageView  ImageViewFacebookButton;

    private AlertDialog dialog;
    Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setTheme(R.style.AppTheme);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        Bundle extras = getIntent().getExtras();
        bundle = new Bundle();
        if(extras != null){
            if(extras.containsKey("Target"))
            {
                // extract the extra-data in the Notification
                if (extras.getString("Target") != null && !(extras.getString("Target").equals("null"))){
                    bundle.putString("Target", extras.getString("Target"));
                    bundle.putString("TargetID", extras.getString("TargetID"));
                    Log.e("Target", extras.getString("Target"));
                    Log.e("TargetID", extras.getString("TargetID"));
                }else {
                    bundle.putString("Target", "");
                    bundle.putString("TargetID", "");
                }
            }else {
                bundle.putString("Target", "");
                bundle.putString("TargetID", "");
            }
        }else {
            bundle.putString("Target", "");
            bundle.putString("TargetID", "");
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // bind the view using butterknife
        ButterKnife.bind(this);
        myPreference = new MyPreference(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        ImageViewFacebookButton.setOnClickListener(this);
        callbackManager = CallbackManager.Factory.create();

        EditTextMoblieNumber.setText("");
        EditTextPassword.setText("");


//
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "panditapp.codegen.com.panditapp",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("com.example.fishe", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }
//        String first = "Don't Have an Account? ";
        String next = "<font color='#F57C13'>SignUp</font>";
        TextViewSignup.setText(Html.fromHtml(/*first + */next));
        TextViewForgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                intent.putExtra("CreatedBy","ForgetPassword");
                startActivity(intent);
            }
        });

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        MoblieNumber = prefs.getString("MoblieNumber","");
        Password = prefs.getString("Password","");
        Checkbox = prefs.getString("CheckBoxTrue","1");
        if(Checkbox.equals("0")){
            checkboxRememberPassword.setChecked(true);
            EditTextMoblieNumber.setText(MoblieNumber);
            EditTextPassword.setText(Password);
        }else{
            checkboxRememberPassword.setChecked(false);
            EditTextMoblieNumber.setText("");
            EditTextPassword.setText("");
        }
        TextViewSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });


        if (myPreference.getDefaultRunTimeValue()[0] != null){
            if (myPreference.isInternetOn()){
                progressBar = new ProgressDialog(LoginActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                API apiService = createServiceHeader(API.class);
                Call<PurohitSessionAuthPojo> call = apiService.USER_SESSION_AUTH_POJO_CALL(jsonObject);
                call.enqueue(new Callback<PurohitSessionAuthPojo>() {
                    @Override
                    public void onResponse(Call<PurohitSessionAuthPojo> call, Response<PurohitSessionAuthPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("NotificationCount", response.body().getNotificationCount());
                                Log.i("NotificationCount",response.body().getNotificationCount());
                                editor.apply();
                                if(response.body().getUpdateFlag().equals("0")){
                                    Intent intentUpdate = new Intent(LoginActivity.this, ProfileEditActivity.class);
                                    startActivity(intentUpdate);
                                }else {
                                    Intent intentHome = new Intent(LoginActivity.this, NavigationActivity.class);
                                    startActivity(intentHome);
                                }
                                break;
                            case "108":
                                Toast.makeText(LoginActivity.this, "Login Requried", Toast.LENGTH_SHORT).show();
                                break;
                            default:

                                myPreference.ShowDialog(LoginActivity.this,"Oops",response.body().getMessage());

                                break;
                        }
                    }
                    @Override
                    public void onFailure(Call<PurohitSessionAuthPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(LoginActivity.this,"Execption",t.getMessage());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }else{
                myPreference.ShowDialog(LoginActivity.this,"Execption","There is no internet connection");
            }

        }else{
            Log.i("LoginRequired","LoginRequired");

        }

        ButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()){
                    if(Check(EditTextMoblieNumber) && Check(EditTextPassword)){
                        progressBar = new ProgressDialog(LoginActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.show();
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                        jsonObject.addProperty("Email", EditTextMoblieNumber.getText().toString());
                        jsonObject.addProperty("Password", EditTextPassword.getText().toString());
                        jsonObject.addProperty("socialEmail", "");
                        jsonObject.addProperty("socialId", "");
                        jsonObject.addProperty("socialType", "");
                        Log.e("JSON", jsonObject.toString());
                        API apiService = createServiceHeader(API.class);
                        Call<LoginPojo> call = apiService.LOGIN_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<LoginPojo>() {
                            @Override
                            public void onResponse(Call<LoginPojo> call, final Response<LoginPojo> response) {
                                progressBar.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":
                                        if(response.body().getUpdateFlag().equals("0")){
                                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                            editor.putString("AccessToken", response.body().getAccessToken());
                                            editor.putString("DeviceId", displayFirebaseRegId());
                                            editor.putString("NotificationCount", response.body().getNotificationCount());
                                            Log.i("NotificationCount",response.body().getNotificationCount());
                                            editor.apply();

//                                            final Dialog dialog = new Dialog(LoginActivity.this);
//                                            dialog.setContentView(R.layout.custom_dialog_success);
//                                            Button dialogButton = (Button) dialog.findViewById(R.id.ButtonOk);
//                                            TextView TextviewBigTitle = (TextView)dialog.findViewById(R.id.TextviewBigTitle);
//                                            TextviewBigTitle.setText("Success");
//                                            TextView TextviewSmallTitle1 = (TextView)dialog.findViewById(R.id.TextviewSmallTitle1);
//                                            TextviewSmallTitle1.setText("Kindly update the profile");
//                                            dialogButton.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    dialog.dismiss();
//                                                    Intent intentProfileEdit = new Intent(LoginActivity.this, ProfileEditActivity.class);
//                                                    intentProfileEdit.putExtra("Operation", "NewUpdate");
//                                                    startActivity(intentProfileEdit);
//
//                                                }
//                                            });
//                                            dialog.show();

                                            final AlertDialog.Builder imageDialog = new AlertDialog.Builder(LoginActivity.this, R.style.RajCustomDialog);
                                            final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                                            final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
                                            imageDialog.setView(dialogView);
                                            TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                            TextviewSmallTitle1.setText("Kindly update the profile");
                                            TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                            TextviewBigTitle.setText("Success");
                                            Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                            ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    Intent intent = new Intent(LoginActivity.this, ProfileEditActivity.class);
                                                    intent.putExtra("Operation", "NewUpdate");
                                                    startActivity(intent);
                                                }
                                            });
                                            dialog = imageDialog.create();
                                            dialog.show();


                                        }else{
                                            if (bundle.getString("Target").equals("FlashActivity")){
                                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                editor.putString("AccessToken", response.body().getAccessToken());
                                                editor.putString("DeviceId", displayFirebaseRegId());
                                                editor.putString("NotificationCount", response.body().getNotificationCount());
                                                editor.apply();
                                                Intent i = new Intent(LoginActivity.this, FlashActivity.class);
                                                i.putExtra("Target", "FlashActivity");
                                                i.putExtra("TargetID", bundle.getString("TargetID"));
                                                startActivity(i);
                                            }else {
                                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                editor.putString("AccessToken", response.body().getAccessToken());
                                                editor.putString("DeviceId", displayFirebaseRegId());
                                                editor.putString("NotificationCount", response.body().getNotificationCount());
                                                editor.apply();
                                                Intent intentOTP = new Intent(LoginActivity.this, NavigationActivity.class);
                                                startActivity(intentOTP);
                                            }

                                        }
                                        break;

                                    case "111":
                                        final Dialog OTPdialog = new Dialog(LoginActivity.this);
                                        OTPdialog .setContentView(R.layout.custom_failor_dialog);
                                        Button dialogButton = (Button) OTPdialog .findViewById(R.id.ButtonOk);
                                        TextView TextviewBigTitle = (TextView)OTPdialog .findViewById(R.id.TextviewBigTitle);
                                        TextviewBigTitle.setText(""+response.body().getReason());
                                        TextView TextviewSmallTitle1 = (TextView)OTPdialog .findViewById(R.id.TextviewSmallTitle1);
                                        TextviewSmallTitle1.setText(""+response.body().getMessage());
                                        dialogButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                editor.putString("AccessToken", response.body().getAccessToken());
                                                editor.putString("DeviceId", displayFirebaseRegId());
                                                editor.apply();
                                                Intent intentOTP = new Intent(LoginActivity.this, OTPageActivity.class);
                                                intentOTP.putExtra("TempAccessToken",response.body().getAccessToken());
                                                startActivity(intentOTP);


                                            }
                                        });
                                        OTPdialog .show();
                                        break;

                                    case "108":
                                        Toast.makeText(LoginActivity.this, "Login Requried", Toast.LENGTH_SHORT).show();
                                        break;
                                    default:
                                        myPreference.ShowDialog(LoginActivity.this,"Oops",response.body().getMessage());
                                        break;
                                }
                            }

                            @Override
                            public void onFailure(Call<LoginPojo> call, Throwable t) {
                                progressBar.dismiss();
                                try {
                                    myPreference.ShowDialog(LoginActivity.this,"Oops",t.getMessage());
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                    }else{
                        myPreference.ShowDialog(LoginActivity.this, "Error", "Login Failed");
                    }


                }else {

                    myPreference.ShowDialog(LoginActivity.this, "Oops!!", "There is no internet connection");
                }

            }
        });


        //1.google sign in option for creating gso
        ImageViewGoogleLoginButton.setOnClickListener(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
//2.google apo client create
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();





        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email","public_profile");
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    Facebookemail = object.getString("email");
                                    FacebookName = object.getString("name");
                                    FacebookID = object.getString("id");
                                    Log.i("FacebookID",""+FacebookID);


                                    if (myPreference.isInternetOn()){
                                        progressBar = new ProgressDialog(LoginActivity.this);
                                        progressBar.setCancelable(true);
                                        progressBar.setMessage("Processing ...");
                                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                        progressBar.setProgress(0);
                                        progressBar.setMax(100);
                                        progressBar.show();
                                        JsonObject jsonObject = new JsonObject();
                                        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                                        jsonObject.addProperty("socialEmail", Facebookemail);
                                        jsonObject.addProperty("socialId", FacebookID);
                                        jsonObject.addProperty("socialType", 1);
                                        API apiService = createServiceHeader(API.class);
                                        Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                                        call.enqueue(new Callback<SoicalLoginPojo>() {
                                            @Override
                                            public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                                                progressBar.dismiss();
                                                switch (response.body().getCode()) {
                                                    case "200":
                                                        Log.i("",""+response.body().getAccessToken());
                                                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                                        editor.putString("AccessToken", response.body().getAccessToken());
                                                        editor.putString("DeviceId", displayFirebaseRegId());
                                                        editor.putString("NotificationCount", response.body().getNotificationCount());
                                                        editor.apply();
                                                        Intent intentOTP = new Intent(LoginActivity.this, NavigationActivity.class);
                                                        startActivity(intentOTP);
                                                        ImageViewFacebookButton.setVisibility(View.GONE);
                                                        ImageViewGoogleLoginButton.setVisibility(View.GONE);
                                                        break;
                                                    case "300":
                                                        Intent intentSignupPage = new Intent(LoginActivity.this,SignUpActivity.class);
                                                        intentSignupPage.putExtra("EmailID",Facebookemail);
                                                        intentSignupPage.putExtra("username",FacebookName);
                                                        intentSignupPage.putExtra("EmailIDUnique",FacebookID);
                                                        intentSignupPage.putExtra("SocialType","1");
                                                        startActivity(intentSignupPage);
                                                        ImageViewFacebookButton.setVisibility(View.GONE);
                                                        ImageViewGoogleLoginButton.setVisibility(View.GONE);
                                                        break;

                                                    case "108":
                                                        Toast.makeText(LoginActivity.this, "Login Requried", Toast.LENGTH_SHORT).show();
                                                        break;
                                                    default:
                                                        myPreference.ShowDialog(LoginActivity.this,"Oops",response.body().getMessage());
                                                        break;
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                                                progressBar.dismiss();
                                                myPreference.ShowDialog(LoginActivity.this,"Oops",t.getMessage());
                                            }
                                        });


                                    }else{
                                        myPreference.ShowDialog(LoginActivity.this,"Oops!!","There is no internet connection");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    String exc=e.toString();
                                    if(exc.contains("No value for email")){
                                        Toast.makeText(LoginActivity.this, "Missing mail address", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();


                        Log.i(LoginActivity.this.getPackageName(), "On Success");
                Log.i(LoginActivity.this.getPackageName(), loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                // App code
                Log.i(LoginActivity.this.getPackageName(), "On Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                FacebookException facebookException = exception;
                Log.i(LoginActivity.this.getPackageName(), "On Error");
                Log.i(LoginActivity.this.getPackageName(), facebookException.getMessage());
                Log.i(LoginActivity.this.getPackageName(), facebookException.getLocalizedMessage());
            }
        });


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ImageViewGoogleLoginButton:
                signin();
                break;
            case R.id.ImageViewFacebookButton:
                loginButton.performClick();
                break;
        }

    }
    private void signin() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //7.this will be get the data from google account and result set in to our user interface
    private void handlerResult(GoogleSignInResult result){
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            GmailpersonName = acct.getDisplayName();
            GmailEmail = acct.getEmail();
            GmailId  = acct.getId();
            updataUI(true);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("dataset",""+data);
//            data.getData().getPath()
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            try {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handlerResult(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    //gmailloginbuttonvisiable
    private void updataUI(boolean isLogin){
        if (isLogin==true) {
            ImageViewGoogleLoginButton.setVisibility(View.GONE);

            if (myPreference.isInternetOn()){
                progressBar = new ProgressDialog(LoginActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId",displayFirebaseRegId());
                jsonObject.addProperty("socialEmail", GmailEmail);
                jsonObject.addProperty("socialId", GmailId);
                jsonObject.addProperty("socialType", 2);
                API apiService = createServiceHeader(API.class);
                Call<SoicalLoginPojo> call = apiService.SOICAL_LOGIN_POJO_CALL(jsonObject);
                call.enqueue(new Callback<SoicalLoginPojo>() {
                    @Override
                    public void onResponse(Call<SoicalLoginPojo> call, Response<SoicalLoginPojo> response) {
                        progressBar.dismiss();
                        Log.i("Code",""+response.body().getCode());
                        switch (response.body().getCode()) {
                            case "200":
                                Log.i("accesstotken",""+response.body().getAccessToken());
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("AccessToken", response.body().getAccessToken());
                                editor.putString("DeviceId", displayFirebaseRegId());
                                editor.putString("NotificationCount", response.body().getNotificationCount());
                                editor.apply();
                                Intent intentUserProfile = new Intent(LoginActivity.this, NavigationActivity.class);
                                startActivity(intentUserProfile);
                                break;
                            case "300":
                                Intent intentSignupPage = new Intent(LoginActivity.this,SignUpActivity.class);
//                                intentSignupPage.putExtra("EmailName",GmailpersonName);
                                intentSignupPage.putExtra("EmailID",GmailEmail);
                                intentSignupPage.putExtra("EmailIDUnique",GmailId);
                                intentSignupPage.putExtra("SocialType","2");
                                startActivity(intentSignupPage);
                                break;

                            case "108":
                                Toast.makeText(LoginActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                myPreference.ShowDialog(LoginActivity.this,"Oops",response.body().getMessage());
                                break;

                        }

                    }

                    @Override
                    public void onFailure(Call<SoicalLoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        myPreference.ShowDialog(LoginActivity.this,"Oops",t.getMessage());

                    }
                });

            }else{
                myPreference.ShowDialog(LoginActivity.this,"Oops!!","There is no internet connection");
            }

        }else{
            ImageViewGoogleLoginButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        //remember password validation
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        MoblieNumber = prefs.getString("MoblieNumber","");
        Password = prefs.getString("Password","");
        Checkbox = prefs.getString("CheckBoxTrue","1");
        if(Checkbox.equals("0")){
            checkboxRememberPassword.setChecked(true);
            EditTextMoblieNumber.setText(MoblieNumber);
            EditTextPassword.setText(Password);
        }else{
            checkboxRememberPassword.setChecked(false);
            EditTextMoblieNumber.setText("");
            EditTextPassword.setText("");
        }

    }

    //validations
    boolean Check(EditText testEditText){
        return testEditText.getText().toString().length()>0 && !testEditText.getText().toString().isEmpty();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e(TAG, "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e(TAG, "Firebase Reg Id is not received yet!");
        return regId;
    }
}
