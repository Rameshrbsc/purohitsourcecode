package panditapp.codegen.com.panditapp.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.login.Login;
import com.facebook.login.LoginManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.R;

public class SkipActivity extends AppCompatActivity {
    @BindView(R.id.skip)
    TextView skip;

    String MY_PREFS_NAME = "MyPrefsFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skip);
        ButterKnife.bind(this);
        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SkipActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

        }, 4000);*/

        SharedPreferences myperf=this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
    String UserName = myperf.getString("login", "");


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(UserName.equals( "1")){
                    Intent intent = new Intent(SkipActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                }else {
                    Intent intent = new Intent(SkipActivity.this, SignUpActivity.class);
                    startActivity(intent);
                    finish();
                }




            }
        });

    }
}
