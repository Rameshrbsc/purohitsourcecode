package panditapp.codegen.com.panditapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Adapter.AreaCheckBoxAdapter;
import panditapp.codegen.com.panditapp.Adapter.AreaSearchAdapterNew;
import panditapp.codegen.com.panditapp.Adapter.ServiceCheckBoxAdapter;
import panditapp.codegen.com.panditapp.Pojo.AreaModel;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.Pojo.PoojaListPojo;
import panditapp.codegen.com.panditapp.Pojo.PoojaListResponsePojo;
import panditapp.codegen.com.panditapp.Pojo.SearchAreaListAPI;
import panditapp.codegen.com.panditapp.Pojo.SearchAreaListResponseAPI;
import panditapp.codegen.com.panditapp.Pojo.ServiceModel;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import panditapp.codegen.com.panditapp.interfac.AdapterCallback;
import panditapp.codegen.com.panditapp.interfac.ServiceCallBack;
import panditapp.codegen.com.panditapp.interfac.UnCheckCallBAck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class SeerviceMappingActivity extends AppCompatActivity implements AdapterCallback, ServiceCallBack, UnCheckCallBAck {


    @BindView(R.id.ButtonService)
    Button ButtonService;
    @BindView(R.id.area_recylerview)
    RecyclerView RecyclerViewArea;
    @BindView(R.id.area_recylerview_final)
    RecyclerView RecyclerViewAreaFinalList;
    @BindView(R.id.service_recycler_view)
    RecyclerView RecyclerViewService;
    MyPreference myPreference;
    ServiceCheckBoxAdapter serviceCheckBoxAdapter;
    AreaCheckBoxAdapter areaSearchAdapter;
    AreaSearchAdapterNew areaSearchAdapterNew;
    private String[] areaList = new String[]{"Gundy", "Teynapet", "Mylapure", "Tambaram"};
    List<AreaModel> FinalList;
    List<AreaModel> list;
    Timer timer;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.EditTextLocation)
    EditText EditTextLocation;
    @BindView(R.id.TextViewNoData)
    TextView TextViewNoData;
    List<SearchAreaListResponseAPI> searchAreaListResponseAPIS;
    AdapterCallback adapterCallback;
    ServiceCallBack serviceCallBack;
    UnCheckCallBAck unCheckCallBAck;
    private ArrayList<AreaModel> arrMyAccList = new ArrayList<AreaModel>();
    AlertDialog dialog;
    private ArrayList<ServiceModel> ServiceList = new ArrayList<ServiceModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seervice_mapping);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Search Location and Service");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        LinearLayoutManager llm1 = new LinearLayoutManager(this);
        LinearLayoutManager llm2 = new LinearLayoutManager(this);
        LinearLayoutManager llm3 = new LinearLayoutManager(this);
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        llm3.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerViewArea.setLayoutManager(llm1);
        RecyclerViewService.setLayoutManager(llm2);
        RecyclerViewAreaFinalList.setLayoutManager(llm3);

        RecyclerViewArea.setHasFixedSize(true);
        RecyclerViewService.setHasFixedSize(true);
        RecyclerViewAreaFinalList.setHasFixedSize(true);

        myPreference = new MyPreference(this);
        adapterCallback = SeerviceMappingActivity.this;
        serviceCallBack = SeerviceMappingActivity.this;
        unCheckCallBAck = SeerviceMappingActivity.this;

        FinalList = new ArrayList<>();
        list = new ArrayList<>();
        showProgressbar(false);


        ButtonService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
        EditTextLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
//                if (s.length()>0){
//                    JsonObject jsonObject = new JsonObject();
//                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
//                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
//                    jsonObject.addProperty("AreaSearchingKey", s.toString());
//                    timer = new Timer();
//                    timer.schedule(new TimerTask() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(LocationSearchingActivity.this, s.toString(), Toast.LENGTH_SHORT).show();
//                        }
//                    },600);

//
//                }
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (s.length() > 0) {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            SeerviceMappingActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    JsonObject jsonObject = new JsonObject();
                                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                    jsonObject.addProperty("AreaSearchingKey", s.toString());
                                    showProgressbar(true);
                                    API apiService = createServiceHeader(API.class);
                                    Call<SearchAreaListAPI> call = apiService.SEARCH_AREA_POJO_CALL(jsonObject);
                                    call.enqueue(new Callback<SearchAreaListAPI>() {
                                        @Override
                                        public void onResponse(Call<SearchAreaListAPI> call, Response<SearchAreaListAPI> response) {
                                            showProgressbar(false);
                                            switch (response.body().getCode()) {
                                                case "200":
                                                    searchAreaListResponseAPIS = new ArrayList<>();
                                                    Collections.addAll(searchAreaListResponseAPIS, response.body().getResponse());
                                                    if (searchAreaListResponseAPIS.size() > 0) {
                                                        TextViewNoData.setVisibility(View.GONE);
                                                        RecyclerViewArea.setVisibility(View.VISIBLE);
                                                        areaSearchAdapterNew = new AreaSearchAdapterNew(SeerviceMappingActivity.this, searchAreaListResponseAPIS, adapterCallback);
                                                        RecyclerViewArea.setAdapter(areaSearchAdapterNew);
                                                    } else {
                                                        RecyclerViewArea.setVisibility(View.GONE);
                                                        TextViewNoData.setVisibility(View.VISIBLE);
                                                    }
                                                    break;
                                                case "108":
                                                    Toast.makeText(SeerviceMappingActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(SeerviceMappingActivity.this, LoginActivity.class));
                                                    //                            getActivity().finish();
                                                    break;
                                                default:
                                                    myPreference.ShowDialog(SeerviceMappingActivity.this, "Oops", response.body().getMessage());
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<SearchAreaListAPI> call, Throwable t) {
                                            showProgressbar(false);
                                            try {
                                                myPreference.ShowDialog(SeerviceMappingActivity.this, "Exception", "Something went wrong");

                                            } catch (NullPointerException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            });

//                            try {
//                                Thread.sleep(2000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//
//                            LocationSearchingActivity.this.runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//
////                                    showResult();
//                                }
//                            });
                        }
                    }, 600);
                } else {
                    RecyclerViewArea.setVisibility(View.GONE);
                    RecyclerViewAreaFinalList.setVisibility(View.VISIBLE);
                    getFinalAreaName();
                }
            }
        });


        searchPooja();
        //getArea();
    }

    public void getArea(String areaCode, String AreaName, double lattitude, double langitude, boolean b) {

        JSONArray Jsonarr = new JSONArray();
        if (b) {
            AreaModel areaModel = new AreaModel();
            areaModel.setAreaName(AreaName);
            areaModel.setAreaCode(areaCode);
            areaModel.setLattitute(lattitude);
            areaModel.setLangitute(langitude);
            FinalList.add(areaModel);
            arrMyAccList.add(areaModel);
            Log.i("kjadad", arrMyAccList.get(0).toString());

            if (arrMyAccList.size() == 1) {
                EditTextLocation.setVisibility(View.GONE);

                RecyclerViewArea.setVisibility(View.GONE);
                RecyclerViewAreaFinalList.setVisibility(View.VISIBLE);
                getFinalAreaName();
            }

        } else {
            int size = arrMyAccList.size();
            for (int i = 0; i < size; i++) {

                String AreaCode = arrMyAccList.get(i).getAreaCode();
                if (areaCode.equals(AreaCode)) {
                    arrMyAccList.remove(i);
                    Log.i("kjadad", String.valueOf(arrMyAccList.size()));
                }
            }
            // getFinalAreaName();


        }


        /*for (int i = 0; i < FinalList.size(); i++) {
            //String aa=item[i].getPoojaId();
            List<AreaModel> list = new ArrayList<>();
            String message = FinalList.get(i).getAreaName();
            AreaModel areaModel1 = new AreaModel();
            areaModel1.setAreaName(message);
            list.add(areaModel);
            arrMyAccList.add(areaModel);

            Jsonarr.put(arrMyAccList.toString());
            String JsonString = Jsonarr.toString();
           // ServiceMapingModel mapingModel = new ServiceMapingModel();
            myPreference.setAllBookingDetails(getApplicationContext(), JsonString);


        }*/

        int size = arrMyAccList.size();
        for (int i = 0; i < size; i++) {


        }
        /*Gson gson = new Gson();
        String json = gson.toJson(bookDetails);*/
        //myPreference.setAllBookingDetails(getApplicationContext(), json);
        /*EditTextLocation.setText("");
        RecyclerViewAreaFinalList.setVisibility(View.VISIBLE);
        areaSearchAdapter = new AreaCheckBoxAdapter(SeerviceMappingActivity.this, list);
        RecyclerViewAreaFinalList.setAdapter(areaSearchAdapter);
        showProgressbar(false);*/


    }

    public void getFinalAreaName() {
        int size = arrMyAccList.size();
        for (int i = 0; i < size; i++) {

            String areaCode = arrMyAccList.get(i).getAreaCode();
            String areaName = arrMyAccList.get(i).getAreaName();
            double latt = arrMyAccList.get(i).getLangitute();
            double lang = arrMyAccList.get(i).getLattitute();

            AreaModel areaModel = new AreaModel();
            areaModel.setAreaName(areaName);
            areaModel.setAreaCode(areaCode);
            areaModel.setLattitute(latt);
            areaModel.setLangitute(lang);
            list.add(areaModel);
        }
        RecyclerViewAreaFinalList.setVisibility(View.VISIBLE);
        areaSearchAdapter = new AreaCheckBoxAdapter(SeerviceMappingActivity.this, list, unCheckCallBAck);
        RecyclerViewAreaFinalList.setAdapter(areaSearchAdapter);
        showProgressbar(false);
    }

    public void searchPooja() {

        final ArrayList<PoojaListResponsePojo> poojaListResponsePojosList = new ArrayList<>();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
        jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);

        API apiService = createServiceHeader(API.class);
        Call<PoojaListPojo> call = apiService.SEARCH_POOJA_POJO_CALL(jsonObject);

        call.enqueue(new retrofit2.Callback<PoojaListPojo>() {
            @Override
            public void onResponse(Call<PoojaListPojo> call, retrofit2.Response<PoojaListPojo> response) {

                switch (response.body().getCode()) {
                    case "200":
                        int size = response.body().getResponse().length;
                        PoojaListResponsePojo[] item = response.body().getResponse();
                        for (int i = 0; i < size; i++) {
                            //String aa=item[i].getPoojaId();
                            String code = item[i].getPoojaId();
                            String message = item[i].getPoojaName();
                            PoojaListResponsePojo poojaListResponsePojo = new PoojaListResponsePojo();
                            poojaListResponsePojo.setPoojaId(code);
                            poojaListResponsePojo.setPoojaName(message);
                            poojaListResponsePojosList.add(poojaListResponsePojo);


                        }

                        serviceCheckBoxAdapter = new ServiceCheckBoxAdapter(SeerviceMappingActivity.this, poojaListResponsePojosList, serviceCallBack);
                        RecyclerViewService.setAdapter(serviceCheckBoxAdapter);


                        break;
                    case "108":
                        Toast.makeText(SeerviceMappingActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SeerviceMappingActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    default:
                        myPreference.ShowDialog(SeerviceMappingActivity.this, "Oops", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PoojaListPojo> call, Throwable t) {

                myPreference.ShowDialog(SeerviceMappingActivity.this, "Oops", "Something went wrong");
            }
        });
    }

    private void showProgressbar(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClickCallback(String areaCode, String AreaName, double lattitude, double langitude, boolean b) {
        showProgressbar(true);
        getArea(areaCode, AreaName, langitude, langitude, b);

    }

    public void submit() {

        String service = "";
        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(SeerviceMappingActivity.this, R.style.RajCustomDialog);
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.custom_progress, null);
        imageDialog.setView(dialogView);
        final AlertDialog alertdialog = imageDialog.create();
        alertdialog.show();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
        jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
        JsonArray jsonElements = new JsonArray();

        for (int j = 0; j < ServiceList.size(); j++) {
            if (service.equals("")) {
                service = ServiceList.get(j).toString();
            } else {
                service = service + ServiceList.get(j).toString();
            }


        }
        for (int i = 0; i < ServiceList.size(); i++) {


            JsonObject AreaPoojaId = new JsonObject();

            AreaPoojaId.addProperty("Area", arrMyAccList.get(0).getAreaCode());
            AreaPoojaId.addProperty("Pooja", ServiceList.get(i).getPoojaId());
            AreaPoojaId.addProperty("Latitude", arrMyAccList.get(0).getLattitute());
            AreaPoojaId.addProperty("Longitude", arrMyAccList.get(0).getLangitute());
            jsonElements.add(AreaPoojaId);
        }
        jsonObject.add("AreaPoojaId", jsonElements);
        Log.e("JSON", jsonObject.toString());
        API apiService = createServiceHeader(API.class);
        Call<LoginPojo> call = apiService.CREATE_GROUPING_POJO_CALL(jsonObject);

        call.enqueue(new retrofit2.Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                alertdialog.dismiss();
                switch (response.body().getCode()) {
                    case "200":
                        onBackPressed();
                        finish();
                        break;
                    case "110":
                        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(SeerviceMappingActivity.this, R.style.RajCustomDialog);
                        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View dialogView = inflater.inflate(R.layout.custom_warning_dialog, null);
                        imageDialog.setView(dialogView);
                        TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                        TextviewSmallTitle1.setText(response.body().getMessage());
                        TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                        TextviewBigTitle.setText("Warning");
                        Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                        ButtonOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                onBackPressed();
                                SeerviceMappingActivity.this.finish();
                            }
                        });
                        dialog = imageDialog.create();
                        dialog.show();
                        break;
                    case "108":
                        Toast.makeText(SeerviceMappingActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SeerviceMappingActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    default:
                        myPreference.ShowDialog(SeerviceMappingActivity.this, "Oops", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                alertdialog.dismiss();
                myPreference.ShowDialog(SeerviceMappingActivity.this, "Oops", "Something went wrong");
            }
        });
    }

    @Override
    public void onClickCallback(String poojaName) {


        ServiceModel serviceModel = new ServiceModel();
        serviceModel.setPoojaId(poojaName);
        ServiceList.add(serviceModel);

    }

    @Override
    public void oncheckbox(String areaCode, boolean b) {
        if (b) {
            arrMyAccList.remove(0);

            EditTextLocation.setVisibility(View.VISIBLE);
            RecyclerViewArea.setVisibility(View.GONE);
            RecyclerViewAreaFinalList.setVisibility(View.GONE);
            list.clear();
            searchAreaListResponseAPIS.clear();
            RecyclerViewAreaFinalList.removeAllViewsInLayout();
        }

    }
}
