package panditapp.codegen.com.panditapp.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.NewChangedPasswordPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class NewChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.ButtonSubmit)
    Button ButtonSubmit;
    @BindView(R.id.EditTextUserNewPassword)
    EditText EditTextUserNewPassword;
    @BindView(R.id.EditTextConfirmNewPassword)
    EditText EditTextConfirmNewPassword;
    private ProgressDialog progressBar;
    MyPreference myPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("ChangePassword");
        ButterKnife.bind(this);
        myPreference = new MyPreference(NewChangePasswordActivity.this);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(EditTextUserNewPassword.getText().toString().equals( EditTextConfirmNewPassword.getText().toString())){

                    if (myPreference.isInternetOn()) {
                        progressBar = new ProgressDialog(NewChangePasswordActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.show();

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId",myPreference.getDefaultRunTimeValue()[1]);
                        jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                        jsonObject.addProperty("NewPassword", EditTextConfirmNewPassword.getText().toString());
                        API apiService = createServiceHeader(API.class);
                        Call<NewChangedPasswordPojo> call = apiService.NEW_CHANGED_PASSWORD_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<NewChangedPasswordPojo>() {
                            @Override
                            public void onResponse(Call<NewChangedPasswordPojo> call, Response<NewChangedPasswordPojo> response) {
                                progressBar.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":
                                        Toast.makeText(NewChangePasswordActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                        break;
                                    case "108":
                                        Intent intentemg = new Intent(NewChangePasswordActivity.this, LoginActivity.class);
                                        startActivity(intentemg);
                                        break;
                                    default:
                                        myPreference.ShowDialog(NewChangePasswordActivity.this,"Oops",response.body().getMessage());

                                        break;
                                }
                            }

                            @Override
                            public void onFailure(Call<NewChangedPasswordPojo> call, Throwable t) {
                                progressBar.dismiss();

                                myPreference.ShowDialog(NewChangePasswordActivity.this,"Oops",t.getMessage());
                            }
                        });

                    }else{
                        myPreference.ShowDialog(NewChangePasswordActivity.this,"Oops!!","There is no internet connection");
                    }

                }else{
                    Toast.makeText(NewChangePasswordActivity.this, "password doas not match", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



}
