package panditapp.codegen.com.panditapp.Pojo;

public class PurohitSchedulingCalendarPojo {

    private String message;

    private PurohitSchedulingCalendarResponsePojo[] CustomerPoojaList;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public PurohitSchedulingCalendarResponsePojo[] getCustomerPoojaList ()
    {
        return CustomerPoojaList;
    }

    public void setCustomerPoojaList (PurohitSchedulingCalendarResponsePojo[] CustomerPoojaList)
    {
        this.CustomerPoojaList = CustomerPoojaList;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", CustomerPoojaList = "+CustomerPoojaList+", code = "+code+"]";
    }
}
