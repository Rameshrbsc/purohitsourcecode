package panditapp.codegen.com.panditapp.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Adapter.CustomerAcceptAdapter;
import panditapp.codegen.com.panditapp.Adapter.CustomerRejectAdapter;
import panditapp.codegen.com.panditapp.Adapter.TopCardPagerAdapter;
import panditapp.codegen.com.panditapp.Pojo.CustomerAcceptList;
import panditapp.codegen.com.panditapp.Pojo.CustomerPendingList;
import panditapp.codegen.com.panditapp.Pojo.CustomerRejectList;
import panditapp.codegen.com.panditapp.Pojo.DashboardListPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SlidingCardClass.ShadowTransformer;
import panditapp.codegen.com.panditapp.SlidingCardClass.TopCardItem;
import panditapp.codegen.com.panditapp.SupportClass.EnhancedWrapContentViewPager;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    MyPreference myPreference;
    private ProgressDialog progressBar;

    @BindView(R.id.ViewPagerTopPager)
    EnhancedWrapContentViewPager ViewPagerTopPager;
    private TopCardPagerAdapter mTopCardAdapter;
    List<CustomerPendingList> customerPendingLists;


    private ShadowTransformer mTopCardShadowTransformer;

    @BindView(R.id.RecyclerViewAccept)
    RecyclerView RecyclerViewAccept;
    CustomerAcceptAdapter customerAcceptAdapter;
    List<CustomerAcceptList> customerAcceptLists;
    @BindView(R.id.LinearLayoutAccept)
    LinearLayout LinearLayoutAccept;
    boolean AcceptExpand = false;
    @BindView(R.id.ImageViewExpandCTRLTop)
    ImageView ImageViewExpandCTRLTop;
    @BindView(R.id.TextViewCount)
    TextView TextViewCount;
//    @BindView(R.id.TopSwipeContainer)
//    SwipeRefreshLayout TopSwipeContainer;

    @BindView(R.id.RecyclerViewReject)
    RecyclerView RecyclerViewReject;
    CustomerRejectAdapter customerRejectAdapter;
    List<CustomerRejectList> customerRejectLists;
    @BindView(R.id.LinearLayoutReject)
    LinearLayout LinearLayoutReject;
    @BindView(R.id.LinearLayoutNoData)
    LinearLayout LinearLayoutNoData;
    boolean RejectExpand = false;
    @BindView(R.id.ImageViewExpandCTRLBottom)
    ImageView ImageViewExpandCTRLBottom;
//    @BindView(R.id.BottomSwipeContainer)
//    SwipeRefreshLayout BottomSwipeContainer;

    Unbinder unbinder;



    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        getActivity().setTitle("Dashboard");
        mTopCardAdapter = new TopCardPagerAdapter();
        myPreference = new MyPreference(getActivity());

        unbinder = ButterKnife.bind(this, v);

        LinearLayoutManager llm1 = new LinearLayoutManager(getActivity());
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerViewAccept.setLayoutManager(llm1);
        RecyclerViewAccept.setHasFixedSize(true);

        LinearLayoutManager llm2 = new LinearLayoutManager(getActivity());
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerViewReject.setLayoutManager(llm2);
        RecyclerViewReject.setHasFixedSize(true);

        LinearLayoutAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AcceptExpand){
                    AcceptExpand = false;
                    ImageViewExpandCTRLTop.setImageResource(R.drawable.ic_expand_more);
                }else {
                    AcceptExpand = true;
                    ImageViewExpandCTRLTop.setImageResource(R.drawable.ic_expand_less);
                }
            }
        });

        LinearLayoutReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RejectExpand){
                    RejectExpand = false;
                    ImageViewExpandCTRLBottom.setImageResource(R.drawable.ic_expand_more);
                }else {
                    RejectExpand = true;
                    ImageViewExpandCTRLBottom.setImageResource(R.drawable.ic_expand_less);
                }
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ServerRequest();
    }

    private void ServerRequest() {
        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("SenderType", "1");

            Log.i("rameshr",jsonObject.toString());
            API apiService = createServiceHeader(API.class);
            Call<DashboardListPojo> call = apiService.PUROHIT_DASHBOARD_POJO_CALL(jsonObject);
            call.enqueue(new Callback<DashboardListPojo>() {
                @Override
                public void onResponse(Call<DashboardListPojo> call, Response<DashboardListPojo> response) {
                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":

                            if(response.body().getCustomerPendingList().length>0){
                                ViewPagerTopPager.setVisibility(View.VISIBLE);
                                customerPendingLists = new ArrayList<>();
                                mTopCardAdapter = new TopCardPagerAdapter();
                                Collections.addAll(customerPendingLists, response.body().getCustomerPendingList());
                                for (int i=0; i< customerPendingLists.size(); i++){
                                    mTopCardAdapter.TopAddCardItem(new TopCardItem(customerPendingLists.get(i).getBookingId(), customerPendingLists.get(i).getUserImage(), customerPendingLists.get(i).getUserName(),
                                            customerPendingLists.get(i).getPoojaDescription(), customerPendingLists.get(i).getPoojaName(), "Rs " + customerPendingLists.get(i).getAmount(), customerPendingLists.get(i).getAddress(),customerPendingLists.get(i).getBookingDate(),
                                            customerPendingLists.get(i).getTime()), getActivity(), new TopCardPagerAdapter.RefreshData() {
                                        @Override
                                        public void Refresh_Data() {
                                            ServerRequest();
                                        }
                                    });
                                    mTopCardShadowTransformer = new ShadowTransformer(ViewPagerTopPager, mTopCardAdapter);
                                    mTopCardShadowTransformer.enableScaling(true);
                                    ViewPagerTopPager.setAdapter(mTopCardAdapter);
                                    ViewPagerTopPager.setPageTransformer(false, mTopCardShadowTransformer);
                                    ViewPagerTopPager.setOffscreenPageLimit(3);
                                    ViewPagerTopPager.setCurrentItem(0);
                                }
                            }else {
                                ViewPagerTopPager.setVisibility(View.GONE);
                            }

                            if(response.body().getCustomerAcceptList().length>0){
                                RecyclerViewAccept.setVisibility(View.VISIBLE);
                                LinearLayoutNoData.setVisibility(View.GONE);
                                TextViewCount.setText(String.valueOf(response.body().getCustomerAcceptList().length));
                                customerAcceptLists = new ArrayList<>();
                                Collections.addAll(customerAcceptLists, response.body().getCustomerAcceptList());
                                customerAcceptAdapter = new CustomerAcceptAdapter(getActivity(), customerAcceptLists);
                                RecyclerViewAccept.setAdapter(customerAcceptAdapter);
                            }else {
                                TextViewCount.setText("0");
                                RecyclerViewAccept.setVisibility(View.GONE);
                                LinearLayoutNoData.setVisibility(View.VISIBLE);
                            }

//                            if (response.body().getCustomerRejectList().length>0){
//                                customerRejectLists = new ArrayList<>();
//                                Collections.addAll(customerRejectLists, response.body().getCustomerRejectList());
//                                customerRejectAdapter = new CustomerRejectAdapter(getActivity(), customerRejectLists);
//                                RecyclerViewReject.setAdapter(customerRejectAdapter);
//                            }
                            break;
                        case "108":
                            Toast.makeText(getActivity(), "Login Required", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getActivity(), LoginActivity.class));
//                            getActivity().finish();
                            break;
                        default:
                            myPreference.ShowDialog(getActivity(),"Oops",response.body().getMessage());
                    }
                }
                @Override
                public void onFailure(Call<DashboardListPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        myPreference.ShowDialog(getActivity(),"Exception",t.getMessage());

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            });
        }else{
            myPreference.ShowDialog(getActivity(),"Exception","There is no internet connection");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbind the view to free some memory
        unbinder.unbind();
    }
}
