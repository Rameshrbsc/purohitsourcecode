package panditapp.codegen.com.panditapp.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.CasteMode;
import panditapp.codegen.com.panditapp.Pojo.CommuntyPojoClass;
import panditapp.codegen.com.panditapp.Pojo.GotharamPojo;
import panditapp.codegen.com.panditapp.Pojo.Gothram_data;
import panditapp.codegen.com.panditapp.Pojo.Subcommunites_data;
import panditapp.codegen.com.panditapp.Pojo.communites_data;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;



public class PersonalActivity extends AppCompatActivity {




    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.EditTextLanguage)
    EditText EditTextLanguage;
    @BindView(R.id.EditTextEmailId)
    EditText EditTextEmailId;
    @BindView(R.id.EditTextPassword)
    EditText EditTextPassword;
    @BindView(R.id.EditTextNatchathiram)
    EditText EditTextNatchathiram;
    @BindView(R.id.EditTextGothram)
    EditText EditTextGothram;
    @BindView(R.id.EditTextSection)
    EditText EditTextSection;
    @BindView(R.id.EditTextAadhar1)
    EditText EditTextAadhar1;
    @BindView(R.id.EditTextAadhar2)
    EditText EditTextAadhar2;
    @BindView(R.id.EditTextAadhar3)
    EditText EditTextAadhar3;
    @BindView(R.id.EditTextPanCard)
    EditText EditTextPanCard;
    @BindView(R.id.EditTextSubCaste)
    EditText EditTextSubCaste;
    @BindView(R.id.EditTextExperience)
    EditText EditTextExperience;

    @BindView(R.id.back)
    TextView back;







    TextView textView;
    @BindView(R.id.caste_spinner)
    Spinner caste_spinner;
    @BindView(R.id.subCaste_spinner)
    Spinner subCaste_spinner;
    @BindView(R.id.img_plus)
    CircularImageView img_plus;

    @BindView(R.id.img_profile)
    CircularImageView img_profile;
    private RadioGroup radioGroup;
    RadioButton otherCommunityRadioButton;


    @BindView(R.id.spinnerGothram)
    Spinner spinnerGothram;

    @BindView(R.id.casteLinearLayout)
    LinearLayout casteLinearLayout;

    @BindView(R.id.subCastLinearLayout)
    LinearLayout subCastLinearLayout; @BindView(R.id.spinnerSubcasteLinearLayout)
    LinearLayout spinnerSubcasteLinearLayout;

    String MY_PREFS_NAME = "MyPrefsFile", encodedImagecheck;

    File camimage;
    Map<String, String> locationDatas = new HashMap<String, String>();
    Map<String, String> gothramDatas = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> listInner = new ArrayList<>();
    File dir = new File(Environment.getExternalStorageDirectory() + "/Purohit User App/.Images");

    private List<communites_data> communites_dataList;
    private List<Gothram_data> gothram_dataList;
    private List<Subcommunites_data> subcommunites_dataList;
    MyPreference myPreference;

    ArrayList<String> communitArrayList = new ArrayList<>();
    ArrayList<String> GothramArrayList = new ArrayList<>();
    ArrayList<String> subcommunitArrayList = new ArrayList<>();
    HashMap<String, String> hashMapLblMs = new HashMap<>();
    List<ArrayList<HashMap<String, String>>> listSuper = new ArrayList<>();

    ArrayList<String> finllist = new ArrayList<>();

    List<CasteMode> adapterModels = new ArrayList<>();
    Map<String, String> subcasteDatqa = new HashMap<String, String>();
    Map<String, String> subcasteDatqaa = new HashMap<String, String>();
    String strCaste = "",str_gothram="",strSubcase="";
    private static final int REQUEST_PERMISSION_SETTING = 151;
    static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    AlertDialog alertDialog;
    Bitmap thumbnail = null, bitmap;

    private Uri currentImageUri;


    private Target mTarget;
    String userChoosenTask, encodedImage, FacebookID, Facebookemail, FacebookName, GmailpersonName, GmailEmail, GmailId, FilePath = null, TempUri;
    @BindView(R.id.spinnernakchitharam)
    Spinner spinnernakchitharam;

    @BindView(R.id.vedhas)
    Spinner vedhas;

    int hinducommunity=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_personalinfo);

        myPreference = new MyPreference(this);

        radioGroup = (RadioGroup) findViewById(R.id.radioGrp);

        otherCommunityRadioButton = (RadioButton) findViewById(R.id.radioM);

        communites_dataList = new ArrayList<>();
        gothram_dataList = new ArrayList<>();
        subcommunites_dataList = new ArrayList<>();

        communitArrayList.add("Select Caste");
        subcommunitArrayList.add("Select Sub-Caste");
        GothramArrayList.add("Select Gothram");
        getCommunity();
        getGothram();
        ButterKnife.bind(this);
        textView = (TextView) findViewById(R.id.next);



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                hinducommunity=1;

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String localStrSubCaste="";
                String spinnerNachthiram = spinnernakchitharam.getSelectedItem().toString();
                String vedahs = vedhas.getSelectedItem().toString();
                String gothram = spinnerGothram.getSelectedItem().toString();
                String localStrCaste = caste_spinner.getSelectedItem().toString();
                if(localStrCaste.equals("others")){
                    localStrSubCaste=EditTextSubCaste.getText().toString();
                    strCaste=EditTextSubCaste.getText().toString();

                }else{
                    localStrSubCaste = subCaste_spinner.getSelectedItem().toString();
                }


                if (spinnerNachthiram.equals("Select Nakshatharam")) {
                    myPreference.ShowDialog(PersonalActivity.this, "field is mandatory", "Select Nakshatharam");
                }else if (gothram.equals("Select Gothram")) {
                    myPreference.ShowDialog(PersonalActivity.this, "field is mandatory", "Select Gothram");
                } else if (vedahs.equals("Select Vedas")) {
                    myPreference.ShowDialog(PersonalActivity.this, "field is mandatory", "Select Vedas");
                } else if (localStrCaste.equals("Select Caste")) {
                    myPreference.ShowDialog(PersonalActivity.this, "field is mandatory", "Select Caste");
                }else if (localStrSubCaste.equals("Select Sub-Caste")) {
                    myPreference.ShowDialog(PersonalActivity.this, "field is mandatory", "Select Sub-Caste");
                }else if (!Check(EditTextExperience)) {
                    myPreference.ShowDialog(PersonalActivity.this, "field is mandatory", "Enter your experience");
                }else  if(hinducommunity==0){
                    myPreference.ShowDialog(PersonalActivity.this, "field is mandatory", "Select Are you ready to do service across all  Hindu  community Yes or no");

                }else {


                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("PurohitNatchtathiram",spinnerNachthiram);
                    editor.putString("PurohitGothram", str_gothram);
                    editor.putString("Purohitvedha", vedahs);
                    editor.putString("PurohitCaset", strCaste);

                    /*if(subCaste_spinner.getSelectedItem().toString().equals("others")){
                        strCaste=EditTextSubCaste.getText().toString();
                    }else {

                    }*/
                    editor.putString("PurohitSubCaste", strSubcase);
                    editor.putString("PurohitExperience", EditTextExperience.getText().toString());
                    if(FilePath != null){
                        editor.putString("ImageUriPath", FilePath);
                    }else {
                        editor.putString("ImageUriPath", null);
                    }
                    editor.putString("PurohitOther", otherCommunityRadioButton.getText().toString());

                    editor.apply();

                    Intent intent = new Intent(getApplicationContext(), KYCActivity.class);
                    startActivity(intent);
                }




            }
        });

        img_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(PersonalActivity.this, permissionsRequired, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        });




        //  getCommunity();
        caste_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                subcommunitArrayList.clear();
                String name = parent.getSelectedItem().toString();
                String key = "";
                String chapterId = "";
                Iterator iterator = locationDatas.entrySet().iterator();


                if (!name.equals("Select Caste")) {


                    if (name.equals("others")) {

                        casteLinearLayout.setVisibility(View.VISIBLE);
                        spinnerSubcasteLinearLayout.setVisibility(View.GONE);

                        //subCastLinearLayout.setVisibility(View.VISIBLE);
                    } else {

                        casteLinearLayout.setVisibility(View.GONE);
                        spinnerSubcasteLinearLayout.setVisibility(View.VISIBLE);
                        //subCastLinearLayout.setVisibility(View.GONE);
                    }

                    while (iterator.hasNext()) {
                        Map.Entry entry = (Map.Entry) iterator.next();
                        String local = entry.getValue().toString();
                        if (name.equals(local)) {
                            key = entry.getKey().toString();
                            strCaste = key;
                            Log.i("locationKey", key);

                        }
                    }

                    for (int k = 0; k < adapterModels.size(); k++) {

                        String finalistName = adapterModels.get(k).getComunityId();
                        if (finalistName.equals(strCaste)) {

                            subcommunitArrayList.add(adapterModels.get(k).getSubCommunityName());
                            subcasteDatqaa.put(adapterModels.get(k).getId(), adapterModels.get(k).getSubCommunityName());

                        }
                    }
                } else {
                    subcommunitArrayList.add("Select Sub-Caste");
                }


                ArrayAdapter<String> adapter = new ArrayAdapter<String>(PersonalActivity.this, android.R.layout.simple_spinner_item, subcommunitArrayList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subCaste_spinner.setAdapter(adapter);


                if (selectedItem.equals("Add new category")) {
                    // do your stuff
                }
            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subCaste_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                //subcommunitArrayList.clear();
                String name = parent.getSelectedItem().toString();
                String key = "";
                String chapterId = "";
                Iterator iterator = subcasteDatqaa.entrySet().iterator();

                if (!name.equals("Select Sub-Caste")) {
                    while (iterator.hasNext()) {
                        Map.Entry entry = (Map.Entry) iterator.next();
                        String local = entry.getValue().toString();
                        if (name.equals(local)) {
                            key = entry.getKey().toString();
                            strCaste = key;
                            Log.i("locationKey", key);

                        }
                    }


                }


            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        spinnerGothram.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
              //  GothramArrayList.clear();

                String name = parent.getSelectedItem().toString();
                String key = "";
                String chapterId = "";
                Iterator iterator = gothramDatas.entrySet().iterator();


                if (!name.equals("Select Gothram")) {

                    while (iterator.hasNext()) {
                        Map.Entry entry = (Map.Entry) iterator.next();
                        String local = entry.getValue().toString();
                        if (name.equals(local)) {
                            key = entry.getKey().toString();
                            str_gothram = key;
                            Log.i("locationKey", key);
                        }
                    }
                }

            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    public void getCommunity() {


        API apiService = createServiceHeader(API.class);
        Call<CommuntyPojoClass> call = apiService.COMMUNTY_POJO_CLASS_CALL();
        call.enqueue(new Callback<CommuntyPojoClass>() {
            @Override
            public void onResponse(Call<CommuntyPojoClass> call, Response<CommuntyPojoClass> response) {

                Log.i("Code", "" + response.body().getCommunites_code());

                Collections.addAll(communites_dataList, response.body().getCommunites_data());
                Collections.addAll(subcommunites_dataList, response.body().getSubcommunites_data());

                for (int i = 0; i < communites_dataList.size(); i++) {
                    String id = communites_dataList.get(i).getCommunityId();
                    String locationName = communites_dataList.get(i).getCommunityName();

                    communitArrayList.add(locationName);
                    locationDatas.put(id, locationName);

                }
                for (int j = 0; j < subcommunites_dataList.size(); j++) {

                    String id = subcommunites_dataList.get(j).getSubCommunityId();
                    String locationId = subcommunites_dataList.get(j).getSubCommunityName();
                    String communityId = subcommunites_dataList.get(j).getCommunityId();

                    hashMapLblMs.put(id, communityId);
                    listInner.add(hashMapLblMs);
                    finllist.add(locationId);
                    listSuper.add(listInner);
                    CasteMode adapterModel = new CasteMode();
                    adapterModel.setId(id);
                    adapterModel.setComunityId(communityId);
                    adapterModel.setSubCommunityName(locationId);

                    adapterModels.add(adapterModel);

                    subcasteDatqa.put(id, locationId);

                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(PersonalActivity.this, android.R.layout.simple_spinner_item, communitArrayList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                caste_spinner.setAdapter(adapter);


            }

            @Override
            public void onFailure(Call<CommuntyPojoClass> call, Throwable t) {

                myPreference.ShowDialog(PersonalActivity.this, "Oops", "Something went wrong!!!");
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("dataset", "" + data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                if (currentImageUri != null) {
                    try {
                        FilePath = currentImageUri.getPath();
                        ;
                        CamLoadImage(BitmapFactory.decodeStream(getContentResolver().openInputStream(currentImageUri)), currentImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            } else if (requestCode == SELECT_FILE) {
                currentImageUri = data.getData();
                try {
                    thumbnail = MediaStore.Images.Media.getBitmap(PersonalActivity.this.getApplicationContext().getContentResolver(), data.getData());
                    Log.i("onsave", "data got from gallery ");

                    FilePath = getRealPathFromURI(PersonalActivity.this, currentImageUri);
                    camimage = new File(FilePath);
                    ImageFileFilter imageFileFilter = new ImageFileFilter(camimage);
                    if (imageFileFilter.accept(camimage)) {
                        Log.i("GalleryImageLoaded", "GalleryImageLoaded");
                        LoadImage(currentImageUri);
                    } else {
                        Toast.makeText(this, "Unsupported File", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_PERMISSION_SETTING) {
                if (checkPermissions()) {
                    selectImage();
                }
            }
        }
    }


    public static class ImageFileFilter implements FileFilter {
        File file;
        private final String[] okFileExtensions = new String[]{"jpg", "png", "jpeg"};

        public ImageFileFilter(File newfile) {
            this.file = newfile;
        }

        public boolean accept(File file) {
            for (String extension : okFileExtensions) {
                if (file.getName().toLowerCase().endsWith(extension)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static class SquareTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawPaint(paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "square";
        }
    }


    private void CamLoadImage(Bitmap thumbnail, Uri currentImageUri) {//camera
        ExifInterface ei = null;
        int orientation = 0;
        try {
            ei = new ExifInterface(FilePath);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                thumbnail = rotateImage(thumbnail, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                thumbnail = rotateImage(thumbnail, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                thumbnail = rotateImage(thumbnail, 270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                thumbnail = thumbnail;
        }
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                img_profile.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
//        Picasso.with(SignUpActivity.this)
//                .load(currentImageUri)
//                .into(mTarget);
        Picasso.with(this).load(currentImageUri).transform(new SignUpActivity.SquareTransform()).into(mTarget);

//        ImageViewProfilePic.setImageBitmap(thumbnail);
        img_profile.setVisibility(View.VISIBLE);

        alertDialog.dismiss();
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private String getRealPathFromURI(FragmentActivity activity, Uri selectedImageUri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private void LoadImage(Uri thumbnail) {//gallery
        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                img_profile.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };
//        Picasso.with(SignUpActivity.this)
//                .load(thumbnail)
//                .into(mTarget);
        Picasso.with(this).load(thumbnail).transform(new SignUpActivity.SquareTransform()).into(mTarget);

//        ImageViewProfilePic.setImageBitmap(thumbnail);

        img_profile.setVisibility(View.VISIBLE);
        Log.i("onsave", " viewwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
        ;
        alertDialog.dismiss();
    }

    private void selectImage() {
        final String[] value = getResources().getStringArray(R.array.Camera);
        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.camera_popup, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PersonalActivity.this);
        alertDialogBuilder.setView(promptsView);
        ListView lv = (ListView) promptsView.findViewById(R.id.selectmode);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, value);
        lv.setAdapter(adapter);
        lv.setSelector(R.color.colorPrimary);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
//                    boolean result= Utility.checkPermission(SignUpActivity.this);
                    userChoosenTask = "Take Photo";
                    alertDialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    dir.mkdirs();

                    if (dir.isDirectory()) {
                        Log.i("FileCreated", "FileCreated");

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        camimage = new File(dir, "PurhoitAPP" + timeStamp + ".jpg");
                        currentImageUri = Uri.fromFile(camimage);//file
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    } else {
                        Log.i("FileNotCreated", "FileNotCreated");
                    }


                } else if (position == 1) {

                    userChoosenTask = "Choose from Library";
//                    boolean result= Utility.checkPermission(SignUpActivity.this);


                    alertDialog.dismiss();
                    startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), SELECT_FILE);
                    Log.i("onsave", "going to gallery ");
                } else if (position == 2) {
                    alertDialog.dismiss();
                    userChoosenTask = "Cancel";


                }
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (checkPermissions()) {
                selectImage();
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Storage access permission is required to read and write files from your mobile. Enable it to upload photo\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

    boolean Check(EditText testEditText){
        return testEditText.getText().toString().length()>0 && !testEditText.getText().toString().isEmpty();
    }


    public void getGothram() {


        API apiService = createServiceHeader(API.class);
        Call<GotharamPojo> call = apiService.GOTHARAM_POJO_CALL();
        call.enqueue(new Callback<GotharamPojo>() {
            @Override
            public void onResponse(Call<GotharamPojo> call, Response<GotharamPojo> response) {

                Log.i("Code", "" + response.body().getGothram_code());

                Collections.addAll(gothram_dataList, response.body().getGothram_data());


                for (int i = 0; i < gothram_dataList.size(); i++) {
                    String id = gothram_dataList.get(i).getGothramId();
                    String locationName = gothram_dataList.get(i).getGothramName();

                    GothramArrayList .add(locationName);
                    gothramDatas.put(id, locationName);

                }


                ArrayAdapter<String> adapter = new ArrayAdapter<String>(PersonalActivity.this, android.R.layout.simple_spinner_item, GothramArrayList );
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerGothram.setAdapter(adapter);


            }

            @Override
            public void onFailure(Call<GotharamPojo> call, Throwable t) {

                Log.i("jdfhdf",t.toString());

                myPreference.ShowDialog(PersonalActivity.this, t.getMessage(), "Something went wrong!!!");
            }
        });


    }



}
