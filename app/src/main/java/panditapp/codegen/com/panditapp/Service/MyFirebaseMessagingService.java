package panditapp.codegen.com.panditapp.Service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import panditapp.codegen.com.panditapp.Activity.FlashActivity;
import panditapp.codegen.com.panditapp.Activity.SplashScreen;
import panditapp.codegen.com.panditapp.Receiver.NoResponseForBookingReceiver;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import panditapp.codegen.com.panditapp.Utils.NotificationUtils;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    MyPreference myPreference;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
               // handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }else {
            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
                //handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }
        }
    }

    private void handleNotification(String title, String body) {
        // app is in foreground, broadcast the push message
        Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
        pushNotification.putExtra("title", title);
        pushNotification.putExtra("body", body);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("message");

            String title = data.getString("title");
            String body = data.getString("body");
            String timestamp = data.getString("Timestamp");
            String imageUrl = data.getString("Image");
            String ExpireTime = data.getString("ExpireTime");
            String NotificationId = data.getString("NotificationId");
            String NotificationCount = data.getString("NotificationCount");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "body: " + body);
            Log.e(TAG, "Regarding: " + data.getString("Regarding"));
            Log.e(TAG, "RegardingId: " + data.getString("RegardingId"));
            Log.e(TAG, "SenderID: " + data.getString("SenderID"));
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + data.getString("Timestamp"));
            Log.e(TAG, "ExpireTime: " + ExpireTime);
            Log.e(TAG, "NotificationId: " + NotificationId);
            Log.e(TAG, "NotificationCount: " + NotificationCount);

            SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("NotificationCount", NotificationCount);
            Log.i("NotificationCount",NotificationCount);
            editor.apply();

            //Update Notification Count in bell icon
            Intent pushNotification = new Intent(Config.NOTIFICATION_ICON);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);


            switch (data.getString("Regarding")) {
                case "3":{
                    Log.e("ReceiverRequestTime",new Date( new Date().getTime()).toString());

                    //Creating Alert Sound
                    MediaPlayer mp;
                    //String filename = "android.resource://" + this.getPackageName() + "/raw/test0";
                    mp=MediaPlayer.create(getApplicationContext(), getApplicationContext().getResources().getIdentifier("bell","raw",getApplicationContext().getPackageName()));
                    mp.start();

                    //Creating Pending intent
                    Log.e("ReceiverToBe", new Date( new Date().getTime() + Long.parseLong(data.getString("ExpireTime"))).toString());
                    Intent myIntent = new Intent(getApplicationContext(), NoResponseForBookingReceiver.class);
                    myIntent.putExtra("BookingID", data.getString("RegardingId"));
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 333, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                    assert manager != null;
                    long time = System.currentTimeMillis();
                    manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, new Date( new Date().getTime() + Long.parseLong(data.getString("ExpireTime"))).getTime(), pendingIntent);

                    //Request for FlashActivity
                    Context ctx = getApplicationContext(); // or you can replace **'this'** with your **ActivityName.this**
                    Intent i = new Intent(getApplicationContext(), FlashActivity.class);
//                    i = ctx.getPackageManager().getLaunchIntentForPackage("panditapp.codegen.com.panditapp");
                    i.putExtra("Target", "FlashActivity");
                    i.putExtra("TargetID", data.getString("RegardingId"));
                    ctx.startActivity(i);

                    //Requesting for normal FCM push notification
                    Intent resultIntent = new Intent(getApplicationContext(), FlashActivity.class);
                    resultIntent.putExtra("Target", "FlashActivity");
                    resultIntent.putExtra("TargetID", data.getString("RegardingId"));
                    resultIntent.putExtra("NotificationId", data.getString("NotificationId"));
                    if (!TextUtils.isEmpty(imageUrl)) {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, body, timestamp, resultIntent, imageUrl);
                    } else {
                        showNotificationMessage(getApplicationContext(), title, body, timestamp, resultIntent);
                    }

                    break;
                }
                default:
//                    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                    notificationUtils.showNotification(R.mipmap.ic_launcher,
//                            title, body, Settings.System.DEFAULT_NOTIFICATION_URI
//                            );//Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE+ "://" + getApplicationContext().getPackageName() + "/" +R.raw.notification)

                    Intent resultIntent = new Intent(getApplicationContext(), SplashScreen.class);
                    resultIntent.putExtra("Target", "");
                    resultIntent.putExtra("TargetID", "");
                    if (!TextUtils.isEmpty(imageUrl)) {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, body, timestamp, resultIntent, imageUrl);
                    } else {
                        showNotificationMessage(getApplicationContext(), title, body, timestamp, resultIntent);
                    }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}