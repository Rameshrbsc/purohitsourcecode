package panditapp.codegen.com.panditapp.Pojo;

public class SkillListResponsePojo {
    private String SkillName;

    private String SkillId;

    public String getSkillName ()
    {
        return SkillName;
    }

    public void setSkillName (String SkillName)
    {
        this.SkillName = SkillName;
    }

    public String getSkillId ()
    {
        return SkillId;
    }

    public void setSkillId (String SkillId)
    {
        this.SkillId = SkillId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SkillName = "+SkillName+", SkillId = "+SkillId+"]";
    }
}
