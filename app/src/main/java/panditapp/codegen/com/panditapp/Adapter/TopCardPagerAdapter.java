package panditapp.codegen.com.panditapp.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SlidingCardClass.CardAdapter;
import panditapp.codegen.com.panditapp.SlidingCardClass.TopCardItem;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class TopCardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<TopCardItem> mData;
    private float mBaseElevation;
    private Context context;

    private RefreshData refreshData;

    private ProgressDialog progressBar;
    MyPreference myPreference;
    TextView TextViewdate;

    public TopCardPagerAdapter() {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void TopAddCardItem(TopCardItem item, Context context, RefreshData refreshData) {
        mViews.add(null);
        mData.add(item);
        this.context = context;
        myPreference = new MyPreference(context);
        this.refreshData = refreshData;
    }

    public interface RefreshData{
        void Refresh_Data();
    }

    @Override
    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.view_pager_top_pager_adapter, container, false);
        container.addView(view);
        try {
            bind(mData.get(position),position, view);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        CardView cardView = (CardView) view.findViewById(R.id.cardView);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    private void bind(TopCardItem item, final int position, View view) throws ParseException {
        TextView TextViewCustomerName = (TextView) view.findViewById(R.id.TextViewCustomerName);
        TextView TextViewDescription = (TextView) view.findViewById(R.id.TextViewDescription);
        TextView TextViewPoojaName = (TextView) view.findViewById(R.id.TextViewPoojaName);
        TextView TextViewAmount = (TextView) view.findViewById(R.id.TextViewAmount);
        TextView TextViewLocation = (TextView) view.findViewById(R.id.TextViewLocation);
        TextView TextViewdate = (TextView) view.findViewById(R.id.TextViewdate);


        ImageView ImageIcon = (ImageView) view.findViewById(R.id.ImageIcon);

        if (!mData.get(position).getmUserImage().isEmpty()){
            Picasso.with(context)
                    .load(mData.get(position).getmUserImage())
                    .into(ImageIcon);
        }
        final LinearLayout LinearLayoutAccept = (LinearLayout) view.findViewById(R.id.LinearLayoutAccept);
        final LinearLayout LinearLayoutReject = (LinearLayout) view.findViewById(R.id.LinearLayoutReject);

        TextViewCustomerName.setText(mData.get(position).getmCustomerName());
        TextViewDescription.setText(mData.get(position).getmDescription());
        TextViewPoojaName.setText(mData.get(position).getmPojoName());
        TextViewAmount.setText(mData.get(position).getmAmount());
        TextViewLocation.setText(mData.get(position).getmAddress());

        TextViewdate.setText(mData.get(position).getDate());
        String time=mData.get(position).getTime();

        String ConvertedDate = new SimpleDateFormat("dd-MM-yyyy",
                Locale.getDefault()).format(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(mData.get(position).getDate()));
        TextViewdate.setText(ConvertedDate +" - "+ new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new SimpleDateFormat("hh:mm:ss", Locale.getDefault()).parse(new StringTokenizer(time).nextToken())));
        Log.i("value",item.getmAmount());

        LinearLayoutAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar = new ProgressDialog(context);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("BookingId", (mData.get(position).getmBookingId()));
                jsonObject.addProperty("Status", "1");

                API apiService = createServiceHeader(API.class);
                Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
                call.enqueue(new Callback<LoginPojo>() {
                    @Override
                    public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                LinearLayoutReject.setVisibility(View.GONE);
                                refreshData.Refresh_Data();
                                Toast.makeText(context, "Accepted", Toast.LENGTH_SHORT).show();
                                break;
                            case "108":
                                Toast.makeText(context, "Login Required", Toast.LENGTH_SHORT).show();
                                context.startActivity(new Intent(context, LoginActivity.class));
                                break;
                            default:
                                myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(context,"Exception",t.getMessage());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });


            }
        });

        LinearLayoutReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                progressBar = new ProgressDialog(context);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("BookingId", (mData.get(position).getmBookingId()));
                jsonObject.addProperty("Status", "2");
                Log.e("JSON", jsonObject.toString());
                API apiService = createServiceHeader(API.class);
                Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
                call.enqueue(new Callback<LoginPojo>() {
                    @Override
                    public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                LinearLayoutAccept.setVisibility(View.GONE);
                                refreshData.Refresh_Data();
                                Toast.makeText(context, "Rejected", Toast.LENGTH_SHORT).show();
                                break;
                            case "108":
                                Toast.makeText(context, "Login Required", Toast.LENGTH_SHORT).show();
                                context.startActivity(new Intent(context, LoginActivity.class));
                                break;
                            default:
                                myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(context,"Exception",t.getMessage());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
