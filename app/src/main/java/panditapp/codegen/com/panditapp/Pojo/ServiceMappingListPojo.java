package panditapp.codegen.com.panditapp.Pojo;

public class ServiceMappingListPojo {
    private String message;

    private String code;

    private ServiceAreaPoojaId[] ServiceAreaPoojaId;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public ServiceAreaPoojaId[] getServiceAreaPoojaId ()
    {
        return ServiceAreaPoojaId;
    }

    public void setServiceAreaPoojaId (ServiceAreaPoojaId[] ServiceAreaPoojaId)
    {
        this.ServiceAreaPoojaId = ServiceAreaPoojaId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", code = "+code+", ServiceAreaPoojaId = "+ServiceAreaPoojaId+"]";
    }
}
