package panditapp.codegen.com.panditapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Adapter.NotificationAdapter;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.Pojo.NotificationListPojo;
import panditapp.codegen.com.panditapp.Pojo.NotificationListResponsePojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class NotificationActivity extends AppCompatActivity {

    @BindView(R.id.RecyclerViewNotification)
    RecyclerView RecyclerViewNotification;

    @BindView(R.id.MarkAllRead)
    Button MarkAllRead;

    @BindView(R.id.LinearLayoutNoData)
    LinearLayout LinearLayoutNoData;

    MyPreference myPreference;
    private ProgressDialog progressBar;

    NotificationAdapter notificationAdapter;
    List<NotificationListResponsePojo> notificationListResponsePojos;
    LinearLayoutManager llm1;

    boolean isLoading = false;
    private  int  PastVisable, VisiableItemCount, TotalItemCount,PreviousTotal = 0;
    int ViewTherholds = 0;
    int ResponseTotalPage ;
    int CurrentPageCount = 1, Count = 1, PAGE_SIZE = 10, TotalPageToCall = 1;

    @BindView(R.id.ProgressBarLoadMore)
    ProgressBar ProgressBarLoadMore;

    JsonArray array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Notification");
        ButterKnife.bind(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        myPreference = new MyPreference(this);

        llm1 = new LinearLayoutManager(this);
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerViewNotification.setLayoutManager(llm1);
        RecyclerViewNotification.setHasFixedSize(true);

        RecyclerViewNotification.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                VisiableItemCount = llm1.getChildCount();
                TotalItemCount = llm1.getItemCount();
                PastVisable = llm1.findFirstVisibleItemPosition();

//                if (dy>0){
//                    if(isLoading){
//                        if(TotalItemCount >= PreviousTotal){
//                            isLoading =false;
//                            PreviousTotal = TotalItemCount;
//
//                        }
//                    }else if(!isLoading && (TotalItemCount - VisiableItemCount) <= (PastVisable + ViewTherholds) && CurrentPageCount <= TotalPageToCall){
//                        CurrentPageCount++;
//                        Log.i("CurrentPageCount ", String.valueOf(CurrentPageCount));
//                        ServerRequest();
//                        isLoading = true;
//                    }
//                }

            if (!isLoading) {
                if ((VisiableItemCount + PastVisable) >= TotalItemCount
                        && PastVisable >= 0
                        && TotalItemCount >= PAGE_SIZE
                        && CurrentPageCount <= TotalPageToCall) {
                    CurrentPageCount++;
                    isLoading = true;
                    Log.e("CurrentPageCount", String.valueOf(CurrentPageCount));
                    ServerRequest();
                }
            }
            }
        });
        notificationListResponsePojos = new ArrayList<>();

        MarkAllRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("SenderType","1");
                    jsonObject.addProperty("ReadNotification","1");
                    array = new JsonArray();
                    for(int i =0 ; i<notificationListResponsePojos.size(); i++){

                        if(notificationListResponsePojos.get(i).getReadNotification().equals("0")){
                            array.add( notificationListResponsePojos.get(i).getNotificationID());

                        }

                    }
                    jsonObject.add("NotificaitonIdArray", array);
                    progressBar = new ProgressDialog(NotificationActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Processing ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();

                    API apiService = createServiceHeader(API.class);
                    Call<LoginPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<LoginPojo>() {
                        @Override
                        public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                            progressBar.dismiss();
                            switch (response.body().getCode()) {
                                case "200":

                                    int TempCount = Integer.parseInt(myPreference.getDefaultRunTimeValue()[8]);
                                    if (TempCount > 0){
                                        TempCount = TempCount - array.size();
                                    }else {
                                        TempCount = 0;
                                    }
                                    //Store Notification count in SharedPreferences
                                    SharedPreferences.Editor editor = getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("NotificationCount", String.valueOf(TempCount));
                                    Log.i("NotificationCount",String.valueOf(TempCount));
                                    editor.apply();
                                    //Update Notification Count in bell icon
                                    Intent pushNotification = new Intent(Config.NOTIFICATION_ICON);
                                    LocalBroadcastManager.getInstance(NotificationActivity.this).sendBroadcast(pushNotification);
                                    CurrentPageCount = 1;
                                    ServerRequest();
                                    break;
                                case "108":
                                    Intent intent = new Intent(NotificationActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                default:
                                    myPreference.ShowDialog(NotificationActivity.this,"Oops",response.body().getMessage());
                                    break;
                            }

                        }

                        @Override
                        public void onFailure(Call<LoginPojo> call, Throwable t) {
                            progressBar.dismiss();
                            try {
                                myPreference.ShowDialog(NotificationActivity.this,"Oops",t.getMessage());
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                }else {
                    myPreference.ShowDialog(NotificationActivity.this,"Oops","There is no internet connection");
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CurrentPageCount <= TotalPageToCall)
        ServerRequest();
    }

    private void ServerRequest() {
        if (myPreference.isInternetOn()) {
//            progressBar = new ProgressDialog(this);
//            progressBar.setCancelable(true);
//            progressBar.setMessage("Processing ...");
//            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressBar.setProgress(0);
//            progressBar.setMax(100);
//            progressBar.show();
            ProgressBarLoadMore.setVisibility(View.VISIBLE);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("SenderType", "1");
            jsonObject.addProperty("per_page", "10");
            jsonObject.addProperty("page", CurrentPageCount);


            API apiService = createServiceHeader(API.class);
            Call<NotificationListPojo> call = apiService.NOTIFICATION_LIST_POJO_CALL(jsonObject);
            call.enqueue(new Callback<NotificationListPojo>() {
                @Override
                public void onResponse(Call<NotificationListPojo> call, Response<NotificationListPojo> response) {
//                    progressBar.dismiss();
                    ProgressBarLoadMore.setVisibility(View.GONE);
                    switch (response.body().getCode()) {
                        case "200":
                            LinearLayoutNoData.setVisibility(View.GONE);
                            TotalPageToCall = Integer.parseInt(response.body().getTotalPage());
                            if (CurrentPageCount == 1){
                                if(response.body().getResponse().length>0) {
                                    notificationListResponsePojos = new ArrayList<>();
                                    Collections.addAll(notificationListResponsePojos, response.body().getResponse());
                                    for (int i = 0; i < notificationListResponsePojos.size(); i++) {
                                        if (notificationListResponsePojos.get(i).getReadNotification().equals("0")) {
                                            MarkAllRead.setVisibility(View.VISIBLE);
                                            break;
                                        } else {
                                            MarkAllRead.setVisibility(View.GONE);
                                        }
                                    }
                                    notificationAdapter = new NotificationAdapter(NotificationActivity.this, notificationListResponsePojos, new NotificationAdapter.RefreshData() {
                                        @Override
                                        public void Refresh_Data(String NotificationID) {
                                            CurrentPageCount = 1;
                                            ServerRequest();
                                        }
                                    });
                                    RecyclerViewNotification.setAdapter(notificationAdapter);
                                }else {
                                    LinearLayoutNoData.setVisibility(View.VISIBLE);
                                    RecyclerViewNotification.setVisibility(View.GONE);
                                }
                            }else {
                                if(response.body().getResponse().length>0) {
                                    List<NotificationListResponsePojo> notificationListResponsePojos = Arrays.asList(response.body().getResponse());
                                    for (int i = 0; i < notificationListResponsePojos.size(); i++) {
                                        if (notificationListResponsePojos.get(i).getReadNotification().equals("0")) {
                                            MarkAllRead.setVisibility(View.VISIBLE);
                                            break;
                                        } else {
                                            MarkAllRead.setVisibility(View.GONE);
                                        }
                                    }
                                    notificationAdapter.addMore(notificationListResponsePojos);
                                    isLoading = false;
                                }else {
                                    myPreference.ShowDialog(NotificationActivity.this,"Message", "No more data");
                                }
                            }
                            break;
                        case "108":
                            Toast.makeText(NotificationActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(NotificationActivity.this, LoginActivity.class));
                            break;
                        default:
                            myPreference.ShowDialog(NotificationActivity.this,"Oops",response.body().getMessage());
                    }
                }
                @Override
                public void onFailure(Call<NotificationListPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        myPreference.ShowDialog(NotificationActivity.this,"Exception",t.getMessage());

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            });
        }else{
            myPreference.ShowDialog(NotificationActivity.this,"Exception","There is no internet connection");
        }
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);


        }
    };



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
