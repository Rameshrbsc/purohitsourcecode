package panditapp.codegen.com.panditapp.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.Date;

import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import panditapp.codegen.com.panditapp.Utils.NotificationUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class NoResponseForBookingReceiver extends BroadcastReceiver{
    MyPreference myPreference;
    @Override
    public void onReceive(final Context context, Intent intent) {
        myPreference = new MyPreference(context);
        Log.e("ReceiverTime",new Date().toString());
        Log.e("BookingID",intent.getStringExtra("BookingID"));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(context);

            myPreference = new MyPreference(context);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("BookingId", intent.getStringExtra("BookingID"));
            jsonObject.addProperty("Status", "");

            API apiService = createServiceHeader(API.class);
            Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
            call.enqueue(new Callback<LoginPojo>() {
                @Override
                public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                    switch (response.body().getCode()) {
                        case "200":
//                            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                            Log.e("ServerResponse","Success");
                            break;
//                        case "108":
//                            Toast.makeText(context, "Login Required", Toast.LENGTH_SHORT).show();
//                            context.startActivity(new Intent(context, LoginActivity.class));
//                            break;
//                        default:
//                            myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                    }
                }
                @Override
                public void onFailure(Call<LoginPojo> call, Throwable t) {
                    //                        myPreference.ShowDialog(context,"Exception",t.getMessage());
                }
            });
    }
}
