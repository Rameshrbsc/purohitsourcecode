package panditapp.codegen.com.panditapp.Pojo;

public class CommuntyPojoClass {
    String communites_code;
    String subcommunites_code;
    private communites_data[]  communites_data;
    private Subcommunites_data[]  subcommunites_data;

    public String getSubcommunites_code() {
        return subcommunites_code;
    }

    public void setSubcommunites_code(String subcommunites_code) {
        this.subcommunites_code = subcommunites_code;
    }

    public Subcommunites_data[] getSubcommunites_data() {
        return subcommunites_data;
    }

    public void setSubcommunites_data(Subcommunites_data[] subcommunites_data) {
        this.subcommunites_data = subcommunites_data;
    }

    public String getCommunites_code() {
        return communites_code;
    }

    public void setCommunites_code(String communites_code) {
        this.communites_code = communites_code;
    }

    public communites_data[] getCommunites_data() {
        return communites_data;
    }

    public void communites_data(communites_data[] communites_data) {
        this.communites_data = communites_data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [communites_code = "+communites_code+", communites_data = "+communites_data+", subcommunites_code = "+subcommunites_code+", subcommunites_data = "+subcommunites_data+"]";
    }

}
