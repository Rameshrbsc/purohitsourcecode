package panditapp.codegen.com.panditapp.Pojo;

public class SkillListPojo {
    private String Message;

    private SkillListResponsePojo[] Response;

    private String Code;

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public SkillListResponsePojo[] getResponse ()
    {
        return Response;
    }

    public void setResponse (SkillListResponsePojo[] Response)
    {
        this.Response = Response;
    }

    public String getCode ()
    {
        return Code;
    }

    public void setCode (String Code)
    {
        this.Code = Code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Message = "+Message+", Response = "+Response+", Code = "+Code+"]";
    }


}
