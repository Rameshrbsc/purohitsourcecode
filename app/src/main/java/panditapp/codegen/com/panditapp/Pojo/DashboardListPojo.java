package panditapp.codegen.com.panditapp.Pojo;

public class DashboardListPojo {
    private String message;

    private CustomerPendingList[] CustomerPendingList;

    private CustomerRejectList[] CustomerRejectList;

    private String code;

    private CustomerAcceptList[] CustomerAcceptList;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public CustomerPendingList[] getCustomerPendingList ()
    {
        return CustomerPendingList;
    }

    public void setCustomerPendingList (CustomerPendingList[] CustomerPendingList)
    {
        this.CustomerPendingList = CustomerPendingList;
    }

    public CustomerRejectList[] getCustomerRejectList ()
    {
        return CustomerRejectList;
    }

    public void setCustomerRejectList (CustomerRejectList[] CustomerRejectList)
    {
        this.CustomerRejectList = CustomerRejectList;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public CustomerAcceptList[] getCustomerAcceptList ()
    {
        return CustomerAcceptList;
    }

    public void setCustomerAcceptList (CustomerAcceptList[] CustomerAcceptList)
    {
        this.CustomerAcceptList = CustomerAcceptList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", CustomerPendingList = "+CustomerPendingList+", CustomerRejectList = "+CustomerRejectList+", code = "+code+", CustomerAcceptList = "+CustomerAcceptList+"]";
    }
}
