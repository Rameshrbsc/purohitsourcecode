package panditapp.codegen.com.panditapp.Pojo;

public class GoogleMapLatLongToAddressPojo {
    private GoogleMapLatLongToAddressResultsPojo[] results;

    private String status;

    private String error_message;

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public GoogleMapLatLongToAddressResultsPojo[] getResults ()
    {
        return results;
    }

    public void setResults (GoogleMapLatLongToAddressResultsPojo[] results)
    {
        this.results = results;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [results = "+results+", status = "+status+"]";
    }
}
