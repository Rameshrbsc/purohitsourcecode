package panditapp.codegen.com.panditapp.Pojo;

public class LoginPojo {
    private String message;

    private String id;

    private String accessToken;

    private String name;

    private String code;

    private String reason;

    private String updateFlag;

    private String LocationAccessToken;

    private String CustomerName;

    private String CustomerAddress;

    private String CustomerLatitude;

    private String CustomerLongitude;

    private String CustomerNumber;

    private String BookingID;

    private String NotificationCount;

    public String getNotificationCount() {
        return NotificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        NotificationCount = notificationCount;
    }

    public String getBookingID() {
        return BookingID;
    }

    public void setBookingID(String bookingID) {
        BookingID = bookingID;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerAddress() {
        return CustomerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        CustomerAddress = customerAddress;
    }

    public String getCustomerLatitude() {
        return CustomerLatitude;
    }

    public void setCustomerLatitude(String customerLatitude) {
        CustomerLatitude = customerLatitude;
    }

    public String getCustomerLongitude() {
        return CustomerLongitude;
    }

    public void setCustomerLongitude(String customerLongitude) {
        CustomerLongitude = customerLongitude;
    }

    public String getCustomerNumber() {
        return CustomerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        CustomerNumber = customerNumber;
    }

    public String getLocationAccessToken() {
        return LocationAccessToken;
    }

    public void setLocationAccessToken(String locationAccessToken) {
        LocationAccessToken = locationAccessToken;
    }

    public String getReason ()
    {
        return reason;
    }

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAccessToken ()
    {
        return accessToken;
    }

    public void setAccessToken (String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getUpdateFlag ()
    {
        return updateFlag;
    }

    public void setUpdateFlag (String updateFlag)
    {
        this.updateFlag = updateFlag;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", id = "+id+", accessToken = "+accessToken+", name = "+name+", code = "+code+", updateFlag = "+updateFlag+"]";
    }

}
