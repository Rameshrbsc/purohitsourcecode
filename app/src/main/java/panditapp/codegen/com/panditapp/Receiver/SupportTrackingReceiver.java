package panditapp.codegen.com.panditapp.Receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.Calendar;
import java.util.Date;

import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Activity.PoojaDetailActivity;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.SupportClass.MyLocation;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class SupportTrackingReceiver extends BroadcastReceiver {
    MyPreference myPreference;

    Context MyContext;

    PendingIntent pendingIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        MyContext = context;

        myPreference = new MyPreference(MyContext);
        Log.e("Raj", "Inside "+new Date().toString());

        MyLocation.LocationResult locationResult = new MyLocation.LocationResult(){
            @Override
            public void gotLocation(Location location) {
                //Got the location!
                Log.i("Background Latitude", String.valueOf(location.getLatitude()));
                Log.i("Background Longitude", String.valueOf(location.getLongitude()));
                Date currentTime = Calendar.getInstance().getTime();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("LocationAccessToken", myPreference.getLocationAccessToken());
                jsonObject.addProperty("Latitude", String.valueOf(location.getLatitude()));
                jsonObject.addProperty("Longitude", String.valueOf(location.getLongitude()));

                API apiService = createServiceHeader(API.class);
                Call<LoginPojo> call = apiService.LIVE_TRACKING_UPDATE_POJO_CALL(jsonObject);
                call.enqueue(new retrofit2.Callback<LoginPojo>() {
                    @Override
                    public void onResponse(Call<LoginPojo> call, retrofit2.Response<LoginPojo> response) {
                        switch (response.body().getCode()) {
                            case "200":
//                                    Toast.makeText(MapsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                Log.d("UserLiveLocationUpdate ", response.body().getMessage());
                                break;
                            case "108":
                                Toast.makeText(MyContext, "Login Required", Toast.LENGTH_SHORT).show();
                                MyContext.startActivity(new Intent(MyContext, LoginActivity.class));
                                break;
                            default:
                                Toast.makeText(MyContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                        try {
                            Toast.makeText(MyContext, "Something went wrong, Try again later", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Intent myIntent = new Intent(MyContext, SupportTrackingReceiver.class);
                pendingIntent = PendingIntent.getBroadcast(MyContext, 100, myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager manager = (AlarmManager) MyContext.getSystemService(Context.ALARM_SERVICE);
                assert manager != null;
                long time = System.currentTimeMillis();
                manager.set(AlarmManager.RTC_WAKEUP,  time + 10000, pendingIntent);
            }};
        MyLocation myLocation = new MyLocation(context);
        myLocation.getLocation(context, locationResult);
    }
}
