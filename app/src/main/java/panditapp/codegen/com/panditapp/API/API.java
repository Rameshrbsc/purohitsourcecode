package panditapp.codegen.com.panditapp.API;

import com.google.gson.JsonObject;

import panditapp.codegen.com.panditapp.Pojo.BookingDetailListPojo;
import panditapp.codegen.com.panditapp.Pojo.CommuntyPojoClass;
import panditapp.codegen.com.panditapp.Pojo.DashboardListPojo;
import panditapp.codegen.com.panditapp.Pojo.GoogleMapDirection;
import panditapp.codegen.com.panditapp.Pojo.GoogleMapLatLongToAddressPojo;
import panditapp.codegen.com.panditapp.Pojo.GotharamPojo;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.Pojo.NewChangedPasswordPojo;
import panditapp.codegen.com.panditapp.Pojo.NotificationListPojo;
import panditapp.codegen.com.panditapp.Pojo.PoojaListPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitForgetPasswordPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitProfileUpdatePojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitProfileViewPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitResendOTPPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitSchedulingCalendarPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitSessionAuthPojo;
import panditapp.codegen.com.panditapp.Pojo.SearchAreaListAPI;
import panditapp.codegen.com.panditapp.Pojo.SendOTPPojo;
import panditapp.codegen.com.panditapp.Pojo.ServiceMappingListPojo;
import panditapp.codegen.com.panditapp.Pojo.SignUpPojo;
import panditapp.codegen.com.panditapp.Pojo.SkillListPojo;
import panditapp.codegen.com.panditapp.Pojo.SoicalLoginPojo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    //userSignup
    @POST("Purohit/register")
    Call<SignUpPojo> SIGN_UP_POJO_CALL(@Body JsonObject requestParameter);

    //    //userAuth
    @POST("Purohit/userSessionAuth")
    Call<PurohitSessionAuthPojo> USER_SESSION_AUTH_POJO_CALL(@Body JsonObject requestParameter);

    //Login
    @POST("Purohit/login")
    Call<LoginPojo> LOGIN_POJO_CALL(@Body JsonObject requestParameter);

    //SocialLogin
    @POST("Purohit/socialLogin")
    Call<SoicalLoginPojo> SOICAL_LOGIN_POJO_CALL(@Body JsonObject requestParameter);

    ////ResendOTP
    @POST("Purohit/resendOtp")
    Call<PurohitResendOTPPojo> RESEND_OTP_POJO_CALL(@Body JsonObject requestParameter);

    //
    //SendOTP
    @POST("Purohit/validateOtp")
    Call<SendOTPPojo> SEND_OTP_POJO_CALL(@Body JsonObject requestParameter);

    //
    @POST("Purohit/profileView")
    Call<PurohitProfileViewPojo> USER_PROFILE_VIEW_POJO_CALL(@Body JsonObject requestParameter);

    @POST("Purohit/profileUpdate")
    Call<PurohitProfileUpdatePojo> USER_PROFILE_UPDATE_POJO_CALL(@Body JsonObject requestParameter);

    //    ForgetPassword
    @POST("Purohit/forgotPassword")
    Call<PurohitForgetPasswordPojo> FORGET_PASSWORD_POJO_CALL(@Body JsonObject requestParameter);

    //    //newpassword
    @POST("Purohit/changePassword")
    Call<NewChangedPasswordPojo> NEW_CHANGED_PASSWORD_POJO_CALL(@Body JsonObject requestParameter);

    //Poojalist
    @POST("Purohit/getPoojaList")
    Call<PoojaListPojo> POOJA_LIST_POJO_CALL(@Body JsonObject requestParameter);

    //Skilllist
    @POST("Purohit/getSkillsList")
    Call<SkillListPojo> SKILL_LIST_POJO_CALL(@Body JsonObject requestParameter);

    //SchedulingCalendar
    @POST("Booking/getBookingsWithMonth")
    Call<PurohitSchedulingCalendarPojo> PUROHIT_SCHEDULING_CALENDAR_POJO_CALL(@Body JsonObject requestParameter);

    //    Booking/getBookingsWithStatus
    @POST("Booking/getBookingsWithStatus")
    Call<DashboardListPojo> PUROHIT_DASHBOARD_POJO_CALL(@Body JsonObject requestParameter);

    //    Booking/getBookingsWithStatus
    @POST("Booking/getBookingDetailsWithId")
    Call<BookingDetailListPojo> BOOKING_DETAIL_LIST_POJO_CALL(@Body JsonObject requestParameter);

    //LocationAccessToken Check
    @POST("Purohit/checkPurohitStarted")
    Call<LoginPojo> VALIDATE_LOCATION_ACCESSTOKEN_POJO_CALL(@Body JsonObject requestParameter);

    //Begin, Abort or End tracking
    @POST("Purohit/addTrackingDetails")
    Call<LoginPojo> TRACKING_POJO_CALL(@Body JsonObject requestParameter);

    //Live Tracking Update
    @POST("Purohit/updateTrackingDetails")
    Call<LoginPojo> LIVE_TRACKING_UPDATE_POJO_CALL(@Body JsonObject requestParameter);

    //Accept or Reject Pooja
    @POST("Booking/updateStatus")
    Call<LoginPojo> ACCEPT_REJECT_POJO_CALL(@Body JsonObject requestParameter);

    //Get Distance from Google Map API
    //https://maps.googleapis.com/maps/api/directions/json?origin=13.008906,80.217450&destination=13.008658,80.217836&key=AIzaSyChzRKOIuTzIGf2vSHqSkURNUM_Tv9vNiE
    @GET("directions/json")
    Call<GoogleMapDirection> GOOGLE_MAP_DIRECTION_CALL(@Query("origin") String origin, @Query("destination") String destination,
                                                       @Query("key") String key);

    //Notification List
    @POST("Notification/List")
    Call<NotificationListPojo> NOTIFICATION_LIST_POJO_CALL(@Body JsonObject requestParameter);

    //NOTIFICATION_READ_SUBMIT_POJO_CALL
    @POST("Notification/ReadNotification")
    Call<LoginPojo> NOTIFICAITON_READ_SUBMIT_POJO_CALL(@Body JsonObject requestParameter);

    //Service Area Search API Call
    @POST("Grouping/searchArea")
    Call<SearchAreaListAPI> SEARCH_AREA_POJO_CALL(@Body JsonObject requestParameter);

    //    https://maps.googleapis.com/maps/api/geocode/json?address=Phoenix%20Market%20City,%20No.%20142,%20Velachery%20Rd,%20Indira%20Gandhi%20Nagar,%20Velachery,%20Chennai,%20Tamil%20Nadu%20600042,%20India&key=AIzaSyD_
    @GET("geocode/json")
    Call<GoogleMapLatLongToAddressPojo> GOOGLE_MAP_LAT_LONG_TO_ADDRESS_POJO_CALL(@Query("address") String address,
                                                                                 @Query("key") String key);

    //Serice Pooja list PI
    @POST("Purohit/getPoojaList")
    Call<PoojaListPojo> SEARCH_POOJA_POJO_CALL(@Body JsonObject requestParameter);

    //Creating Service Mapping
    @POST("Grouping/addGrouping")
    Call<LoginPojo> CREATE_GROUPING_POJO_CALL(@Body JsonObject requestParameter);

    //List Service Mapping
    @POST("Grouping/groupingView")
    Call<ServiceMappingListPojo> VIEW_GROUPING_POJO_CALL(@Body JsonObject requestParameter);

    //Deleting Service Mapping
    @POST("Grouping/groupingDelete")
    Call<LoginPojo> DELETE_GROUPING_POJO_CALL(@Body JsonObject requestParameter);

    //Update Service Mapping
    @POST("Grouping/groupingUpdate")
    Call<LoginPojo> UPDATE_GROUPING_POJO_CALL(@Body JsonObject requestParameter);


    //Update Service Mapping
    @POST("Community/getCommunitySubCommunityList")
    Call<CommuntyPojoClass> COMMUNTY_POJO_CLASS_CALL();//Update Service Mapping

    @POST("Gothram/getGothramList")
    Call<GotharamPojo> GOTHARAM_POJO_CALL();


}
