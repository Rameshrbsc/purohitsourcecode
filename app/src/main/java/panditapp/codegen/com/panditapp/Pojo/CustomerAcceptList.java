package panditapp.codegen.com.panditapp.Pojo;

public class CustomerAcceptList
{
    private String UserPhoneNumber;

    private String Amount;

    private String BookingId;

    private String BookingDate;

    private String BookingStatus;

    private String UserName;

    private String CatogeryName;

    private String SubmitDate;

    private String Time;

    private String Address;

    private String Latitude;

    private String SubCatogeryName;

    private String Longitude;

    private String Language;

    private String PoojaName;

    private String UserEmailID;

    private String PoojaDescription;

    private String UserImage;

    public String getUserImage() {
        return UserImage;
    }

    public void setUserImage(String userImage) {
        UserImage = userImage;
    }

    public String getPoojaDescription() {
        return PoojaDescription;
    }

    public void setPoojaDescription(String poojaDescription) {
        PoojaDescription = poojaDescription;
    }

    public String getUserPhoneNumber ()
    {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber (String UserPhoneNumber)
    {
        this.UserPhoneNumber = UserPhoneNumber;
    }

    public String getAmount ()
    {
        return Amount;
    }

    public void setAmount (String Amount)
    {
        this.Amount = Amount;
    }

    public String getBookingId ()
    {
        return BookingId;
    }

    public void setBookingId (String BookingId)
    {
        this.BookingId = BookingId;
    }

    public String getBookingDate ()
    {
        return BookingDate;
    }

    public void setBookingDate (String BookingDate)
    {
        this.BookingDate = BookingDate;
    }

    public String getBookingStatus ()
    {
        return BookingStatus;
    }

    public void setBookingStatus (String BookingStatus)
    {
        this.BookingStatus = BookingStatus;
    }

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public String getCatogeryName ()
    {
        return CatogeryName;
    }

    public void setCatogeryName (String CatogeryName)
    {
        this.CatogeryName = CatogeryName;
    }

    public String getSubmitDate ()
    {
        return SubmitDate;
    }

    public void setSubmitDate (String SubmitDate)
    {
        this.SubmitDate = SubmitDate;
    }

    public String getTime ()
    {
        return Time;
    }

    public void setTime (String Time)
    {
        this.Time = Time;
    }

    public String getAddress ()
    {
        return Address;
    }

    public void setAddress (String Address)
    {
        this.Address = Address;
    }

    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }

    public String getSubCatogeryName ()
    {
        return SubCatogeryName;
    }

    public void setSubCatogeryName (String SubCatogeryName)
    {
        this.SubCatogeryName = SubCatogeryName;
    }

    public String getLongitude ()
    {
        return Longitude;
    }

    public void setLongitude (String Longitude)
    {
        this.Longitude = Longitude;
    }

    public String getLanguage ()
    {
        return Language;
    }

    public void setLanguage (String Language)
    {
        this.Language = Language;
    }

    public String getPoojaName ()
    {
        return PoojaName;
    }

    public void setPoojaName (String PoojaName)
    {
        this.PoojaName = PoojaName;
    }

    public String getUserEmailID ()
    {
        return UserEmailID;
    }

    public void setUserEmailID (String UserEmailID)
    {
        this.UserEmailID = UserEmailID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UserPhoneNumber = "+UserPhoneNumber+", Amount = "+Amount+", BookingId = "+BookingId+", BookingDate = "+BookingDate+", BookingStatus = "+BookingStatus+", UserName = "+UserName+", CatogeryName = "+CatogeryName+", SubmitDate = "+SubmitDate+", Time = "+Time+", Address = "+Address+", Latitude = "+Latitude+", SubCatogeryName = "+SubCatogeryName+", Longitude = "+Longitude+", Language = "+Language+", PoojaName = "+PoojaName+", UserEmailID = "+UserEmailID+"]";
    }
}