package panditapp.codegen.com.panditapp.Pojo;

public class BookingDetails {
    private String UserPhoneNumber;

    private String Amount;

    private String BookingId;

    private String BookingDate;

    private String UserName;

    private String CatogeryName;

    private String SubmitDate;

    private String Status;

    private String Time;

    private String LocationName;

    private String Rating;

    private String SubCatogeryName;

    private String Feedback;

    private String Language;

    private String PoojaName;

    private String UserEmailID;

    private String PoojaImage;

    private String Latitude;

    private String Longitude;

    private String UserImage;

    private String PoojaFinalSubmit;

    public String getPoojaFinalSubmit() {
        return PoojaFinalSubmit;
    }

    public void setPoojaFinalSubmit(String poojaFinalSubmit) {
        PoojaFinalSubmit = poojaFinalSubmit;
    }

    public String getUserImage() {
        return UserImage;
    }

    public void setUserImage(String userImage) {
        UserImage = userImage;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getUserPhoneNumber ()
    {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber (String UserPhoneNumber)
    {
        this.UserPhoneNumber = UserPhoneNumber;
    }

    public String getAmount ()
    {
        return Amount;
    }

    public void setAmount (String Amount)
    {
        this.Amount = Amount;
    }

    public String getBookingId ()
    {
        return BookingId;
    }

    public void setBookingId (String BookingId)
    {
        this.BookingId = BookingId;
    }

    public String getBookingDate ()
    {
        return BookingDate;
    }

    public void setBookingDate (String BookingDate)
    {
        this.BookingDate = BookingDate;
    }

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public String getCatogeryName ()
    {
        return CatogeryName;
    }

    public void setCatogeryName (String CatogeryName)
    {
        this.CatogeryName = CatogeryName;
    }

    public String getSubmitDate ()
    {
        return SubmitDate;
    }

    public void setSubmitDate (String SubmitDate)
    {
        this.SubmitDate = SubmitDate;
    }

    public String getStatus ()
    {
        return Status;
    }

    public void setStatus (String Status)
    {
        this.Status = Status;
    }

    public String getTime ()
    {
        return Time;
    }

    public void setTime (String Time)
    {
        this.Time = Time;
    }

    public String getLocationName ()
    {
        return LocationName;
    }

    public void setLocationName (String LocationName)
    {
        this.LocationName = LocationName;
    }

    public String getRating ()
    {
        return Rating;
    }

    public void setRating (String Rating)
    {
        this.Rating = Rating;
    }

    public String getSubCatogeryName ()
    {
        return SubCatogeryName;
    }

    public void setSubCatogeryName (String SubCatogeryName)
    {
        this.SubCatogeryName = SubCatogeryName;
    }

    public String getFeedback ()
    {
        return Feedback;
    }

    public void setFeedback (String Feedback)
    {
        this.Feedback = Feedback;
    }

    public String getLanguage ()
    {
        return Language;
    }

    public void setLanguage (String Language)
    {
        this.Language = Language;
    }

    public String getPoojaName ()
    {
        return PoojaName;
    }

    public void setPoojaName (String PoojaName)
    {
        this.PoojaName = PoojaName;
    }

    public String getUserEmailID ()
    {
        return UserEmailID;
    }

    public void setUserEmailID (String UserEmailID)
    {
        this.UserEmailID = UserEmailID;
    }

    public String getPoojaImage ()
    {
        return PoojaImage;
    }

    public void setPoojaImage (String PoojaImage)
    {
        this.PoojaImage = PoojaImage;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UserPhoneNumber = "+UserPhoneNumber+", Amount = "+Amount+", BookingId = "+BookingId+", BookingDate = "+BookingDate+", UserName = "+UserName+", CatogeryName = "+CatogeryName+", SubmitDate = "+SubmitDate+", Status = "+Status+", Time = "+Time+", LocationName = "+LocationName+", Rating = "+Rating+", SubCatogeryName = "+SubCatogeryName+", Feedback = "+Feedback+", Language = "+Language+", PoojaName = "+PoojaName+", UserEmailID = "+UserEmailID+", PoojaImage = "+PoojaImage+"]";
    }
}
