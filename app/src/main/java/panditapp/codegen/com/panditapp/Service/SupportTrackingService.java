package panditapp.codegen.com.panditapp.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Date;

public class SupportTrackingService extends Service {

//    LocationThread locationThread;
    Handler mHandler;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
//        locationThread.run();

    }

    @Override
    public void onCreate() {
        super.onCreate();
//        locationThread = new LocationThread();
        mHandler = new Handler();
        mStatusChecker.run();

        Log.e("ThreadCreate", "onCreate");
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                Log.e("ThreadCreate", "Inside "+new Date().toString()); //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, 3000);
            }
        }
    };

//    private class LocationThread extends Thread{
//        static final long DELAY = 3000;
//        @Override
//        public void run() {
//            super.run();
//            try {
//                Log.e("ThreadCreate", "Inside "+new Date().toString());
//                Thread.sleep(DELAY);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        locationThread.interrupt();
//        locationThread.stop();
        Log.e("ThreadCreate", "onDestroy");
        mHandler.removeCallbacks(mStatusChecker);
    }
}
