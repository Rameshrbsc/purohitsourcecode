package panditapp.codegen.com.panditapp.Service;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import panditapp.codegen.com.panditapp.Activity.PoojaDetailActivity;
import panditapp.codegen.com.panditapp.Activity.TrackingActivity;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyLocation;

public class FloatingOverMapIconService extends Service {
    private WindowManager windowManager;
    private FrameLayout frameLayout;
    private String str_ride_id;
    public static final String BROADCAST_ACTION = "com.sribarati.appfloatingbutton";
    private int layout_parms, CurrentScreenMiddleArea = 0;

    Context context;

    String BookingID, CustomerLatitude, CustomerLongitude, CustomerName, MyLatitude, MyLongitude,
            CustomerAddress, CustomerNumber, LocationAccessToken;

    private ProgressDialog progressBar;
    int mWidth;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createFloatingBackButton();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // to receive any data from activity
//        str_ride_id = intent.getStringExtra("RIDE_ID");

        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey("BookingID")) {
            BookingID = extras.getString("BookingID");
            CustomerLatitude = extras.getString("CustomerLatitude");
            CustomerLongitude = extras.getString("CustomerLongitude");
            CustomerName = extras.getString("CustomerName");
            CustomerAddress = extras.getString("CustomerAddress");
            MyLatitude = extras.getString("MyLatitude");
            MyLongitude = extras.getString("MyLongitude");
            CustomerNumber = extras.getString("CustomerNumber");
            LocationAccessToken = extras.getString("LocationAccessToken");
            context = this;
        }
        return START_STICKY;
    }

    private void createFloatingBackButton() {

//        CurrentJobDetail.isFloatingIconServiceAlive = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            layout_parms = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        }
        else {
            layout_parms = WindowManager.LayoutParams.TYPE_PHONE;
        }

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                layout_parms,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.START | Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;



        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        frameLayout = new FrameLayout(this);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        // Here is the place where you can inject whatever layout you want in the frame layout
        layoutInflater.inflate(R.layout.custom_start_ride_back_button_over_map, frameLayout);

        Display display = windowManager.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        CurrentScreenMiddleArea = size.x/2;
        Log.e("CurrentScreenMiddleArea", String.valueOf(CurrentScreenMiddleArea));
        final ConstraintLayout ConstraintLayoutFloat = (ConstraintLayout) frameLayout.findViewById(R.id.ConstraintLayoutFloat);
        ViewTreeObserver vto = ConstraintLayoutFloat.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ConstraintLayoutFloat.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                int width = ConstraintLayoutFloat.getMeasuredWidth();

                //To get the accurate middle of the screen we subtract the width of the android floating widget.
                mWidth = size.x - width;

            }
        });

//        ImageView backOnMap = (ImageView) frameLayout.findViewById(R.id.ImageViewBackButton);
//        backOnMap.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(BROADCAST_ACTION);
//                //Passing needed date back to TrackingActivity
//                intent.putExtra("BookingID", BookingID);
//                intent.putExtra("MyLatitude", "13.011328");
//                intent.putExtra("MyLongitude", "80.212755");
//                intent.putExtra("CustomerLatitude", CustomerLatitude);
//                intent.putExtra("CustomerLongitude", CustomerLongitude);
//                intent.putExtra("CustomerName", CustomerName);
//                intent.putExtra("CustomerNumber", CustomerNumber);
//                intent.putExtra("CustomerAddress", CustomerAddress);
//                intent.putExtra("LocationAccessToken", LocationAccessToken);
//
//                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//
//                //stopping the service
//                FloatingOverMapIconService.this.stopSelf();
//            }
//        });

        frameLayout.findViewById(R.id.ImageViewBackButton).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;

                    case MotionEvent.ACTION_UP:

                        //xDiff and yDiff contain the minor changes in position when the view is clicked.
                        float xDiff = event.getRawX() - initialTouchX;
                        float yDiff = event.getRawY() - initialTouchY;

                        Log.e("RawX", String.valueOf(event.getRawX()));
                        Log.e("RawY", String.valueOf(event.getRawY()));
                        Log.e("xDiff", String.valueOf(xDiff));
                        Log.e("yDiff", String.valueOf(yDiff));

//                        if (xDiff>0){
////                            int[] location = new int[2];
////                            frameLayout.getLocationOnScreen(location);
////                            params.x = 10; //getApplicationContext().getLocationInWindow()
//
//
//                        }else

                         if (xDiff == 0){
                            Intent intent = new Intent(BROADCAST_ACTION);
                            //Passing needed date back to TrackingActivity
                            intent.putExtra("BookingID", BookingID);
                            intent.putExtra("MyLatitude", "13.011328");
                            intent.putExtra("MyLongitude", "80.212755");
                            intent.putExtra("CustomerLatitude", CustomerLatitude);
                            intent.putExtra("CustomerLongitude", CustomerLongitude);
                            intent.putExtra("CustomerName", CustomerName);
                            intent.putExtra("CustomerNumber", CustomerNumber);
                            intent.putExtra("CustomerAddress", CustomerAddress);
                            intent.putExtra("LocationAccessToken", LocationAccessToken);

                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                            //stopping the service
                            FloatingOverMapIconService.this.stopSelf();
                        }else if (event.getRawX() < CurrentScreenMiddleArea){
                            params.x = 0;
                        }else if (event.getRawX() > CurrentScreenMiddleArea){
                            params.x = (int) mWidth;
                        }

//                        if ((Math.abs(xDiff) < 5) && (Math.abs(yDiff) == mWidth ?  true: false)){
//                        }




                        windowManager.updateViewLayout(frameLayout, params);
                        return true;

                    case MotionEvent.ACTION_MOVE:

                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        //Update the layout with new X & Y coordinate
                        windowManager.updateViewLayout(frameLayout, params);
                        return true;
                        }
                return false;
                }
            });

        windowManager.addView(frameLayout, params);
    }

    public static Point getLocationOnScreen(View view){
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return new Point(location[0], location[1]);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        windowManager.removeView(frameLayout);
    }
}
