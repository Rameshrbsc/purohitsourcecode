package panditapp.codegen.com.panditapp.Pojo;

public class BookingDetailListPojo {
    private String message;

    private BookingDetails[] BookingDetails;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public BookingDetails[] getBookingDetails ()
    {
        return BookingDetails;
    }

    public void setBookingDetails (BookingDetails[] BookingDetails)
    {
        this.BookingDetails = BookingDetails;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", BookingDetails = "+BookingDetails+", code = "+code+"]";
    }
}
