package panditapp.codegen.com.panditapp.Pojo;

public class Legs {
    private Distance distance;

    private String start_address;

    private String end_address;

    private String[] traffic_speed_entry;

    private String[] via_waypoint;


    public Distance getDistance ()
    {
        return distance;
    }

    public void setDistance (Distance distance)
    {
        this.distance = distance;
    }


    public String getStart_address ()
    {
        return start_address;
    }

    public void setStart_address (String start_address)
    {
        this.start_address = start_address;
    }

    public String getEnd_address ()
    {
        return end_address;
    }

    public void setEnd_address (String end_address)
    {
        this.end_address = end_address;
    }

    public String[] getTraffic_speed_entry ()
    {
        return traffic_speed_entry;
    }

    public void setTraffic_speed_entry (String[] traffic_speed_entry)
    {
        this.traffic_speed_entry = traffic_speed_entry;
    }

    public String[] getVia_waypoint ()
    {
        return via_waypoint;
    }

    public void setVia_waypoint (String[] via_waypoint)
    {
        this.via_waypoint = via_waypoint;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [distance = "+distance+", start_address = "+start_address+", end_address = "+end_address+", traffic_speed_entry = "+traffic_speed_entry+", via_waypoint = "+via_waypoint+"]";
    }
}
