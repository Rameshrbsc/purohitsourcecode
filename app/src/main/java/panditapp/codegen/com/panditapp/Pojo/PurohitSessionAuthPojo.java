package panditapp.codegen.com.panditapp.Pojo;

public class PurohitSessionAuthPojo {
    private String message;

    private String code;

    private String updateFlag;

    private String NotificationCount;

    public String getNotificationCount() {
        return NotificationCount;
    }

    public void setNotificationCount(String notificationCount) {
        NotificationCount = notificationCount;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getUpdateFlag ()
    {
        return updateFlag;
    }

    public void setUpdateFlag (String updateFlag)
    {
        this.updateFlag = updateFlag;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", code = "+code+", updateFlag = "+updateFlag+"]";
    }
}
