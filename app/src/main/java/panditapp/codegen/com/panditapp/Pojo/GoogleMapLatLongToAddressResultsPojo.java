package panditapp.codegen.com.panditapp.Pojo;

public class GoogleMapLatLongToAddressResultsPojo {

    private String place_id;

    private String formatted_address;

    private String[] types;

    private GoogleMapLatLongToAddressResultsGeometryPojo geometry;

    public String getPlace_id ()
    {
        return place_id;
    }

    public void setPlace_id (String place_id)
    {
        this.place_id = place_id;
    }

    public String getFormatted_address ()
    {
        return formatted_address;
    }

    public void setFormatted_address (String formatted_address)
    {
        this.formatted_address = formatted_address;
    }

    public String[] getTypes ()
    {
        return types;
    }

    public void setTypes (String[] types)
    {
        this.types = types;
    }

    public GoogleMapLatLongToAddressResultsGeometryPojo getGeometry ()
    {
        return geometry;
    }

    public void setGeometry (GoogleMapLatLongToAddressResultsGeometryPojo geometry)
    {
        this.geometry = geometry;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [place_id = "+place_id+", formatted_address = "+formatted_address+", types = "+types+", geometry = "+geometry+"]";
    }
}
