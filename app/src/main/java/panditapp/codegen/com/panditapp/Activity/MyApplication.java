package panditapp.codegen.com.panditapp.Activity;

import android.app.Application;

import panditapp.codegen.com.panditapp.Utils.TypefaceUtil;


public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(),
                "SERIF",
                "font/07558_CenturyGothic.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
    }
}
