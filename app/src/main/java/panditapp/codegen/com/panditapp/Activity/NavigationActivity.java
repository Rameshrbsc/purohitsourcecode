package panditapp.codegen.com.panditapp.Activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;

import panditapp.codegen.com.panditapp.Fragment.ChangePasswordFragment;
import panditapp.codegen.com.panditapp.Fragment.DashboardFragment;
import panditapp.codegen.com.panditapp.Fragment.LocationGroupingFragment;
import panditapp.codegen.com.panditapp.Fragment.ProfileViewFragment;
import panditapp.codegen.com.panditapp.Fragment.SchedulingCalenderFragment;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    boolean doubleBackToExitPressedOnce = false;
    GoogleApiClient mGoogleApiClient;
    MyPreference myPreference;
    String MY_PREFS_NAME = "MyPrefsFile";

    BroadcastReceiver NotificationReceiver;

    MenuItem menuItem;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purohit_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (drawer.isDrawerOpen(Gravity.END)) {
//                    drawer.closeDrawer(Gravity.END);
//                } else {
//                    drawer.openDrawer(Gravity.END);
//                }
//            }
//        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        myPreference = new MyPreference(NavigationActivity.this);

        DashboardFragment fragment = new DashboardFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame,fragment)
                .addToBackStack(null)
                .commit();
        fragmentManager.executePendingTransactions();

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext()) //Use app context to prevent leaks using activity
                //.enableAutoManage(this /* FragmentActivity */, connectionFailedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        NotificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.NOTIFICATION_ICON)){
                    Log.e("NOTIFICATION_ICON", "NOTIFICATION_ICON");
                    SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
                    String restoredText = prefs.getString("NotificationCount", "0");
                    Integer NotificationCount = Integer.parseInt(restoredText);

                    if(NotificationCount<=0){
                        menuItem.setIcon(buildCounterDrawable(0));
                    }else{
                        menuItem.setIcon(buildCounterDrawable(NotificationCount));
                    }
                }else {
                    Log.e("Fail", "Fail");
                }
            }
        };
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 1){
//            Intent i = new Intent(StudentNavigationActivity.this, LoginActivity.class);
//            startActivity(i);
//            finish();

            if (doubleBackToExitPressedOnce) {
//                moveTaskToBack(true);
//                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
//                editor.putString("AccessToken", null);
//                editor.putString("DeviceId", null);
////                LoginManager.getInstance().logOut();
////                editor.putString("FacebookLogout","fblogout");
//                if (mGoogleApiClient.isConnected()) {
//                    Auth.GoogleSignInApi.signOut(mGoogleApiClient);
//                    mGoogleApiClient.disconnect();
//                    mGoogleApiClient.connect();
//                }
//                editor.apply();
//                Intent i=new Intent(NavigationActivity.this,LoginActivity.class);
//                startActivity(i);
//                NavigationActivity.this.finish();
//                return;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    NavigationActivity.this.finishAffinity();
                    System.exit(0);
                }else {
                    NavigationActivity.this.finish();
                    System.exit(0);
                }
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to close", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }else if (count > 1) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.purohit_navigation, menu);
        menuItem = menu.findItem(R.id.action_notification);

        Integer NotificationCount = Integer.parseInt(myPreference.getDefaultRunTimeValue()[8]);

        if(NotificationCount<=0){
            menuItem.setIcon(buildCounterDrawable(0));
        }else{
            menuItem.setIcon(buildCounterDrawable(NotificationCount));
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_notification) {
            startActivity(new Intent(this, NotificationActivity.class));
            return true;
        }
        else if(id == R.id.Logout){
//            Toast.makeText(this, "Logout Successfully", Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putString("AccessToken", null);
            editor.putString("DeviceId", null);
            editor.putString("NotificationCount", null);
            LoginManager.getInstance().logOut();
            editor.putString("FacebookLogout","fblogout");
            if (mGoogleApiClient.isConnected()) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                mGoogleApiClient.disconnect();
                mGoogleApiClient.connect();
            }
            LocalBroadcastManager.getInstance(this).unregisterReceiver(NotificationReceiver);

//                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//                                Toast.makeText(UserNavigationActivity.this, ""+status, Toast.LENGTH_SHORT).show();
//                            }
//                        });
            editor.apply();
            Intent i=new Intent(NavigationActivity.this,LoginActivity.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Dashboard){
            DashboardFragment fragment = new DashboardFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame,fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();
        } else if (id == R.id.MyProfile) {

            ProfileViewFragment fragment = new ProfileViewFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame,fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();

            // Handle the camera action
        } else if (id == R.id.LocationGrouping) {
            LocationGroupingFragment fragment = new LocationGroupingFragment ();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame,fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();
//
        }else if(id == R.id.ChangePassword){
            ChangePasswordFragment fragment = new ChangePasswordFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame,fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();
        }else if(id == R.id.Logout){



            logoutPopup();




        }else if(id == R.id.ScheduleCalender){
            SchedulingCalenderFragment fragment = new SchedulingCalenderFragment ();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame,fragment)
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public  void logoutPopup(){



        final AlertDialog alertDialog = new AlertDialog.Builder(NavigationActivity.this).create();
        alertDialog.setIcon(R.mipmap.ic_launcher);

        alertDialog.setMessage("Are sure you want to logout ");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("AccessToken", null);
                        editor.putString("DeviceId", null);
//            LoginManager.getInstance().logOut();
                        editor.putString("FacebookLogout","fblogout");
                        if (mGoogleApiClient.isConnected()) {
                            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                            mGoogleApiClient.disconnect();
                            mGoogleApiClient.connect();
                        }
                        editor.apply();
                        Intent i=new Intent(NavigationActivity.this,LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
    private Drawable buildCounterDrawable(int count) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.notification_count_icon, null);
//        view.setBackgroundResource(backgroundImageId);
        TextView textView = (TextView) view.findViewById(R.id.count);

        if (count <= 0) {
            View counterTextPanel = view.findViewById(R.id.count);
            counterTextPanel.setVisibility(View.GONE);

            textView.setText("" + count);
        } else {
            textView.setText("" + count);
        }

        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(NotificationReceiver,
                new IntentFilter(Config.NOTIFICATION_ICON));
    }

    @Override
    protected void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(NotificationReceiver);
    }

    public void setActionBarTitle(String title) {
        NavigationActivity.this.setTitle(title);
    }
}
