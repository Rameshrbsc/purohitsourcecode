package panditapp.codegen.com.panditapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Adapter.AreaSearchAdapter;
import panditapp.codegen.com.panditapp.Pojo.SearchAreaListAPI;
import panditapp.codegen.com.panditapp.Pojo.SearchAreaListResponseAPI;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class LocationSearchingActivity extends AppCompatActivity {
    @BindView(R.id.EditTextLocation)
    EditText EditTextLocation;
    @BindView(R.id.RecyclerViewArea)
    RecyclerView RecyclerViewArea;
//    String[] PoojaName = {"Pooja1","Pooja2","Pooja3","Pooja4","Pooja5","Pooja6"};

    @BindView(R.id.TextViewNoData)
    TextView TextViewNoData;

    MyPreference myPreference;

    AreaSearchAdapter areaSearchAdapter;
    List<SearchAreaListResponseAPI> searchAreaListResponseAPIS;

    Timer timer;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_searching);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // bind the view using butterknife
        ButterKnife.bind(this);

        setTitle("Searching location");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LinearLayoutManager llm1 = new LinearLayoutManager(this);
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerViewArea.setLayoutManager(llm1);
        RecyclerViewArea.setHasFixedSize(true);

        myPreference = new MyPreference(this);

        showProgressbar(false);
        EditTextLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
//                if (s.length()>0){
//                    JsonObject jsonObject = new JsonObject();
//                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
//                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
//                    jsonObject.addProperty("AreaSearchingKey", s.toString());
//                    timer = new Timer();
//                    timer.schedule(new TimerTask() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(LocationSearchingActivity.this, s.toString(), Toast.LENGTH_SHORT).show();
//                        }
//                    },600);

//
//                }
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (s.length()>0){
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            LocationSearchingActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    JsonObject jsonObject = new JsonObject();
                                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                    jsonObject.addProperty("AreaSearchingKey", s.toString());
                                    showProgressbar(true);
                                    API apiService = createServiceHeader(API.class);
                                    Call<SearchAreaListAPI> call = apiService.SEARCH_AREA_POJO_CALL(jsonObject);
                                    call.enqueue(new Callback<SearchAreaListAPI>() {
                                        @Override
                                        public void onResponse(Call<SearchAreaListAPI> call, Response<SearchAreaListAPI> response) {
                                            showProgressbar(false);
                                            switch (response.body().getCode()) {
                                                case "200":
                                                    searchAreaListResponseAPIS = new ArrayList<>();
                                                    Collections.addAll(searchAreaListResponseAPIS, response.body().getResponse());
                                                    if (searchAreaListResponseAPIS.size()>0){
                                                        TextViewNoData.setVisibility(View.GONE);
                                                        RecyclerViewArea.setVisibility(View.VISIBLE);
                                                        areaSearchAdapter = new AreaSearchAdapter(LocationSearchingActivity.this,searchAreaListResponseAPIS);
                                                        RecyclerViewArea.setAdapter(areaSearchAdapter);
                                                    }else {
                                                        RecyclerViewArea.setVisibility(View.GONE);
                                                        TextViewNoData.setVisibility(View.VISIBLE);
                                                    }
                                                    break;
                                                case "108":
                                                    Toast.makeText(LocationSearchingActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(LocationSearchingActivity.this, LoginActivity.class));
                //                            getActivity().finish();
                                                    break;
                                                default:
                                                    myPreference.ShowDialog(LocationSearchingActivity.this,"Oops",response.body().getMessage());
                                            }
                                        }
                                        @Override
                                        public void onFailure(Call<SearchAreaListAPI> call, Throwable t) {
                                            showProgressbar(false);
                                            try {
                                                myPreference.ShowDialog(LocationSearchingActivity.this,"Exception","Something went wrong");

                                            } catch (NullPointerException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            });

//                            try {
//                                Thread.sleep(2000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//
//                            LocationSearchingActivity.this.runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//
////                                    showResult();
//                                }
//                            });
                        }
                    },600);
                }
            }
        });



    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return false;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showProgressbar(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}
