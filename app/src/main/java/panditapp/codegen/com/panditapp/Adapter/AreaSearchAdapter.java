package panditapp.codegen.com.panditapp.Adapter;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LocationCreateGroupingActivity;
import panditapp.codegen.com.panditapp.Activity.LocationSearchingActivity;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Pojo.GoogleMapLatLongToAddressPojo;
import panditapp.codegen.com.panditapp.Pojo.SearchAreaListResponseAPI;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Service;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;

import static android.app.Activity.RESULT_OK;

public class AreaSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private LocationSearchingActivity context;
    private List<SearchAreaListResponseAPI> searchAreaListResponseAPIS;
    private MyPreference myPreference;
    private LatLng p1 = new LatLng(0.0, 0.0);
    private ProgressDialog progressBar;
    public AreaSearchAdapter(LocationSearchingActivity locationSearchingActivity, List<SearchAreaListResponseAPI> searchAreaListResponseAPIS) {
        this.context = locationSearchingActivity;
        this.searchAreaListResponseAPIS = searchAreaListResponseAPIS;
        this.myPreference = new MyPreference(locationSearchingActivity);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView TextViewLocation;
        LinearLayout LinearLayoutParent;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewLocation = (TextView) itemView.findViewById(R.id.TextViewLocation);
            LinearLayoutParent = (LinearLayout) itemView.findViewById(R.id.LinearLayoutParent);
        }
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_card_location, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.TextViewLocation.setText(searchAreaListResponseAPIS.get(position).getAreaName());
        viewHolder.LinearLayoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar = new ProgressDialog(context);
                progressBar.setCancelable(true);
                progressBar.setMessage("Fetching your area details ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                getLocationFromAddress(searchAreaListResponseAPIS.get(position).getAreaName(), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchAreaListResponseAPIS.size();
    }

    private void getLocationFromAddress(String strAddress, final int position){
        if (myPreference.isInternetOn()){
            API apiService = Service.createService(API.class);
            Call<GoogleMapLatLongToAddressPojo> call = apiService.GOOGLE_MAP_LAT_LONG_TO_ADDRESS_POJO_CALL(strAddress, "AIzaSyC62mjD0ADscIJA4kjY3uegTxw9Qtp1Aes");

            call.enqueue(new retrofit2.Callback<GoogleMapLatLongToAddressPojo>() {
                @Override
                public void onResponse(Call<GoogleMapLatLongToAddressPojo> call, retrofit2.Response<GoogleMapLatLongToAddressPojo> response) {
                    progressBar.dismiss();
                    int response_code = response.code();
                    if (response_code == 200) {
                        String status = response.body().getStatus();
                        if (status.equals("OK")) {
                            Double DynamicLat, DynamicLong;
                            LatLng lng = new LatLng(Double.valueOf(response.body().getResults()[0].getGeometry().getLocation().getLat()), Double.valueOf(response.body().getResults()[0].getGeometry().getLocation().getLng()));
                            DynamicLat = Double.valueOf(response.body().getResults()[0].getGeometry().getLocation().getLat());
                            Log.d("DynamicLat", String.valueOf(DynamicLat));
                            DynamicLong = Double.valueOf(response.body().getResults()[0].getGeometry().getLocation().getLng());
                            Log.d("DynamicLong", String.valueOf(DynamicLong));
                            p1 = lng;
                            Intent intent = new Intent();
                            intent.putExtra("AreaName", searchAreaListResponseAPIS.get(position).getAreaName());
                            intent.putExtra("AreaID", searchAreaListResponseAPIS.get(position).getAreaId());
//                          ((Activity) context).setResult(LocationCreateGroupingActivity.RequestingAreaCode, intent);
                            intent.putExtra("Latitude", p1.latitude);
                            intent.putExtra("Longitude", p1.longitude);
                            context.setResult(RESULT_OK, intent);
                            context.finish();
                        }
                        else {
                            Log.d("Error", response.body().getError_message());
                        }
                    } else {
                        myPreference.ShowDialog(context, "Oops", "Server Problem, Please try again later");
                    }
                }


                @Override
                public void onFailure(Call<GoogleMapLatLongToAddressPojo> call, Throwable t) {
                    myPreference.ShowDialog(context, "Oops", "Server Problem, Please try again later");
                }
            });
        }else {
            myPreference.ShowDialog(context, "Oops", "There is no internet connection !!!");
        }
    }
}
