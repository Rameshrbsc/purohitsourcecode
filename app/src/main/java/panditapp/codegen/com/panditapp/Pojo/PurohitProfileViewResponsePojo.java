package panditapp.codegen.com.panditapp.Pojo;

public class PurohitProfileViewResponsePojo {

    private String PurohitNatcharam;

    private String PurohitPooja;

    private String PurohitName;

    private String PurohitAbout;

    private String PurohitLocation;

    private String PurohitPanNumber;

    private String PurohitSkills;

    private String PurohitGothram;

    private String PurohitAadharNumber;

    private String PurohitPhoneNumber;

    private String PurohitExperience;

    private String PurohitSection;

    private String PurohitProfilePic;

    private String PurohitLanguage;

    private String PurohitEmailID;

    private String PurohitProfileThumbPic;

    public String getPurohitNatcharam ()
    {
        return PurohitNatcharam;
    }

    public void setPurohitNatcharam (String PurohitNatcharam)
    {
        this.PurohitNatcharam = PurohitNatcharam;
    }

    public String getPurohitPooja ()
    {
        return PurohitPooja;
    }

    public void setPurohitPooja (String PurohitPooja)
    {
        this.PurohitPooja = PurohitPooja;
    }

    public String getPurohitName ()
    {
        return PurohitName;
    }

    public void setPurohitName (String PurohitName)
    {
        this.PurohitName = PurohitName;
    }

    public String getPurohitAbout ()
    {
        return PurohitAbout;
    }

    public void setPurohitAbout (String PurohitAbout)
    {
        this.PurohitAbout = PurohitAbout;
    }

    public String getPurohitLocation ()
    {
        return PurohitLocation;
    }

    public void setPurohitLocation (String PurohitLocation)
    {
        this.PurohitLocation = PurohitLocation;
    }

    public String getPurohitPanNumber ()
    {
        return PurohitPanNumber;
    }

    public void setPurohitPanNumber (String PurohitPanNumber)
    {
        this.PurohitPanNumber = PurohitPanNumber;
    }

    public String getPurohitSkills ()
    {
        return PurohitSkills;
    }

    public void setPurohitSkills (String PurohitSkills)
    {
        this.PurohitSkills = PurohitSkills;
    }

    public String getPurohitGothram ()
    {
        return PurohitGothram;
    }

    public void setPurohitGothram (String PurohitGothram)
    {
        this.PurohitGothram = PurohitGothram;
    }

    public String getPurohitAadharNumber ()
    {
        return PurohitAadharNumber;
    }

    public void setPurohitAadharNumber (String PurohitAadharNumber)
    {
        this.PurohitAadharNumber = PurohitAadharNumber;
    }

    public String getPurohitPhoneNumber ()
    {
        return PurohitPhoneNumber;
    }

    public void setPurohitPhoneNumber (String PurohitPhoneNumber)
    {
        this.PurohitPhoneNumber = PurohitPhoneNumber;
    }

    public String getPurohitExperience ()
    {
        return PurohitExperience;
    }

    public void setPurohitExperience (String PurohitExperience)
    {
        this.PurohitExperience = PurohitExperience;
    }

    public String getPurohitSection ()
    {
        return PurohitSection;
    }

    public void setPurohitSection (String PurohitSection)
    {
        this.PurohitSection = PurohitSection;
    }

    public String getPurohitProfilePic ()
    {
        return PurohitProfilePic;
    }

    public void setPurohitProfilePic (String PurohitProfilePic)
    {
        this.PurohitProfilePic = PurohitProfilePic;
    }

    public String getPurohitLanguage ()
    {
        return PurohitLanguage;
    }

    public void setPurohitLanguage (String PurohitLanguage)
    {
        this.PurohitLanguage = PurohitLanguage;
    }

    public String getPurohitEmailID ()
    {
        return PurohitEmailID;
    }

    public void setPurohitEmailID (String PurohitEmailID)
    {
        this.PurohitEmailID = PurohitEmailID;
    }

    public String getPurohitProfileThumbPic ()
    {
        return PurohitProfileThumbPic;
    }

    public void setPurohitProfileThumbPic (String PurohitProfileThumbPic)
    {
        this.PurohitProfileThumbPic = PurohitProfileThumbPic;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PurohitNatcharam = "+PurohitNatcharam+", PurohitPooja = "+PurohitPooja+", PurohitName = "+PurohitName+", PurohitAbout = "+PurohitAbout+", PurohitLocation = "+PurohitLocation+", PurohitPanNumber = "+PurohitPanNumber+", PurohitSkills = "+PurohitSkills+", PurohitGothram = "+PurohitGothram+", PurohitAadharNumber = "+PurohitAadharNumber+", PurohitPhoneNumber = "+PurohitPhoneNumber+", PurohitExperience = "+PurohitExperience+", PurohitSection = "+PurohitSection+", PurohitProfilePic = "+PurohitProfilePic+", PurohitLanguage = "+PurohitLanguage+", PurohitEmailID = "+PurohitEmailID+", PurohitProfileThumbPic = "+PurohitProfileThumbPic+"]";
    }
}
