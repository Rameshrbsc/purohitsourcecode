package panditapp.codegen.com.panditapp.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.FlashActivity;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Activity.NotificationActivity;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.Pojo.NotificationListResponsePojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private List<NotificationListResponsePojo> notificationListResponsePojos;
    private MyPreference myPreference;
    private RefreshData refreshData;
    public NotificationAdapter(NotificationActivity notificationActivity, List<NotificationListResponsePojo> notificationListResponsePojos, RefreshData refreshData) {
        context = notificationActivity;
        this.notificationListResponsePojos = notificationListResponsePojos;
        myPreference = new MyPreference(context);
        this.refreshData = refreshData;
    }


    public interface RefreshData{
        void Refresh_Data(String NotificationID);
    }



    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView TextViewTitle, TextViewDate, TextViewBody;
        CardView My_card_view;
        private ViewHolder(View itemView) {
            super(itemView);
            TextViewTitle = (TextView) itemView.findViewById(R.id.TextViewTitle);
            TextViewDate = (TextView) itemView.findViewById(R.id.TextViewDate);
            TextViewBody = (TextView) itemView.findViewById(R.id.TextViewBody);

            My_card_view = (CardView) itemView.findViewById(R.id.My_card_view);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_notification_list_card, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        if (notificationListResponsePojos.get(position).getReadNotification().equals("0")){
            viewHolder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
            viewHolder.TextViewTitle.setTextColor(Color.BLACK);
            viewHolder.TextViewTitle.setTypeface(null, Typeface.BOLD);
            viewHolder.TextViewBody.setText(notificationListResponsePojos.get(position).getBody());
            viewHolder.TextViewBody.setTextColor(Color.BLACK);
            viewHolder.TextViewBody.setTypeface(null, Typeface.BOLD);
            viewHolder.TextViewDate.setText(notificationListResponsePojos.get(position).getTimestamp());
        }else {
            viewHolder.TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
            viewHolder.TextViewBody.setText(notificationListResponsePojos.get(position).getBody());
            viewHolder.TextViewDate.setText(notificationListResponsePojos.get(position).getTimestamp());
        }

        viewHolder.My_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                View view1 = layoutInflater.inflate(R.layout.notification_view_popup_dialog, null);
                TextView TextViewTitle = view1.findViewById(R.id.TextViewTitle);
                TextViewTitle.setText(notificationListResponsePojos.get(position).getTitle());
                TextView TextViewDate = view1.findViewById(R.id.TextViewDate);
                TextViewDate.setText(notificationListResponsePojos.get(position).getTimestamp());
                TextView TextViewBody = view1.findViewById(R.id.TextViewBody);
                TextViewBody.setText(notificationListResponsePojos.get(position).getBody());

                //Notification Read Update
                if (myPreference.isInternetOn()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                    jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                    jsonObject.addProperty("SenderType","1");
                    jsonObject.addProperty("ReadNotification","1");
                    JsonArray array = new JsonArray();
                    array.add(notificationListResponsePojos.get(position).getNotificationID());
                    jsonObject.add("NotificaitonIdArray", array);
                    API apiService = createServiceHeader(API.class);
                    Call<LoginPojo> call = apiService.NOTIFICAITON_READ_SUBMIT_POJO_CALL(jsonObject);
                    call.enqueue(new Callback<LoginPojo>() {
                        @Override
                        public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {

                            switch (response.body().getCode()) {
                                case "200":
                                    int TempCount = Integer.parseInt(myPreference.getDefaultRunTimeValue()[8]);
                                    if (TempCount>0){
                                        TempCount = --TempCount;
                                    }else {
                                        TempCount = 0;
                                    }
                                    //Store Notification count in SharedPreferences
                                    SharedPreferences.Editor editor = context.getSharedPreferences(MyPreference.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                    editor.putString("NotificationCount", String.valueOf(TempCount));
                                    Log.i("NotificationCount",String.valueOf(TempCount));
                                    editor.apply();
                                    //Update Notification Count in bell icon
                                    Intent pushNotification = new Intent(Config.NOTIFICATION_ICON);
                                    LocalBroadcastManager.getInstance(context).sendBroadcast(pushNotification);
                                    refreshData.Refresh_Data("1");
                                    break;
                                case "108":
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    context.startActivity(intent);
                                default:
                                    myPreference.ShowDialog(context,"Oops",response.body().getMessage());
                                    break;
                            }

                        }

                        @Override
                        public void onFailure(Call<LoginPojo> call, Throwable t) {
                            try {
                                myPreference.ShowDialog(context,"Oops",t.getMessage());
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                }else {
                    myPreference.ShowDialog(context,"Oops","There is no internet connection");
                }

                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(view1);
                builder.setCancelable(false).
                        setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                AlertDialog alertDialog1 = builder.create();
                alertDialog1.show();
            }
        });

        holder.setIsRecyclable(false);//stop the draw in recyclerview must put
    }

    @Override
    public int getItemCount() {
        return notificationListResponsePojos.size();
    }

    public void addMore(List<NotificationListResponsePojo> notificationListResponsePojos){
        this.notificationListResponsePojos.addAll(notificationListResponsePojos);
        notifyDataSetChanged();

    }
}
