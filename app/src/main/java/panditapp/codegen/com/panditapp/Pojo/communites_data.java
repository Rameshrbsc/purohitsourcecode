package panditapp.codegen.com.panditapp.Pojo;

public class communites_data {
    String CommunityId;
    String CommunityName;

    public String getCommunityId() {
        return CommunityId;
    }

    public void setCommunityId(String communityId) {
        CommunityId = communityId;
    }

    public String getCommunityName() {
        return CommunityName;
    }

    public void setCommunityName(String communityName) {
        CommunityName = communityName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CommunityId = "+CommunityId+", CommunityName = "+CommunityName+"]";
    }
}
