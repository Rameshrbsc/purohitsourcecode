package panditapp.codegen.com.panditapp.Pojo;

public class PoojaListResponsePojo {
    private String PoojaId;

    private String PoojaName;

    public String getPoojaId ()
    {
        return PoojaId;
    }

    public void setPoojaId (String PoojaId)
    {
        this.PoojaId = PoojaId;
    }

    public String getPoojaName ()
    {
        return PoojaName;
    }

    public void setPoojaName (String PoojaName)
    {
        this.PoojaName = PoojaName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PoojaId = "+PoojaId+", PoojaName = "+PoojaName+"]";
    }
}
