package panditapp.codegen.com.panditapp.Pojo;

public class Gothram_data {
    String GothramId;
    String GothramName;

    public String getGothramId() {
        return GothramId;
    }

    public void setGothramId(String gothramId) {
        GothramId = gothramId;
    }

    public String getGothramName() {
        return GothramName;
    }

    public void setGothramName(String gothramName) {
        GothramName = gothramName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [GothramId = "+GothramId+", GothramName = "+GothramName+"]";
    }
}
