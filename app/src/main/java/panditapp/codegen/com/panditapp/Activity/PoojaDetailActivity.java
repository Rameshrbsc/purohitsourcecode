package panditapp.codegen.com.panditapp.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.PermissionRequest;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.BuildConfig;
import panditapp.codegen.com.panditapp.Pojo.BookingDetailListPojo;
import panditapp.codegen.com.panditapp.Pojo.LoginPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyLocation;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class PoojaDetailActivity extends AppCompatActivity {
    MyPreference myPreference;
    private ProgressDialog progressBar;

    //Bind TextView
    @BindView(R.id.TextViewPoojaName)
    TextView TextViewPoojaName;
    @BindView(R.id.TextViewUserName)
    TextView TextViewUserName;
    @BindView(R.id.TextViewUserEmailID)
    TextView TextViewUserEmailID;
    @BindView(R.id.TextViewMoblieNumber)
    TextView TextViewMoblieNumber;
    @BindView(R.id.TextViewLanguage)
    TextView TextViewLanguage;
    @BindView(R.id.TextViewCatogeryName)
    TextView TextViewCatogeryName;
    @BindView(R.id.TextViewSubCatogeryName)
    TextView TextViewSubCatogeryName;
    @BindView(R.id.TextViewBookingDate)
    TextView TextViewBookingDate;
    @BindView(R.id.TextViewTime)
    TextView TextViewTime;
    @BindView(R.id.TextViewAmount)
    TextView TextViewAmount;
    @BindView(R.id.TextViewTrackMe)
    TextView TextViewTrackMe;
    @BindView(R.id.TextViewAddress)
    TextView TextViewAddress;@BindView(R.id.TextViewPoojaCancel)
    TextView TextViewPoojaCancel;

    //Bind ImageView
    @BindView(R.id.ImageViewPoojaImage)
    ImageView ImageViewPoojaImage;

    Menu menu;

    String Latitude, Longitude, BookingID, PoojaFinalSubmit = "";

    FloatingActionButton PoojaFinalSubmitButton;

    AlertDialog SuccessDialog;

    //Google
    LocationCallback mLocationCallback;
    FusedLocationProviderClient mFusedLocationClient;
    SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    String TAG = "Message";
    private static final int REQUEST_CHECK_SETTINGS = 100, REQUEST_LOCATION_ACCESS = 150;
    private static final int REQUEST_PERMISSION_SETTING = 151;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private LocationSettingsRequest mLocationSettingsRequest;


    String[] permissionsRequired = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pooja_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        myPreference = new MyPreference(this);

        TextViewPoojaCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog alertDialog = new AlertDialog.Builder(PoojaDetailActivity.this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Do you really want to cancel this Service ? ");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();


            }
        });

        PoojaFinalSubmitButton = (FloatingActionButton) findViewById(R.id.PoojaFinalSubmit);
        PoojaFinalSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(PoojaDetailActivity.this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Is the pooja was completed in customer house?\n\n" +
                        "If so continue with this, the booking will be updated to competed status\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                progressBar = new ProgressDialog(PoojaDetailActivity.this);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Making final submit ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();

                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                jsonObject.addProperty("BookingId", BookingID);
                                jsonObject.addProperty("Status", "4");
                                Log.e("JSON", jsonObject.toString());
                                API apiService = createServiceHeader(API.class);
                                Call<LoginPojo> call = apiService.ACCEPT_REJECT_POJO_CALL(jsonObject);
                                call.enqueue(new Callback<LoginPojo>() {
                                    @Override
                                    public void onResponse(Call<LoginPojo> Requestcall, Response<LoginPojo> response) {
                                        progressBar.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
                                                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(PoojaDetailActivity.this, R.style.RajCustomDialog);
                                                final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                                                final View dialogView = inflater.inflate(R.layout.custom_dialog_success, null);
                                                imageDialog.setView(dialogView);
                                                TextView TextviewSmallTitle1 = dialogView.findViewById(R.id.TextviewSmallTitle1);
                                                TextviewSmallTitle1.setText("Congratulations your pooja was closed successfully");
                                                TextView TextviewBigTitle = dialogView.findViewById(R.id.TextviewBigTitle);
                                                TextviewBigTitle.setText("Success!!!");
                                                Button ButtonOk = dialogView.findViewById(R.id.ButtonOk);
                                                ButtonOk.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        onBackPressed();
                                                        SuccessDialog.dismiss();
                                                    }
                                                });
                                                SuccessDialog = imageDialog.create();
                                                SuccessDialog.show();
                                                break;
                                            case "108":
                                                Toast.makeText(PoojaDetailActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                                startActivity(new Intent(PoojaDetailActivity.this, LoginActivity.class));
                                                break;
                                            default:
                                                myPreference.ShowDialog(PoojaDetailActivity.this,"Oops",response.body().getMessage());
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                                        progressBar.dismiss();
                                        try {
                                            myPreference.ShowDialog(PoojaDetailActivity.this,"Exception",t.getMessage());

                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressBar != null){
            progressBar.dismiss();
        }
        Bundle extras = getIntent().getExtras();
        if (myPreference.isInternetOn()) {
            if (extras != null && extras.containsKey("BookingID")) {
                progressBar = new ProgressDialog(PoojaDetailActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Processing ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                jsonObject.addProperty("BookingId", extras.getString("BookingID"));
                BookingID = extras.getString("BookingID");
                jsonObject.addProperty("SenderType", "1");
                Log.e("JSON", jsonObject.toString());
                API apiService = createServiceHeader(API.class);
                Call<BookingDetailListPojo> call = apiService.BOOKING_DETAIL_LIST_POJO_CALL(jsonObject);
                call.enqueue(new Callback<BookingDetailListPojo>() {
                    @Override
                    public void onResponse(Call<BookingDetailListPojo> call, Response<BookingDetailListPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                if (response.body().getBookingDetails().length > 0) {
                                    if (!response.body().getBookingDetails()[0].getUserImage().isEmpty()){
                                        Picasso.with(PoojaDetailActivity.this)
                                                .load(response.body().getBookingDetails()[0].getUserImage())
                                                .into(ImageViewPoojaImage);


                                        Glide.with(getApplicationContext())
                                                .load(response.body().getBookingDetails()[0].getUserImage())
                                                .apply(RequestOptions.circleCropTransform())
                                                .into(ImageViewPoojaImage);




                                    }else {
                                        ImageViewPoojaImage.setVisibility(View.GONE);
                                    }
                                    TextViewPoojaName.setText(response.body().getBookingDetails()[0].getPoojaName());
                                    setTitle(response.body().getBookingDetails()[0].getPoojaName());
                                    TextViewUserName.setText(response.body().getBookingDetails()[0].getUserName());
                                    TextViewUserEmailID.setText(response.body().getBookingDetails()[0].getUserEmailID());
                                    TextViewMoblieNumber.setText(response.body().getBookingDetails()[0].getUserPhoneNumber());
                                    TextViewLanguage.setText(response.body().getBookingDetails()[0].getLanguage());
                                    TextViewCatogeryName.setText(response.body().getBookingDetails()[0].getCatogeryName());
                                    TextViewSubCatogeryName.setText(response.body().getBookingDetails()[0].getSubCatogeryName());
                                    TextViewAddress.setText(response.body().getBookingDetails()[0].getLocationName());
                                    PoojaFinalSubmit = response.body().getBookingDetails()[0].getPoojaFinalSubmit();
                                    if (PoojaFinalSubmit.equals("1")){
                                        PoojaFinalSubmitButton.setVisibility(View.VISIBLE);
                                    }
                                    try {
                                        //Date Processing
                                        String ConvertedDate = new SimpleDateFormat("dd-MM-yyyy",
                                                Locale.getDefault()).format(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(response.body().getBookingDetails()[0].getBookingDate()));
                                        String CurrentDate = new SimpleDateFormat("dd-MM-yyyy",
                                                Locale.getDefault()).format(new Date());
                                        TextViewBookingDate.setText(ConvertedDate);

                                        //Time Processing
                                        Date ConvertedTime = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy", Locale.getDefault()).parse(response.body().getBookingDetails()[0].getTime()+" "+ConvertedDate);
                                        Date CurrentTime = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy", Locale.getDefault()).parse(new SimpleDateFormat("HH:mm:ss dd-MM-yyyy", Locale.getDefault()).format(new Date()));
                                        TextViewTime.setText(new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new SimpleDateFormat("hh:mm:ss", Locale.getDefault()).parse(new StringTokenizer(response.body().getBookingDetails()[0].getTime()).nextToken())));

                                        //Calculating Time difference
                                        long DifferenceTime = ConvertedTime.getTime() - CurrentTime.getTime();
                                        int DifferenceInHours = (int) ((DifferenceTime - (1000*60*60*24*(int) (DifferenceTime / (1000*60*60*24)))) / (1000*60*60));

                                        if (DifferenceInHours > 4 && !PoojaFinalSubmit.equals("1")) { //&& !PoojaFinalSubmit.equals("1")
                                            if (menu != null) {
                                                menu.clear();
                                            }
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    TextViewAmount.setText("Rs. " + response.body().getBookingDetails()[0].getAmount());
                                    Latitude = response.body().getBookingDetails()[0].getLatitude();
                                    Longitude = response.body().getBookingDetails()[0].getLongitude();
                                }
                                break;
                            case "108":
                                Toast.makeText(PoojaDetailActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(PoojaDetailActivity.this, LoginActivity.class));
                                break;
                            default:
                                myPreference.ShowDialog(PoojaDetailActivity.this, "Oops", response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<BookingDetailListPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(PoojaDetailActivity.this, "Execption", t.getMessage());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } else {
            myPreference.ShowDialog(this, "Oops", "There is no internet connection");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.track, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_track:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Are you on the way to attend this pooja now?\n\n" +
                        "If so continue with this, you'll be started to track and your location will be shared with the customer\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(PoojaDetailActivity.this, permissionsRequired, REQUEST_LOCATION_ACCESS);
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

//        LocationRequest locationRequest = LocationRequest.create();
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
//        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
//
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
//        builder.setAlwaysShow(true);
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(PoojaDetailActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        if (ActivityCompat.checkSelfPermission(PoojaDetailActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PoojaDetailActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
            }
        })
        .addOnFailureListener(PoojaDetailActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the
                            // result in onActivityResult().
                            ResolvableApiException rae = (ResolvableApiException) e;
                            rae.startResolutionForResult(PoojaDetailActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sie) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Location settings are inadequate, and cannot be " +
                                "fixed here. Fix in Settings.";
                        Log.e(TAG, errorMessage);

                        Toast.makeText(PoojaDetailActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                }
            }
        });

//        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
//        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//            @Override
//            public void onResult(LocationSettingsResult result) {
//                final Status status = result.getStatus();
//                switch (status.getStatusCode()) {
//                    case LocationSettingsStatusCodes.SUCCESS:
//                        Log.i(TAG, "All location settings are satisfied.");
//                        break;
//                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
//
//                        try {
//                            // Show the dialog by calling startResolutionForResult(), and check the result
//                            // in onActivityResult().
//                            status.startResolutionForResult(PoojaDetailActivity.this, REQUEST_CHECK_SETTINGS);
//                        } catch (IntentSender.SendIntentException e) {
//                            Log.i(TAG, "PendingIntent unable to execute request.");
//                        }
//                        break;
//                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
//                        break;
//                }
//            }
//        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        ContinueToMap();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
            case REQUEST_PERMISSION_SETTING:
//                int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
                int permissionState = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
                Log.e("permissionState", String.valueOf(permissionState));
                if (checkPermissions()){
                    ContinueToMap();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_ACCESS){
            if (checkPermissions()){
                ContinueToMap();
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(PoojaDetailActivity.this).create();
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setTitle("Alert!!!");
                alertDialog.setMessage("Location access permission is required to get your location. Enable it to track you\n\nThank you");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Proceed", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openSettings();
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        }
    }

    private void ContinueToMap() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (myPreference.isInternetOn()) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(PoojaDetailActivity.this);
            mSettingsClient = LocationServices.getSettingsClient(PoojaDetailActivity.this);


            assert manager != null;
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//                                                        displayLocationSettingsRequest(PoojaDetailActivity.this);
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
                mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                builder.addLocationRequest(mLocationRequest);
                mLocationSettingsRequest = builder.build();

                //////////////
                GoogleApiClient googleApiClient = new GoogleApiClient.Builder(PoojaDetailActivity.this)
                        .addApi(LocationServices.API).build();
                googleApiClient.connect();
                PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, mLocationSettingsRequest);
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(LocationSettingsResult result) {
                        final Status status = result.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                Log.i(TAG, "All location settings are satisfied.");
                                ContinueToTrackingActivity();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the result
                                    // in onActivityResult().
                                    status.startResolutionForResult(PoojaDetailActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                                break;
                        }
                    }
                });
            } else {
                ContinueToTrackingActivity();
            }
        }
    }

    private void ContinueToTrackingActivity() {
        progressBar = new ProgressDialog(PoojaDetailActivity.this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Getting your location ...!!!");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        Log.e("Testing", "Getting your location ...");

        MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
            @Override
            public void gotLocation(final Location location) {
                //Got the location!
                progressBar.dismiss();
                Log.i("Latitude", String.valueOf(location.getLatitude()));
                Log.i("Longitude", String.valueOf(location.getLongitude()));

                progressBar = new ProgressDialog(PoojaDetailActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Validating your tracking request...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                Log.e(TAG, myPreference.getDefaultRunTimeValue()[1]);
                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                Log.e(TAG, myPreference.getDefaultRunTimeValue()[0]);
                API apiService = createServiceHeader(API.class);
                Call<LoginPojo> call = apiService.VALIDATE_LOCATION_ACCESSTOKEN_POJO_CALL(jsonObject);
                call.enqueue(new Callback<LoginPojo>() {
                    @Override
                    public void onResponse(Call<LoginPojo> call, final Response<LoginPojo> response) {
                        progressBar.dismiss();
                        switch (response.body().getCode()) {
                            case "200":
                                progressBar = new ProgressDialog(PoojaDetailActivity.this);
                                progressBar.setCancelable(true);
                                progressBar.setMessage("Connecting to server ...");
                                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progressBar.setProgress(0);
                                progressBar.setMax(100);
                                progressBar.show();

                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
                                Log.e(TAG, myPreference.getDefaultRunTimeValue()[1]);
                                jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
                                Log.e(TAG, myPreference.getDefaultRunTimeValue()[0]);
                                jsonObject.addProperty("BookingId", getIntent().getExtras().getString("BookingID"));
                                Log.e(TAG, getIntent().getExtras().getString("BookingID"));
                                jsonObject.addProperty("Latitude", String.valueOf(location.getLatitude()));
                                Log.e(TAG, String.valueOf(location.getLatitude()));
                                jsonObject.addProperty("Longitude", String.valueOf(location.getLongitude()));
                                Log.e(TAG, String.valueOf(location.getLongitude()));
                                jsonObject.addProperty("CurrentTime", new SimpleDateFormat("kk:mm:ss", Locale.getDefault()).format(new Date()));
                                Log.e(TAG, new SimpleDateFormat("kk:mm:ss", Locale.getDefault()).format(new Date()));
                                jsonObject.addProperty("Status", "1");
                                jsonObject.addProperty("LocationAccessToken", "");


                                API apiService = createServiceHeader(API.class);
                                Call<LoginPojo> TrackingCall = apiService.TRACKING_POJO_CALL(jsonObject);
                                TrackingCall.enqueue(new Callback<LoginPojo>() {
                                    @Override
                                    public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                                        progressBar.dismiss();
                                        switch (response.body().getCode()) {
                                            case "200":
                                                if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("BookingID")){
                                                    Intent intent = new Intent(PoojaDetailActivity.this, TrackingActivity.class);
                                                    intent.putExtra("BookingID", getIntent().getExtras().getString("BookingID"));
                                                    intent.putExtra("MyLatitude", String.valueOf(location.getLatitude()));
                                                    intent.putExtra("MyLongitude", String.valueOf(location.getLongitude()));
                                                    intent.putExtra("CustomerLatitude", Latitude);
                                                    intent.putExtra("CustomerLongitude", Longitude);
                                                    intent.putExtra("CustomerName", TextViewUserName.getText().toString());
                                                    intent.putExtra("CustomerNumber", TextViewMoblieNumber.getText().toString());
                                                    intent.putExtra("CustomerAddress", TextViewAddress.getText().toString());
                                                    intent.putExtra("LocationAccessToken", response.body().getLocationAccessToken());
//                                                    Log.d(TAG, response.body().getLocationAccessToken());
                                                    System.out.print("LocationAT: " + response.body().getLocationAccessToken());
                                                    SharedPreferences.Editor editor = getSharedPreferences("LocationAccessToken", MODE_PRIVATE).edit();
                                                    editor.putString("AccessToken", response.body().getLocationAccessToken());
                                                    editor.apply();
                                                    intent.putExtra("LocationAccessToken", response.body().getLocationAccessToken());
                                                    startActivity(intent);
                                                }
                                                break;
                                            case "108":
                                                Toast.makeText(PoojaDetailActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                                startActivity(new Intent(PoojaDetailActivity.this, LoginActivity.class));
                                                break;
                                            default:
                                                myPreference.ShowDialog(PoojaDetailActivity.this,"Oops",response.body().getMessage());
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                                        progressBar.dismiss();
                                        try {
                                            myPreference.ShowDialog(PoojaDetailActivity.this,"Exception",t.getMessage());

                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                break;
                            case "202":
                                final AlertDialog alertDialog = new AlertDialog.Builder(PoojaDetailActivity.this).create();
                                alertDialog.setIcon(R.mipmap.ic_launcher);
                                alertDialog.setTitle("Alert!!!");
                                alertDialog.setMessage(response.body().getMessage() + "\n\n" +
                                        "Would you like to continue to go to previous tracking?");
                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(PoojaDetailActivity.this, TrackingActivity.class);
                                                intent.putExtra("BookingID", response.body().getBookingID());
                                                intent.putExtra("MyLatitude", String.valueOf(location.getLatitude()));
                                                intent.putExtra("MyLongitude", String.valueOf(location.getLongitude()));
                                                intent.putExtra("CustomerLatitude", response.body().getCustomerLatitude());
                                                intent.putExtra("CustomerLongitude", response.body().getCustomerLongitude());
                                                intent.putExtra("CustomerName", response.body().getCustomerName());
                                                intent.putExtra("CustomerNumber", response.body().getCustomerNumber());
                                                intent.putExtra("CustomerAddress", response.body().getCustomerAddress());
                                                intent.putExtra("LocationAccessToken", response.body().getLocationAccessToken());
//                                                Log.d(TAG, response.body().getLocationAccessToken());
                                                System.out.print("LocationAT: " + response.body().getLocationAccessToken());
                                                SharedPreferences.Editor editor = getSharedPreferences("LocationAccessToken", MODE_PRIVATE).edit();
                                                editor.putString("AccessToken", response.body().getLocationAccessToken());
                                                editor.apply();
                                                intent.putExtra("LocationAccessToken", response.body().getLocationAccessToken());
                                                startActivity(intent);
                                            }
                                        });
                                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        alertDialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                break;
                            case "108":
                                Toast.makeText(PoojaDetailActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(PoojaDetailActivity.this, LoginActivity.class));
                                break;
                            default:
                                myPreference.ShowDialog(PoojaDetailActivity.this,"Oops",response.body().getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginPojo> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            myPreference.ShowDialog(PoojaDetailActivity.this,"Exception",t.getMessage());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };

        MyLocation myLocation = new MyLocation(getApplication());
        myLocation.getLocation(PoojaDetailActivity.this, locationResult);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }



    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", PoojaDetailActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
