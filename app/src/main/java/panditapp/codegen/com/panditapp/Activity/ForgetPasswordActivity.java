package panditapp.codegen.com.panditapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Pojo.PurohitForgetPasswordPojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.Service.Config;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

public class ForgetPasswordActivity extends AppCompatActivity {
    @BindView(R.id.EditTextMoblieNumber)
    EditText EditTextMoblieNumber;
    @BindView(R.id.ButtonVeriftyOTP)
    Button ButtonVeriftyOTP;

    private ProgressDialog progressBar;
    MyPreference myPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_forget_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        final String CreatedBy = intent.getExtras().get("CreatedBy").toString();
        myPreference = new MyPreference(ForgetPasswordActivity.this);

        ButtonVeriftyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myPreference.isInternetOn()){
                    if(EditTextMoblieNumber.getText().toString().length()==10){
                        progressBar = new ProgressDialog(ForgetPasswordActivity.this);
                        progressBar.setCancelable(true);
                        progressBar.setMessage("Processing ...");
                        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressBar.setProgress(0);
                        progressBar.setMax(100);
                        progressBar.show();
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("DeviceId", displayFirebaseRegId());
                        jsonObject.addProperty("PurohitMobileNumber",EditTextMoblieNumber.getText().toString());
                        API apiService = createServiceHeader(API.class);
                        Call<PurohitForgetPasswordPojo> call = apiService.FORGET_PASSWORD_POJO_CALL(jsonObject);
                        call.enqueue(new Callback<PurohitForgetPasswordPojo>() {
                            @Override
                            public void onResponse(Call<PurohitForgetPasswordPojo> call, Response<PurohitForgetPasswordPojo> response) {
                                progressBar.dismiss();
                                switch (response.body().getCode()) {
                                    case "200":
                                        Intent intent = new Intent(ForgetPasswordActivity.this,OTPageActivity.class);
//                                        intent.putExtra("OTPgot",""+response.body().get());
                                        intent.putExtra("MoblieNumber",EditTextMoblieNumber.getText().toString());
                                        intent.putExtra("TempAccessToken",""+response.body().getAccessToken());
                                        intent.putExtra("NotificationCount", response.body().getNotificationCount());
                                        intent.putExtra("ForgetPassword", "1");
                                        intent.putExtra("CreatedBy",CreatedBy);
                                        startActivity(intent);
                                        break;


                                    case "108":
                                        Intent intentemg = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                                        startActivity(intentemg);
                                        break;
                                    default:
                                        myPreference.ShowDialog(ForgetPasswordActivity.this,"Oops",response.body().getMessage());
                                        break;

                                }

                            }

                            @Override
                            public void onFailure(Call<PurohitForgetPasswordPojo> call, Throwable t) {
                                progressBar.dismiss();
                                try {
                                    myPreference.ShowDialog(ForgetPasswordActivity.this,"Oops",t.getMessage());

                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }else{

                        Toast.makeText(ForgetPasswordActivity.this, "Please enter your mobile number", Toast.LENGTH_SHORT).show();

                    }
                }else{
                    myPreference.ShowDialog(ForgetPasswordActivity.this,"Oops!!","There is no internet connection");
                }
            }
        });

    }
    //validations
    boolean Check(EditText testEditText){
        return testEditText.getText().toString().length()>0 && !testEditText.getText().toString().isEmpty();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        if (!TextUtils.isEmpty(regId))
        {
            Log.e("OTP Activity", "Firebase reg id: " + regId);
            return regId;
//            FirebaseMessaging.getInstance().subscribeToTopic("news");
        }
        else
            Log.e("OTP Activity", "Firebase Reg Id is not received yet!");
        return regId;
    }

}
