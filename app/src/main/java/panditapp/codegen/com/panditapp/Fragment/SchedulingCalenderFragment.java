package panditapp.codegen.com.panditapp.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateLongClickListener;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import panditapp.codegen.com.panditapp.API.API;
import panditapp.codegen.com.panditapp.Activity.LoginActivity;
import panditapp.codegen.com.panditapp.Adapter.CalenderDateListAdapter;
import panditapp.codegen.com.panditapp.Pojo.PurohitSchedulingCalendarPojo;
import panditapp.codegen.com.panditapp.Pojo.PurohitSchedulingCalendarResponsePojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.accessibility.AccessibilityNodeInfo.CollectionInfo.SELECTION_MODE_MULTIPLE;
import static com.facebook.FacebookSdk.getApplicationContext;
import static panditapp.codegen.com.panditapp.Service.Service.createServiceHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class SchedulingCalenderFragment extends Fragment implements OnDateSelectedListener, View.OnClickListener {
    Date date, datepick;
    MyPreference myPreference;
    private ProgressDialog progressBar;
    Unbinder unbinder;
    MaterialCalendarView calendarView;
    String PickDates;
    DateFormat dateFormat;
    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    private List<PurohitSchedulingCalendarResponsePojo> purohitSchedulingCalendarResponsePojos, Temppurohitlist;
    ListView ListViewDateDetails;
    ArrayList<CalendarDay> dates;

    TextView TextViewEvent;
    ImageView ImageViewCloseButton;
    ImageView ImageViewUpArrow;
    LinearLayout bottom_sheet;
    BottomSheetBehavior sheetBehavior;

    public SchedulingCalenderFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scheduling_calender_frontend, container, false);
        unbinder = ButterKnife.bind(this, view);
        myPreference = new MyPreference(getActivity());
        getActivity().setTitle("Schedule");
//        setListViewHeightBasedOnChildren(L
//        istViewDateDetails);
//        ListViewDateDetails.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });

        calendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        dateFormat = new SimpleDateFormat("yyyy-MM", Locale.getDefault());
        date = new Date();
        calendarView.setOnClickListener(this);


       /* calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

                Toast.makeText(getApplicationContext(), String.valueOf(date), Toast.LENGTH_LONG).show();
                //clickCalender(date);
                calendarView.getSelectedDates();
                //  oneDayDecorator.setDate(date);
            }
        });*/

        calendarView.getSelectedDates();

        /*calendarView.addDecorator(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                Calendar cal1 = day.getCalendar();
                Calendar cal2 = Calendar.getInstance();
                //Toast.makeText(getApplicationContext(), String.valueOf(cal2), Toast.LENGTH_SHORT).show();

                Toast.makeText(getApplicationContext(), String.valueOf("fgfgf"), Toast.LENGTH_LONG).show();
                return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                        && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                        && cal1.get(Calendar.DAY_OF_YEAR) ==
                        cal2.get(Calendar.DAY_OF_YEAR));

            }

            @Override
            public void decorate(DayViewFacade view) {
                view.setBackgroundDrawable(ContextCompat.getDrawable(getContext(),R.drawable.selector));
               // Toast.makeText(getApplicationContext(), String.valueOf(date), Toast.LENGTH_LONG).show();
            }
        });*/

        calendarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), String.valueOf("fgfgf"), Toast.LENGTH_LONG).show();
            }
        });
        calendarView.setOnDateChangedListener(this);
        calendarView.addDecorators(new MySelectorDecorator(getActivity()),
                oneDayDecorator
        );
        oneDayDecorator.setDate(date);
        Log.i("dateFinal", date.toString());

        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        ListViewDateDetails = (ListView) view.findViewById(R.id.ListViewDateDetails);
        TextViewEvent = (TextView) view.findViewById(R.id.TextViewEvent);
        ImageViewCloseButton = (ImageView) view.findViewById(R.id.ImageViewCloseButton);
        ImageViewUpArrow = (ImageView) view.findViewById(R.id.ImageViewUpArrow);
        bottom_sheet = (LinearLayout) view.findViewById(R.id.bottom_sheet);

        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);


        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("Date", dateFormat.format(date));
            API apiService = createServiceHeader(API.class);
            Call<PurohitSchedulingCalendarPojo> call = apiService.PUROHIT_SCHEDULING_CALENDAR_POJO_CALL(jsonObject);
            call.enqueue(new Callback<PurohitSchedulingCalendarPojo>() {
                @Override
                public void onResponse(Call<PurohitSchedulingCalendarPojo> call, Response<PurohitSchedulingCalendarPojo> response) {

                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            purohitSchedulingCalendarResponsePojos = new ArrayList<>();
                            Collections.addAll(purohitSchedulingCalendarResponsePojos, response.body().getCustomerPoojaList());
                            //loadingdots
                            new ApiSimulator(purohitSchedulingCalendarResponsePojos).executeOnExecutor(Executors.newSingleThreadExecutor());

                            break;
                        case "108":
                            Intent logoutintent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(logoutintent);
                            break;
                        default:
                            myPreference.ShowDialog(getActivity(), "Oops", response.body().getMessage());

                            break;
                    }

                }

                @Override
                public void onFailure(Call<PurohitSchedulingCalendarPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        myPreference.ShowDialog(getActivity(), "Oops", t.getMessage());

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });

        } else {
            myPreference.ShowDialog(getActivity(), "Oops", "There is no internet connection");

        }
        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull final CalendarDay date, boolean selected) {
        calendarView.invalidateDecorators();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        PickDates = selected ? dateFormat.format(date.getDate()) : "No Selection";

        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");

        try {
            datepick = input.parse(PickDates);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //in onCreate

        ListViewDateDetails.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow NestedScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow NestedScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });


        if (purohitSchedulingCalendarResponsePojos.size() > 0) {
            Temppurohitlist = new ArrayList<>();
            for (int i = 0; i < purohitSchedulingCalendarResponsePojos.size(); i++) {
                if (PickDates.equals(purohitSchedulingCalendarResponsePojos.get(i).getBookingDate())) {
                    Temppurohitlist.add(purohitSchedulingCalendarResponsePojos.get(i));
                }
            }

            if (Temppurohitlist.size() > 0) {
                if (Temppurohitlist.size() == 1) {
                    ImageViewUpArrow.setVisibility(View.GONE);
                }
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                TextViewEvent.setText("Events : " + output.format(datepick));
                ImageViewCloseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                });
                CalenderDateListAdapter calenderDateListAdapter = new CalenderDateListAdapter(getActivity(), Temppurohitlist);
                ListViewDateDetails.setAdapter(calenderDateListAdapter);
                ListViewDateDetails.setVisibility(View.VISIBLE);

            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                TextViewEvent.setText("No booking");
                ListViewDateDetails.setVisibility(View.GONE);
                ImageViewCloseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                });
            }

        } else {
            Log.i("Dates", "No data found");


        }

    }

    @Override
    public void onClick(View v) {

    }


    /**
     * Use a custom selector
     */
    public class MySelectorDecorator implements DayViewDecorator {

        private final Drawable drawable;

        public MySelectorDecorator(Activity context) {
            drawable = context.getResources().getDrawable(R.drawable.selector);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return true;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setSelectionDrawable(drawable);
        }
    }


    /**
     * Decorate a day by making the text big and bold
     */
    public class OneDayDecorator implements DayViewDecorator {

        private CalendarDay date;

        public OneDayDecorator() {
            date = CalendarDay.today();
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return date != null && day.equals(date);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new StyleSpan(Typeface.BOLD));
            view.addSpan(new RelativeSizeSpan(1.4f));
        }

        /**
         * We're changing the internals, so make sure to call {@linkplain MaterialCalendarView#invalidateDecorators()}
         *
         * @param date
         */
        public void setDate(java.util.Date date) {
            this.date = CalendarDay.from(date);
        }
    }

    /**
     * Simulate an API call to show how to add decorators
     */
    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {
        List<PurohitSchedulingCalendarResponsePojo> purohitSchedulingCalendarResponsePojos;

        public ApiSimulator(List<PurohitSchedulingCalendarResponsePojo> purohitSchedulingCalendarResponsePojos) {
            this.purohitSchedulingCalendarResponsePojos = purohitSchedulingCalendarResponsePojos;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dates = new ArrayList<>();

            for (int i = 0; i < purohitSchedulingCalendarResponsePojos.size(); i++) {
                Calendar calendar1 = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                try {
                    calendar1.setTime(sdf.parse(purohitSchedulingCalendarResponsePojos.get(i).getBookingDate()));// all done
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                final CalendarDay day = CalendarDay.from(calendar1);
                dates.add(day);


            }


            return dates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            if (getActivity().isFinishing()) {
                return;
            }

            calendarView.addDecorator(new EventDecorator(Color.RED, dates));
        }
    }

    /**
     * Decorate several days with a dot
     */
    public class EventDecorator implements DayViewDecorator {

        private int color;
        private HashSet<CalendarDay> dates;
        private Collection<CalendarDay> calendarDays;

        public EventDecorator(int color, Collection<CalendarDay> dates) {
            this.color = color;
            this.dates = new HashSet<>(dates);
            this.calendarDays = dates;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {

            for (int i = 0; i < calendarDays.size(); i++) {
                Log.i("Collection Test", "" + calendarDays.toArray()[i]);
            }
            view.addSpan(new DotSpan(5, color));
        }
    }

//    /**** Method for Setting the Height of the ListView dynamically.
//     **** Hack to fix the issue of not showing all the items of the ListView
//     **** when placed inside a ScrollView  ****/
//    public static void setListViewHeightBasedOnChildren(ListView listView) {
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter == null)
//            return;
//
//        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
//        int totalHeight = 0;
//        View view = null;
//        for (int i = 0; i < listAdapter.getCount(); i++) {
//            view = listAdapter.getView(i, view, listView);
//            if (i == 0)
//                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
//
//            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//            totalHeight += view.getMeasuredHeight();
//        }
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
//        listView.setLayoutParams(params);
//    }

    public class BottomSheetListView extends ListView {
        public BottomSheetListView(Context context, AttributeSet p_attrs) {
            super(context, p_attrs);
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            return true;
        }

        @Override
        public boolean onTouchEvent(MotionEvent ev) {
            if (canScrollVertically(this)) {
                getParent().requestDisallowInterceptTouchEvent(true);
            }
            return super.onTouchEvent(ev);
        }

        public boolean canScrollVertically(AbsListView view) {
            boolean canScroll = false;

            if (view != null && view.getChildCount() > 0) {
                boolean isOnTop = view.getFirstVisiblePosition() != 0 || view.getChildAt(0).getTop() != 0;
                boolean isAllItemsVisible = isOnTop && view.getLastVisiblePosition() == view.getChildCount();

                if (isOnTop || isAllItemsVisible) {
                    canScroll = true;
                }
            }

            return canScroll;
        }
    }

    public void clickCalender(CalendarDay date) {

        if (myPreference.isInternetOn()) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setCancelable(true);
            progressBar.setMessage("Processing...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.show();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("DeviceId", myPreference.getDefaultRunTimeValue()[1]);
            jsonObject.addProperty("AccessToken", myPreference.getDefaultRunTimeValue()[0]);
            jsonObject.addProperty("Date", dateFormat.format(date));
            API apiService = createServiceHeader(API.class);
            Call<PurohitSchedulingCalendarPojo> call = apiService.PUROHIT_SCHEDULING_CALENDAR_POJO_CALL(jsonObject);
            call.enqueue(new Callback<PurohitSchedulingCalendarPojo>() {
                @Override
                public void onResponse(Call<PurohitSchedulingCalendarPojo> call, Response<PurohitSchedulingCalendarPojo> response) {

                    progressBar.dismiss();
                    switch (response.body().getCode()) {
                        case "200":
                            purohitSchedulingCalendarResponsePojos = new ArrayList<>();
                            Collections.addAll(purohitSchedulingCalendarResponsePojos, response.body().getCustomerPoojaList());
                            //loadingdots
                            new ApiSimulator(purohitSchedulingCalendarResponsePojos).executeOnExecutor(Executors.newSingleThreadExecutor());

                            break;
                        case "108":
                            Intent logoutintent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(logoutintent);
                            break;
                        default:
                            myPreference.ShowDialog(getActivity(), "Oops", response.body().getMessage());

                            break;
                    }

                }

                @Override
                public void onFailure(Call<PurohitSchedulingCalendarPojo> call, Throwable t) {
                    progressBar.dismiss();
                    try {
                        myPreference.ShowDialog(getActivity(), "Oops", t.getMessage());

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
            });
        } else {
            myPreference.ShowDialog(getActivity(), "Oops", "There is no internet connection");

        }
    }


}


