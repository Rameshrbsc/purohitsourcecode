package panditapp.codegen.com.panditapp.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import panditapp.codegen.com.panditapp.Pojo.PoojaListResponsePojo;
import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.interfac.ServiceCallBack;

/**
 * Created by Parsania Hardik on 29-Jun-17.
 */
public class ServiceCheckBoxAdapter extends RecyclerView.Adapter<ServiceCheckBoxAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    public static ArrayList<PoojaListResponsePojo> poojaListResponsePojos;
    private Context ctx;
    private ServiceCallBack callBack;

    public ServiceCheckBoxAdapter(Context ctx, ArrayList<PoojaListResponsePojo> poojaListResponsePojos,ServiceCallBack serviceCallBack) {

        inflater = LayoutInflater.from(ctx);
        this.poojaListResponsePojos = poojaListResponsePojos;
        this.callBack = serviceCallBack;
        this.ctx = ctx;
    }

    @Override
    public ServiceCheckBoxAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.service_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.checkBox.setText(poojaListResponsePojos.get(position).getPoojaName());
        // holder.checkBox.setChecked(poojaListResponsePojos.get(position).getPoojaName());
        holder.tvAnimal.setText(poojaListResponsePojos.get(position).getPoojaName());

        // holder.checkBox.setTag(R.integer.btnplusview, convertView);
        //holder.checkBox.setTag(position);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(holder.checkBox.isChecked()){

                    poojaListResponsePojos.get(position).getPoojaId();
                    callBack.onClickCallback(poojaListResponsePojos.get(position).getPoojaId());

                }else {
                    callBack.onClickCallback("");
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return poojaListResponsePojos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        protected CheckBox checkBox;
        private TextView tvAnimal;

        public MyViewHolder(View itemView) {
            super(itemView);

            checkBox = (CheckBox) itemView.findViewById(R.id.cb);
            tvAnimal = (TextView) itemView.findViewById(R.id.areaName);
        }

    }
}





