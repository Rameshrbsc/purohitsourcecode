package panditapp.codegen.com.panditapp.Pojo;

public class SearchAreaListAPI {
    private SearchAreaListResponseAPI[] response;

    private String message;

    private String code;

    public SearchAreaListResponseAPI[] getResponse ()
    {
        return response;
    }

    public void setResponse (SearchAreaListResponseAPI[] response)
    {
        this.response = response;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [response = "+response+", message = "+message+", code = "+code+"]";
    }
}
