package panditapp.codegen.com.panditapp.Pojo;

public class GoogleMapLatLongToAddressResultsGeometryPojo {

    private String location_type;

    private GoogleMapLatLongToAddressResultsGeometryLocationPojo location;

    public String getLocation_type ()
    {
        return location_type;
    }

    public void setLocation_type (String location_type)
    {
        this.location_type = location_type;
    }

    public GoogleMapLatLongToAddressResultsGeometryLocationPojo getLocation ()
    {
        return location;
    }

    public void setLocation (GoogleMapLatLongToAddressResultsGeometryLocationPojo location)
    {
        this.location = location;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [location_type = "+location_type+", location = "+location+"]";
    }
}
