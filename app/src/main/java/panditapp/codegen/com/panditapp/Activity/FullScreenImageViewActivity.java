package panditapp.codegen.com.panditapp.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.squareup.picasso.Picasso;

import java.util.Objects;

import panditapp.codegen.com.panditapp.R;
import panditapp.codegen.com.panditapp.SupportClass.TouchImageView;

public class FullScreenImageViewActivity extends AppCompatActivity {
    TouchImageView imgDisplay;
    CoordinatorLayout layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle("Profile Image");
//        toolbar.setNavigationIcon(R.drawable.ic_back_while);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

        imgDisplay = (TouchImageView) findViewById(R.id.imgDisplay);
        Intent intent = getIntent();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Picasso.with(this).load(Objects.requireNonNull(Objects.requireNonNull(intent.getExtras()).get("Path")).toString()).into(imgDisplay);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    @Override
    public void onBackPressed() {

        finish();
    }

    }
